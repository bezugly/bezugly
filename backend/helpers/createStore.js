import axios from 'axios';
import http from 'http';
import https from 'https';
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';

import reducers from '../../frontend/reducers';

export default (req) => {
    const axiosInstance = axios.create({
        baseURL: `${process.env.API}/v1`,
        headers: {
            cookie: req.get('cookie') || '',
            'Content-Type': 'application/json',
        },
        withCredentials: true,
        httpAgent: new http.Agent({ keepAlive: true }),
        httpsAgent: new https.Agent({ keepAlive: true }),
    });

    return createStore(reducers, {}, applyMiddleware(thunk.withExtraArgument(axiosInstance)));
};
