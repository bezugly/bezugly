import React from 'react';
import { renderToString } from 'react-dom/server';
import { StaticRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import { renderRoutes } from 'react-router-config';
import serialize from 'serialize-javascript';
import { Helmet } from 'react-helmet';
import Routes from '../../frontend/Routes';
import html from '../../public/template.html';

export default (req, store, context) => {
    const content = renderToString(
        <Provider store={store}>
            <StaticRouter location={req.path} context={context}>
                <div>{renderRoutes(Routes)}</div>
            </StaticRouter>
        </Provider>,
    );

    const helmet = Helmet.renderStatic();
    const head = `${helmet.title.toString()}\n${helmet.meta.toString()}`;

    return html
        .replace('React content', content)
        .replace('Head content', head)
        .replace(
            'After body content',
            `<script>window.INITIAL_STATE = ${serialize(store.getState())};</script>`,
        );
};
