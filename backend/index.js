import 'babel-polyfill';
import express from 'express';
import { matchRoutes } from 'react-router-config';
import proxy from 'express-http-proxy';
import morgan from 'morgan';
import moment from 'moment-timezone';
import renderer from './helpers/renderer';
import createStore from './helpers/createStore';
import Routes from '../frontend/Routes';

import { getCurrentUser } from '../frontend/actions';

require('dotenv').config();

const app = express();

morgan.token('moscow-date', () => moment().tz('Europe/Moscow').format());

app.use(morgan(':moscow-date - :method :url :status - HTTP/:http-version - :user-agent - :remote-addr - :remote-user'));

app.use('/api', proxy(process.env.API, {
    proxyReqOptDecorator(opts) {
        const options = { ...opts };

        options.headers['x-forwarded-host'] = `${process.env.HOST}:${process.env.PORT}`;

        return options;
    },

    limit: '10mb',
}));

app.use(express.static('public'));

app.use(express.static('static'));

const renderContent = (req, res, store) => {
    const context = {};
    const content = renderer(req, store, context);

    if (context.url) {
        res.redirect(301, context.url);
    }

    if (context.notFound) {
        res.status(404);
    }

    res.send(content);
};

app.get('*', (req, res) => {
    const store = createStore(req);
    const promises = matchRoutes(Routes, req.path)
        .map(({ route, match }) => route.loadData && route.loadData(store, match.params))
        .filter(Boolean)
        .map(promise => new Promise(resolve => promise.then(resolve).catch(resolve)));

    Promise.all(promises)
        .then(() => store.dispatch(getCurrentUser()))
        .then(() => renderContent(req, res, store))
        .catch(() => renderContent(req, res, store));
});

app.listen(process.env.PORT, () => {
    console.log(`Сервер запущен на ${process.env.PORT} порте`);
});
