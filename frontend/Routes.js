import React from 'react';
import { Redirect } from 'react-router-dom';

import App from './components/App';
import MainPage from './pages/Main';
import CollaborationPage from './pages/Collaboration';
import BlogPage from './pages/Blog';
import DraftsPage from './pages/Blog/Drafts';
import NewDraftPage from './pages/Blog/Drafts/New';
import DraftEditPage from './pages/Blog/Drafts/Edit';
import DraftItemPage from './pages/Blog/Drafts/Item';
import PostsPage from './pages/Blog/Posts';
import PostItemPage from './pages/Blog/Posts/Item';
import NotFoundPage from './pages/NotFound';

const blogRoutes = [
    {
        ...PostsPage,
        path: '/blog',
        exact: true,
    },
    {
        path: '/blog/posts',
        exact: true,
        render: () => <Redirect to="/blog" />,
    },
    {
        ...PostItemPage,
        path: '/blog/posts/:slug',
        exact: true,
    },
    {
        ...DraftsPage,
        path: '/blog/drafts',
        exact: true,
    },
    {
        ...DraftItemPage,
        path: '/blog/drafts/:slug',
        exact: true,
    },
    {
        ...DraftEditPage,
        path: '/blog/drafts/:slug/edit',
        exact: true,
    },
    {
        ...NotFoundPage,
    },
];

export default [
    {
        ...App,
        routes: [
            {
                ...MainPage,
                path: '/',
                exact: true,
            },
            {
                ...BlogPage,
                path: '/blog',
                routes: blogRoutes,
            },
            {
                ...CollaborationPage,
                path: '/collaboration',
                exact: true,
            },
            {
                ...CollaborationPage,
                path: '/collaboration/:tab(company|frontend|hr)',
                exact: true,
            },
            {
                ...NewDraftPage,
                path: '/new-post',
                exact: true,
            },
            {
                ...NotFoundPage,
            },
        ],
    },
];
