export const FETCH_POSTS = 'FETCH_POSTS';
export const FETCH_POST = 'FETCH_POST';
export const DELETE_POST = 'DELETE_POST';
export const COPY_POST = 'COPY_POST';

export const FETCH_DRAFTS = 'FETCH_DRAFTS';
export const FETCH_DRAFT = 'FETCH_DRAFT';
export const CREATE_DRAFT = 'CREATE_DRAFT';
export const UPDATE_DRAFT = 'UPDATE_DRAFT';
export const DELETE_DRAFT = 'DELETE_DRAFT';
export const PUBLISH_DRAFT = 'PUBLISH_DRAFT';
export const REPLACE_DRAFT_WITH_POST = 'REPLACE_DRAFT_WITH_POST';
export const PUBLISH_DRAFT_AS_NEW_POST = 'PUBLISH_DRAFT_AS_NEW_POST';

export const CHECK_SLUG = 'CHECK_SLUG';

export const UPLOAD_OG_IMAGE = 'UPLOAD_OG_IMAGE';
export const UPLOAD_FILE = 'UPLOAD_FILE';
export const UPDATE_FILE = 'UPDATE_FILE';

export const LOGIN_USER = 'LOGIN_USER';
export const LOGOUT_USER = 'LOGOUT_USER';
export const GET_CURRENT_USER = 'GET_CURRENT_USER';

export const SEND_MESSAGE = 'SEND_MESSAGE';
export const REMOVE_MESSAGE = 'REMOVE_MESSAGE';

export const SHOW_LOADER = 'SHOW_LOADER';

export const START = '_START';
export const SUCCESS = '_SUCCESS';
export const FAIL = '_FAIL';
