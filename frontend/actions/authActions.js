import {
    START,
    SUCCESS,
    FAIL,
    LOGIN_USER,
    LOGOUT_USER,
    GET_CURRENT_USER,
} from './actionTypes';

import { sendMessage } from './messageActions';

export const loginUser = password => async (dispatch, getState, api) => {
    dispatch({ type: LOGIN_USER + START });

    let body = null;

    try {
        body = await api.post('/user/login', {
            email: 'd.e.bezugly@gmail.com',
            password,
        });
    } catch ({ response: { data: { status, message } } }) {
        if (status !== 404) {
            dispatch(sendMessage('danger', status, message));
        }

        return dispatch({ type: LOGIN_USER + FAIL, status });
    }

    return dispatch({ type: LOGIN_USER + SUCCESS, payload: body.data });
};

export const logoutUser = () => async (dispatch, getState, api) => {
    dispatch({ type: LOGOUT_USER + START });

    try {
        await api.get('/user/logout');
    } catch ({ response: { data: { status, message } } }) {
        dispatch(sendMessage('danger', status, message));

        return dispatch({ type: LOGOUT_USER + FAIL });
    }

    return dispatch({ type: LOGOUT_USER + SUCCESS, payload: null });
};

export const getCurrentUser = () => async (dispatch, getState, api) => {
    dispatch({ type: GET_CURRENT_USER + START });

    let body = null;

    try {
        body = await api.get('/user');
    } catch (_) {
        return dispatch({ type: GET_CURRENT_USER + FAIL });
    }

    return dispatch({ type: GET_CURRENT_USER + SUCCESS, payload: body.data });
};
