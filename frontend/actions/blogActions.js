import {
    SHOW_LOADER,
    FETCH_POSTS,
    FETCH_POST,
    DELETE_POST,
    COPY_POST,
    FETCH_DRAFTS,
    FETCH_DRAFT,
    CREATE_DRAFT,
    UPDATE_DRAFT,
    DELETE_DRAFT,
    PUBLISH_DRAFT,
    REPLACE_DRAFT_WITH_POST,
    PUBLISH_DRAFT_AS_NEW_POST,
    CHECK_SLUG,
    UPLOAD_FILE,
    UPDATE_FILE,
    UPLOAD_OG_IMAGE,
    START,
    SUCCESS,
    FAIL,
} from './actionTypes';

import { sendMessage } from './messageActions';

const SHOW_LOADER_DELAY = 1000;

export const showLoader = loader => ({ type: SHOW_LOADER, payload: loader });

export const fetchPosts = () => async (dispatch, getState, api) => {
    dispatch({ type: FETCH_POSTS + START });

    const loaderTimeoutId = setTimeout(() => dispatch(showLoader('posts')), SHOW_LOADER_DELAY);
    let body = null;

    try {
        body = await api.get('/blog/posts');
    } catch ({ response: { data: { message } } }) {
        clearTimeout(loaderTimeoutId);

        return dispatch({ type: FETCH_POSTS + FAIL, error: message });
    }

    clearTimeout(loaderTimeoutId);

    const payload = body.data;

    payload.list = payload.list.reduce((map, post) => map.set(post.slug, post), new Map());

    return dispatch({ type: FETCH_POSTS + SUCCESS, payload });
};

export const fetchPostById = id => async (dispatch, getState, api) => {
    dispatch({ type: FETCH_POST + START });

    const loaderTimeoutId = setTimeout(() => dispatch(showLoader('post')), SHOW_LOADER_DELAY);
    let body = null;

    try {
        body = await api.get(`/blog/posts/${id}`);
    } catch ({ response: { data: { message } } }) {
        clearTimeout(loaderTimeoutId);

        return dispatch({ type: FETCH_POST + FAIL, error: message });
    }

    clearTimeout(loaderTimeoutId);

    return dispatch({ type: FETCH_POST + SUCCESS, payload: body.data });
};

export const fetchPostBySlug = slug => async (dispatch, getState, api) => {
    dispatch({ type: FETCH_POST + START });

    let body = null;

    try {
        body = await api.get(`/blog/posts?slug=${slug}`);
    } catch ({ response: { data: { message } } }) {
        return dispatch({ type: FETCH_POST + FAIL, error: message });
    }

    const payload = {
        post: body.data.list[0],
        files: body.data.files,
    };

    return dispatch({ type: FETCH_POST + SUCCESS, payload });
};

export const createDraft = (data = {}) => async (dispatch, getState, api) => {
    dispatch({ type: CREATE_DRAFT + START });

    let body = null;

    try {
        body = await api.post('/blog/posts', data);
    } catch ({ response: { data: { status, message } } }) {
        if (status !== 401) {
            dispatch(sendMessage('danger', status, message));
        }

        return dispatch({ type: CREATE_DRAFT + FAIL, error: message });
    }

    return dispatch({ type: CREATE_DRAFT + SUCCESS, payload: body.data });
};

export const deletePost = id => async (dispatch, getState, api) => {
    dispatch({ type: DELETE_POST + START });

    let body = null;

    try {
        body = await api.delete(`/blog/posts/${id}`);
    } catch ({ response: { data: { message } } }) {
        return dispatch({ type: DELETE_POST + FAIL, error: message });
    }

    return dispatch({ type: DELETE_POST + SUCCESS, payload: body.data });
};

export const copyPost = post => async (dispatch) => {
    dispatch({ type: COPY_POST + START });

    let body = null;

    try {
        const newPost = { ...post };

        // eslint-disable-next-line
        newPost.reference = post._id;

        // eslint-disable-next-line
        delete newPost.__v;
        // eslint-disable-next-line
        delete newPost._id;

        body = await dispatch(createDraft(newPost));
    } catch ({ response: { data: { message } } }) {
        return dispatch({ type: COPY_POST + FAIL, error: message });
    }

    return dispatch({ type: COPY_POST + SUCCESS, payload: body.payload });
};

export const fetchDrafts = () => async (dispatch, getState, api) => {
    dispatch({ type: FETCH_DRAFTS + START });

    const loaderTimeoutId = setTimeout(() => dispatch(showLoader('drafts')), SHOW_LOADER_DELAY);
    let body = null;

    try {
        body = await api.get('/blog/drafts');
    } catch ({ response: { data: { message } } }) {
        clearTimeout(loaderTimeoutId);

        return dispatch({ type: FETCH_DRAFTS + FAIL, error: message });
    }

    clearTimeout(loaderTimeoutId);

    const payload = body.data;

    payload.list = payload.list.reduce((map, draft) => map.set(draft.draftSlug, draft), new Map());

    return dispatch({ type: FETCH_DRAFTS + SUCCESS, payload });
};

export const fetchDraftById = id => async (dispatch, getState, api) => {
    dispatch({ type: FETCH_DRAFT + START });

    let body = null;

    try {
        body = await api.get(`/blog/drafts/${id}`);
    } catch ({ response: { data: { message } } }) {
        return dispatch({ type: FETCH_DRAFT + FAIL, error: message });
    }

    return dispatch({ type: FETCH_DRAFT + SUCCESS, payload: body.data });
};

export const fetchDraftBySlug = slug => async (dispatch, getState, api) => {
    dispatch({ type: FETCH_DRAFT + START });

    const loaderTimeoutId = setTimeout(() => dispatch(showLoader('draft')), SHOW_LOADER_DELAY);
    let body = null;

    try {
        body = await api.get(`/blog/drafts?draftSlug=${slug}`);
    } catch ({ response: { data: { message } } }) {
        clearTimeout(loaderTimeoutId);

        return dispatch({ type: FETCH_DRAFT + FAIL, error: message });
    }

    clearTimeout(loaderTimeoutId);

    const payload = {
        draft: body.data.list[0],
        files: body.data.files,
    };

    return dispatch({ type: FETCH_DRAFT + SUCCESS, payload });
};

export const updateDraft = (id, data = {}) => async (dispatch, getState, api) => {
    dispatch({ type: UPDATE_DRAFT + START });

    let body = null;

    try {
        body = await api.patch(`/blog/posts/${id}`, data);
    } catch ({ response }) {
        // Нет интернета
        if (!response) {
            dispatch(sendMessage('warning', 522, 'Нет интернета'));
        }

        const { message: error } = response.data;

        return dispatch({ type: UPDATE_DRAFT + FAIL, error });
    }

    return dispatch({ type: UPDATE_DRAFT + SUCCESS, payload: body.data });
};

export const deleteDraft = id => async (dispatch, getState, api) => {
    dispatch({ type: DELETE_DRAFT + START });

    let body = null;

    try {
        body = await api.delete(`/blog/posts/${id}`);
    } catch ({ response: { data: { message } } }) {
        return dispatch({ type: DELETE_DRAFT + FAIL, error: message });
    }

    return dispatch({ type: DELETE_DRAFT + SUCCESS, payload: body.data });
};

export const publishDraft = id => async (dispatch, getState, api) => {
    dispatch({ type: PUBLISH_DRAFT + START });

    let body = null;

    try {
        body = await api.patch(`/blog/posts/${id}`, { draft: false });
    } catch ({ response: { data: { status, message } } }) {
        dispatch(sendMessage('danger', status, message));

        return dispatch({ type: PUBLISH_DRAFT + FAIL, error: message });
    }

    return dispatch({ type: PUBLISH_DRAFT + SUCCESS, payload: body.data });
};

export const replaceDraftWithPost = draft => async (dispatch) => {
    dispatch({ type: REPLACE_DRAFT_WITH_POST + START });

    // eslint-disable-next-line
    const draftId = draft._id;
    const postId = draft.reference;
    let body = null;

    try {
        await dispatch(deletePost(postId));

        body = await dispatch(publishDraft(draftId));
    } catch ({ response: { data: { status, message } } }) {
        dispatch(sendMessage('danger', status, message));

        return dispatch({ type: REPLACE_DRAFT_WITH_POST + FAIL, error: message });
    }

    return dispatch({ type: REPLACE_DRAFT_WITH_POST + SUCCESS, payload: body.payload });
};

export const publishDraftAsNewPost = draft => async (dispatch) => {
    dispatch({ type: PUBLISH_DRAFT_AS_NEW_POST + START });

    let body = null;
    const newPost = { ...draft };

    // eslint-disable-next-line
    delete newPost.reference;

    try {
        // eslint-disable-next-line
        body = await dispatch(publishDraft(newPost._id));
    } catch ({ response: { data: { status, message } } }) {
        return dispatch({ type: PUBLISH_DRAFT_AS_NEW_POST + FAIL, error: message });
    }

    return dispatch({ type: PUBLISH_DRAFT_AS_NEW_POST + SUCCESS, payload: body.payload });
};

export const checkSlug = slug => async (dispatch, getState, api) => {
    dispatch({ type: CHECK_SLUG + START });

    let body = null;

    try {
        body = await api.get(`/blog/posts/slugs/${slug}`);
    } catch ({ response: { data: { message } } }) {
        return dispatch({ type: CHECK_SLUG + FAIL });
    }

    return dispatch({ type: CHECK_SLUG + SUCCESS, payload: body.data });
};

export const uploadFile = file => async (dispatch, getState, api) => {
    dispatch({ type: UPLOAD_FILE + START });

    const formData = new FormData();
    let body = null;

    formData.append('photo', file);

    try {
        body = await api.post('/blog/images', formData, {
            headers: {
                'Content-Type': 'multipart/form-data',
            },
        });
    } catch ({ response: { data: { status, message } } }) {
        if (status === 413) {
            dispatch(sendMessage('warning', status, 'Файл слишком большой. Принимаем до 10 Мб.'));
        } else {
            dispatch(sendMessage('danger', status, message));
        }

        return dispatch({ type: UPLOAD_FILE + FAIL, error: message });
    }

    return dispatch({ type: UPLOAD_FILE + SUCCESS, payload: body.data });
};

export const uploadOgImage = file => async (dispatch) => {
    dispatch({ type: UPLOAD_OG_IMAGE + START });

    let body = null;

    try {
        // eslint-disable-next-line
        body = await dispatch(uploadFile(file));
    } catch ({ response: { data: { status, message } } }) {
        return dispatch({ type: UPLOAD_OG_IMAGE + FAIL, error: message });
    }

    return dispatch({ type: UPLOAD_OG_IMAGE + SUCCESS, payload: body.payload });
};

export const updateFile = (fileId, data) => async (dispatch, getState, api) => {
    dispatch({ type: UPDATE_FILE + START });

    let body = null;

    try {
        body = await api.patch(`/blog/images/${fileId}`, data);
    } catch ({ response: { data: { status, message } } }) {
        dispatch(sendMessage('danger', status, message));

        return dispatch({ type: UPDATE_FILE + FAIL, error: message });
    }

    return dispatch({ type: UPDATE_FILE + SUCCESS, payload: body.data });
};
