import {
    SEND_MESSAGE,
    REMOVE_MESSAGE,
} from './actionTypes';

export const sendMessage = (theme, status, text) => ({
    type: SEND_MESSAGE,
    theme,
    status,
    text,
});

export const removeMessage = id => ({ type: REMOVE_MESSAGE, id });
