import React from 'react';
import PropTypes from 'prop-types';

import P from 'ui/Typography/P';
import Link from 'ui/Typography/Link';
import Spinner from 'ui/Spinner';

import PromoText from '../PromoText';
import BlogMenu from '../BlogMenu';

import './style.scss';

const renderList = list => (
    Array.from(list).map(([key, post]) => (
        <article key={key} className="blog-all-posts__link">
            <Link
                theme="default"
                href={`/blog/posts/${post.slug}`}
                inlineBlock
            >
                {post.title}
            </Link>
        </article>
    ))
);

const renderScreen = (loader, posts) => {
    if (loader) {
        return (
            <section className="page-layout__container">
                <Spinner theme="default" size="large" />
            </section>
        );
    }

    return (
        <div className="page-layout__container">
            {posts.list.size
                ? renderList(posts.list)
                : (
                    <P size="large">
                        Почитать пока нечего&nbsp;
                        <span role="img" aria-label="Удивленное лицо">😱</span>
                    </P>
                )
            }
        </div>
    );
};

const AllPostsPageContent = ({ loader, posts, title }) => (
    <div className="page-layout__main blog-all">
        <section className="page-layout__container">
            <BlogMenu />
        </section>

        <section className="page-layout__container">
            <PromoText title={title} />
        </section>

        {renderScreen(loader, posts)}
    </div>
);

AllPostsPageContent.propTypes = {
    posts: PropTypes.shape({
        list: PropTypes.instanceOf(Map).isRequired,
    }).isRequired,
    loader: PropTypes.bool.isRequired,
    title: PropTypes.string.isRequired,
};

export default AllPostsPageContent;
