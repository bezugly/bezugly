import { connect } from 'react-redux';

import AllPostsPageContent from './AllPostsPageContent';

const mapStateToProps = ({ blog: { posts, loaders } }, { title }) => ({
    posts,
    loader: loaders.posts,
    title,
});

export default connect(mapStateToProps, null)(AllPostsPageContent);
