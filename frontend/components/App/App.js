import React from 'react';
import PropTypes from 'prop-types';
import { renderRoutes } from 'react-router-config';

import Message from 'ui/Message';
import ErrorBoundary from '../ErrorBoundary';
import PageLayout from '../PageLayout';

import './App.scss';

const App = ({ route, location: { pathname } }) => (
    <PageLayout pathname={pathname}>
        <ErrorBoundary>
            {renderRoutes(route.routes)}
            <Message />
        </ErrorBoundary>
    </PageLayout>
);

App.propTypes = {
    route: PropTypes.shape({
        routes: PropTypes.array.isRequired,
    }).isRequired,
    location: PropTypes.shape({
        pathname: PropTypes.string.isRequired,
    }).isRequired,
};

export default {
    component: App,
};
