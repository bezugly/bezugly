import React from 'react';

import './style.scss';

const BlogContent = ({ children, className, ...restProps }) => {
    const modifiedChildren = React.Children.map(children, (child) => {
        if (child.type && child.type.name === 'Factoid') {
            return (<div className="blog-content__factoid">{child}</div>);
        }

        return child;
    });
    const classNames = [
        'blog-content',
        className || '',
    ].join(' ').trim();

    return (<div {...restProps} className={classNames}>{modifiedChildren}</div>);
};

export default BlogContent;
