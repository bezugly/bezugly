import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import './style.scss';

const BlogControls = ({ className, children }) => {
    const classNames = classnames('blog-controls', className);

    return (
        <div className={classNames}>{children}</div>
    );
};

BlogControls.group = ({ children }) => (
    <div className="blog-controls__group">
        {React.Children.map(children, item => (
            <div className="blog-controls__item">{item}</div>
        ))}
    </div>
);

BlogControls.right = ({ children }) => (
    <div className="blog-controls__right">{children}</div>
);

BlogControls.left = ({ children }) => (
    <div className="blog-controls__left">{children}</div>
);

BlogControls.group.defaultProps = {
    children: '',
};

BlogControls.group.propTypes = {
    children: PropTypes.oneOfType([
        PropTypes.node,
        PropTypes.arrayOf(PropTypes.node),
    ]),
};

BlogControls.right.defaultProps = {
    children: '',
};

BlogControls.right.propTypes = {
    children: PropTypes.oneOfType([
        PropTypes.node,
        PropTypes.arrayOf(PropTypes.node),
    ]),
};

BlogControls.left.defaultProps = {
    children: '',
};

BlogControls.left.propTypes = {
    children: PropTypes.oneOfType([
        PropTypes.node,
        PropTypes.arrayOf(PropTypes.node),
    ]),
};

BlogControls.defaultProps = {
    className: '',
    children: '',
};

BlogControls.propTypes = {
    className: PropTypes.string,
    children: PropTypes.oneOfType([
        PropTypes.node,
        PropTypes.arrayOf(PropTypes.node),
    ]),
};

export default BlogControls;
