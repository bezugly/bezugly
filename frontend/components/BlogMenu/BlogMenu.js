import React from 'react';
import PropTypes from 'prop-types';

import Link from 'ui/Typography/Link';
import Button from 'ui/Button';
import BlogControls from '../BlogControls';

const BlogMenu = ({
    createDraft,
    createDraftLoader,
    user,
    children,
}) => (
    <BlogControls>
        <BlogControls.left>
            <BlogControls.group>
                <Link className="blog-controls__text-link" theme="default" href="/blog" route>Все заметки</Link>
                {Boolean(user) && <Link theme="default" href="/blog/drafts" icon="tiles" route>Черновики</Link>}
                {Boolean(user) && <Button theme="default" size="small" icon="plus" noBorder onClick={createDraft} loading={createDraftLoader} route>Написать</Button>}
            </BlogControls.group>
        </BlogControls.left>
        {Boolean(children) && <BlogControls.right>{children}</BlogControls.right>}
    </BlogControls>
);

BlogMenu.defaultProps = {
    children: null,
    user: null,
};

BlogMenu.propTypes = {
    createDraft: PropTypes.func.isRequired,
    createDraftLoader: PropTypes.bool.isRequired,
    children: PropTypes.oneOfType([
        PropTypes.arrayOf(PropTypes.node),
        PropTypes.node,
    ]),
    user: PropTypes.shape(),
};

export default BlogMenu;
