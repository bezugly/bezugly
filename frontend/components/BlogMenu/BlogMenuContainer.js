import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import { createDraft } from '../../actions';

import BlogMenu from './BlogMenu';

class BlogMenuContainer extends Component {
    createDraft = () => {
        // eslint-disable-next-line
        const { createDraft, history } = this.props;

        return createDraft()
            .then(({ payload: draft }) => draft && history.push(`/blog/drafts/${draft.draftSlug}/edit`));
    };

    render() {
        const { createDraftLoader, user, children } = this.props;

        return (
            <BlogMenu
                createDraft={this.createDraft}
                createDraftLoader={createDraftLoader}
                user={user}
            >
                {children}
            </BlogMenu>
        );
    }
}

const mapStateToProps = ({ blog, auth }) => ({
    createDraftLoader: blog.loaders.createDraft,
    user: auth.user,
});

BlogMenuContainer.defaultProps = {
    children: null,
    user: null,
};

BlogMenuContainer.propTypes = {
    createDraft: PropTypes.func.isRequired,
    createDraftLoader: PropTypes.bool.isRequired,
    history: PropTypes.shape({
        push: PropTypes.func,
    }).isRequired,
    children: PropTypes.oneOfType([
        PropTypes.arrayOf(PropTypes.node),
        PropTypes.node,
    ]),
    user: PropTypes.shape(),
};

export default withRouter(connect(mapStateToProps, { createDraft })(BlogMenuContainer));
