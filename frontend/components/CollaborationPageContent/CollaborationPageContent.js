import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import Tabs from 'ui/Tabs';

import PromoText from '../PromoText';
import Company from './Company';
import Frontend from './Frontend';
import HR from './HR';

import './style.scss';

class CollaborationPageContent extends Component {
    static propTypes = {
        className: PropTypes.string,
        tab: PropTypes.string,
        historyPush: PropTypes.func,
    };

    static defaultProps = {
        className: '',
        tab: null,
        historyPush: null,
    };

    tabs = ['company', 'frontend', 'hr'];

    constructor(props) {
        super(props);

        const activeTab = this.tabs.indexOf(props.tab);

        this.state = {
            activeTab: activeTab !== -1 ? activeTab : 0,
        };
    }

    onChangeTab = (activeTab) => {
        const { historyPush } = this.props;

        if (this.tabs[activeTab] && historyPush) {
            historyPush(`/collaboration/${this.tabs[activeTab]}`);
        }
    };

    render() {
        const { className } = this.props;
        const { activeTab } = this.state;
        const classes = classnames(
            'collaboration-page',
            className,
        );

        return (
            <div className={classes}>
                <section className="page-layout__container">
                    <PromoText
                        className="collaboration-page__caption"
                        title="Как со&nbsp;мной поработать"
                        text="#JavaScript, #React, #Node.js, #Mongodb, #HTML, #CSS, #LESS, #SCSS, #Webpack, #Gulp, #Wordpress"
                    />
                </section>

                <section className="collaboration-page__content">
                    <Tabs size="large" container defaultActiveIndex={activeTab} onChange={this.onChangeTab}>
                        <Tabs.Tab index={0} title="Студии и&nbsp;компании" className="collaboration-page__company-section">
                            <Company />
                        </Tabs.Tab>
                        <Tabs.Tab index={1} title="Фронтендеру" className="collaboration-page__frontend-section">
                            <Frontend />
                        </Tabs.Tab>
                        <Tabs.Tab index={2} title="Рекрутеру" className="collaboration-page__hr-section">
                            <HR />
                        </Tabs.Tab>
                    </Tabs>
                </section>
            </div>
        );
    }
}

export default CollaborationPageContent;
