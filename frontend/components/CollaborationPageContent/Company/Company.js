import React, { Fragment } from 'react';
import { Helmet } from 'react-helmet';

import P from 'ui/Typography/P';
import H2 from 'ui/Typography/H2';
import H3 from 'ui/Typography/H3';
import Link from 'ui/Typography/Link';
import Blockquote from 'ui/Typography/Blockquote';

import ProjectPreview from '../../ProjectPreview';
import SmallCTA from '../../SmallCTA';

import robenPreview from '../../../images/main-preview-roben.png';
import robenPreview2x from '../../../images/main-preview-roben@2x.png';
import housePreview from '../../../images/main-house-preview.png';
import housePreview2x from '../../../images/main-house-preview@2x.png';
import reephos from '../../../images/collaboration-reephos-preview.jpg';
import reephos2x from '../../../images/collaboration-reephos-preview@2x.jpg';
import nlschool from '../../../images/collaboration-nl-school-preview.jpg';
import nlschool2x from '../../../images/collaboration-nl-school-preview@2x.jpg';
import summerHouse from '../../../images/collaboration-summerhouse-preview.jpg';
import summerHouse2x from '../../../images/collaboration-summerhouse-preview@2x.jpg';

const Company = () => (
    <Fragment>
        <Helmet>
            <title>Как студии и компании поработать с Дмитрием Безуглым</title>

            <meta property="og:title" content="Как студии и компании поработать с Дмитрием Безуглым" />
            <meta property="og:description" content="Заверстаю и запрограммирую интерфейс, поставлю сайт на вордпресс, сделаю лендинг с нуля. Дороже рядового фрилансера, но дешевле студии." />
        </Helmet>

        <div className="page-layout__container">
            <P size="large" className="collaboration-page__max-content">Заверстаю и&nbsp;запрограммирую интерфейс, поставлю сайт на&nbsp;вордпресс, сделаю лендинг с&nbsp;нуля. Дороже рядового фрилансера, но&nbsp;дешевле студии.</P>
        </div>

        <section>
            <div className="page-layout__container">
                <H2 className="collaboration-page__title">Интерфейс на&nbsp;React + Redux</H2>

                <div className="collaboration-page__react-projects">
                    <ProjectPreview
                        href="//roben.ru/dealers-list"
                        target="_blank"
                        image={{
                            sources: [robenPreview, robenPreview2x],
                            alt: 'Сайт roben.ru',
                        }}
                    >
                        <Fragment>
                            <H3><Link theme="default" inlineBlock href="//roben.ru/dealers-list" target="_blank" route={false}>Виджет для поиска дилеров</Link></H3>
                            <P size="normal">Заверстал, запрограммировал и&nbsp;поставил на&nbsp;вордпресс, чтобы пользователи сами вбивали адреса.</P>
                            <P size="normal">#Yandex Map API, #React, #Webpack, #SCSS, #Wordpress.</P>
                        </Fragment>
                    </ProjectPreview>

                    <ProjectPreview
                        href="/house-quiz"
                        target="_blank"
                        image={{
                            sources: [housePreview, housePreview2x],
                            alt: 'Сайт doma.bezugly.ru',
                        }}
                    >
                        <Fragment>
                            <H3><Link theme="default" inlineBlock href="/house-quiz" target="_blank" route={false}>Сайт-тест по&nbsp;выбору деревянных домов</Link></H3>
                            <P size="normal">Задизайнил и&nbsp;запрограммировал. Заявки приходят в&nbsp;телеграм и&nbsp;амоцрм.</P>
                            <P size="normal">#React, #Webpack, #SCSS, #Telegram API, #AMOCRM API.</P>
                        </Fragment>
                    </ProjectPreview>
                </div>
            </div>
        </section>

        <section>
            <div className="page-layout__container">
                <H2 className="collaboration-page__title">Верстка сайта и&nbsp;установка Wordpress</H2>

                <div className="collaboration-page__wordpress-projects">
                    <ProjectPreview
                        className="collaboration-page__wordpress-projects-item"
                        href="http://reephos.ru"
                        target="_blank"
                        image={{
                            sources: [reephos, reephos2x],
                            alt: 'Сайт roben.ru',
                        }}
                    >
                        <Fragment>
                            <H3><Link theme="default" inlineBlock href="http://reephos.ru" target="_blank" route={false}>Рифос</Link></H3>
                            <P size="normal">Заверстал, поставил на&nbsp;вордпресс и&nbsp;настроил отправку заявок на&nbsp;почту.</P>
                            <P size="normal">#LESS, #Wordpress.</P>
                        </Fragment>
                    </ProjectPreview>
                </div>
            </div>
        </section>

        <section>
            <div className="page-layout__container">
                <H2 className="collaboration-page__title">Лендинги с&nbsp;нуля</H2>

                <div className="collaboration-page__landing-projects">
                    <ProjectPreview
                        href="http://nl-school.ru"
                        target="_blank"
                        image={{
                            sources: [nlschool, nlschool2x],
                            alt: 'Сайт nl-school.ru',
                        }}
                    >
                        <Fragment>
                            <H3><Link theme="default" inlineBlock href="http://nl-school.ru" target="_blank" route={false}>Школа иностранных языков</Link></H3>
                            <P size="normal">Задал 110&nbsp;вопросов, собрал прототип, написал текст, задизайнил и&nbsp;запрограммировал.</P>
                        </Fragment>
                    </ProjectPreview>

                    <ProjectPreview
                        href="/summer-house"
                        target="_blank"
                        image={{
                            sources: [summerHouse, summerHouse2x],
                            alt: 'Сайт по продаже беседок',
                        }}
                    >
                        <Fragment>
                            <H3><Link theme="default" inlineBlock href="/summer-house" target="_blank" route={false}>Беседки из&nbsp;лиственницы</Link></H3>
                            <P size="normal">Задал 110&nbsp;вопросов, собрал прототип, написал текст, задизайнил и&nbsp;запрограммировал.</P>
                        </Fragment>
                    </ProjectPreview>
                </div>
            </div>
        </section>

        <section className="page-layout__container">
            <div className="collaboration-page__company-service">
                <div className="collaboration-page__company-service-column">
                    <P size="large">На&nbsp;связи весь день, но&nbsp;занимаюсь проектами по&nbsp;вечерам, примерно с&nbsp;7&nbsp;вечера.</P>
                    <P size="large">Комфортно работать как одному, так и&nbsp;в&nbsp;команде. Спокойно впишусь в&nbsp;аджайл.</P>
                    <P size="large">Работаю по&nbsp;предоплате 100%. Если проект большой, можем разбить на&nbsp;этапы и&nbsp;двигаться недельными итерациями с&nbsp;предоплатой за&nbsp;неделю.</P>
                </div>
                <div className="collaboration-page__company-service-column">
                    <P size="large">Понимаю, что у&nbsp;вас в&nbsp;проекте свои особенности. В&nbsp;любом случае пишите, подберем решение и&nbsp;условия.</P>
                </div>
            </div>
        </section>

        <section className="page-layout__container">
            <div className="collaboration-page__company-reviews">
                <Blockquote className="collaboration-page__company-reviews-item" title="Дмитрий Балаев">
                    <Fragment>
                        <P size="normal">Я&nbsp;имел опыт работы с&nbsp;Дмитрием на&nbsp;нескольких проектах&nbsp;&mdash; небольшие правки, разработка сложного сайта, разработка двух весьма сложных модулей на&nbsp;сайты&nbsp;WP.</P>
                        <P size="normal">Цены, которые назначает Дмитрий&nbsp;&mdash; выше средних по&nbsp;рынку, но&nbsp;в&nbsp;данном случае эти цифры берутся не&nbsp;с&nbsp;пустого места&nbsp;&mdash; меня искренне радуют профессионализм, ответственность и&nbsp;умение + желание решать настандартные сложные задачи.</P>
                        <P size="normal">Когда меня спрашивают о&nbsp;специалисте по&nbsp;WP&nbsp;&mdash; я&nbsp;всегда рекомендую Дмитрия как отличного специалиста, но&nbsp;предупреждаю, что он&nbsp;человек занятой + может укусить по&nbsp;финансам. Но&nbsp;&mdash; лично я&nbsp;всегда обращусь именно к&nbsp;нему в&nbsp;первую очередь.</P>
                    </Fragment>
                </Blockquote>

                <Blockquote className="collaboration-page__company-reviews-item" title="Максим Шипилло">
                    <Fragment>
                        <P size="normal">Заказывали сайт барбершопа для донесения информации об&nbsp;услугах салона красоты. Решил все ясно, четко и&nbsp;понятно, получилось на&nbsp;5.</P>
                        <P size="normal">Понравилась усидчивая работа, адекватно воспринимались все просьбы/вопросы и&nbsp;пожелания. Еще серьезный подход к&nbsp;делу: были сделаны презентации, изучен рынок требуемых услуг и&nbsp;также был проведен подробный анализ, что в&nbsp;итоге помогло нам в&nbsp;своей работе.</P>
                        <P size="normal">Рекомендовали нашему дизайнеру, который делал лого.</P>
                    </Fragment>
                </Blockquote>

                <Blockquote className="collaboration-page__company-reviews-item" title="Ярослав Мисонжников">
                    <Fragment>
                        <P size="normal">Заказывал сайт reephos.ru и&nbsp;neomart.ru</P>
                        <P size="normal">Результатом очень доволен. Дмитрий работал быстро и&nbsp;качественно, а&nbsp;если и&nbsp;возникали какие-то корректировки к&nbsp;работе&nbsp;&mdash; делал все оперативно.</P>
                        <P size="normal">Понравилось адекватное общение и&nbsp;четкие сроки.</P>
                        <P size="normal">Рекомендовал уже многим. Посоветовал&nbsp;бы четче ставить задачи, чтобы облегчить твою работу.</P>
                    </Fragment>
                </Blockquote>
            </div>
        </section>

        <SmallCTA />
    </Fragment>
);

export default Company;
