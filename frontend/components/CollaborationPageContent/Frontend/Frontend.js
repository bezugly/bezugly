import React, { Fragment } from 'react';
import { Helmet } from 'react-helmet';

import P from 'ui/Typography/P';
import H2 from 'ui/Typography/H2';
import Link from 'ui/Typography/Link';
import Blockquote from 'ui/Typography/Blockquote';
import Image from 'ui/Typography/Image';
import Factoid from 'ui/Factoid';

import BlogContent from '../../BlogContent';

import courseHacking1 from '../../../images/course-hacking-1.jpg';
import courseHacking12x from '../../../images/course-hacking-1@2x.jpg';
import courseHacking2 from '../../../images/course-hacking-2.jpg';
import courseHacking22x from '../../../images/course-hacking-2@2x.jpg';
import courseHacking3 from '../../../images/course-hacking-3.jpg';
import courseHacking32x from '../../../images/course-hacking-3@2x.jpg';

const Frontend = () => (
    <Fragment>
        <Helmet>
            <title>Как фронтендеру поработать с Дмитрием Безуглым</title>

            <meta property="og:title" content="Как фронтендеру поработать с Дмитрием Безуглым" />
            <meta property="og:description" content="Помогу пройти путь «новичок → стажер → младший разработчик»." />
        </Helmet>

        <div className="page-layout__container">
            <P size="large" className="collaboration-page__max-content">Помогу пройти путь &laquo;новичок &rarr; стажер &rarr; младший разработчик&raquo;.</P>
        </div>

        <section className="page-layout__container">
            <H2 className="collaboration-page__title">Записаться в&nbsp;студенты на&nbsp;интенсивы HTML Academy</H2>

            <BlogContent>
                <P size="normal">
                    <Link theme="default" href="//htmlacademy.ru" target="_blank" route={false}>HTML Academy</Link>
                    {' '}
                    воспитывает профессиональных фронтенд-разработчиков.
                </P>

                <P size="normal">Я&nbsp;наставник на&nbsp;интенсивах Академии и&nbsp;помогаю студентам сдавать проекты: проверяю, мотивирую, даю советы по&nbsp;разработке и&nbsp;работе.</P>

                <P size="normal">Помог 38&nbsp;студентам. Некоторые стали наставниками, устроились в&nbsp;Яндекс и&nbsp;Рамблер.</P>

                <P size="normal">Работаю только с&nbsp;теми, кто точно решил идти во&nbsp;фронтенд. Если вы&nbsp;просто попробовать,&nbsp;&mdash; не&nbsp;записывайтесь ко&nbsp;мне.</P>
            </BlogContent>
        </section>

        <section className="collaboration-page__academy-articles">
            <div className="page-layout__container">
                <P size="normal">Учиться в&nbsp;Академии сложно. Написал 3&nbsp;статьи, как пройти интенсивы и&nbsp;перейти из&nbsp;новичка в&nbsp;стажеры. Если готовы к&nbsp;такому&nbsp;&mdash; мы&nbsp;сработаемся.</P>

                <div className="collaboration-page__academy-articles-list">
                    <Link theme="default" className="collaboration-page__academy-articles-item" href="/blog/posts/course-hacking-1/">
                        <Image sources={[courseHacking1, courseHacking12x]} alt="Иллюстрация «До интенсива»" />
                        <span>До&nbsp;интенсива</span>
                    </Link>
                    <Link theme="default" className="collaboration-page__academy-articles-item" href="/blog/posts/course-hacking-2/">
                        <Image sources={[courseHacking2, courseHacking22x]} alt="Иллюстрация «Во время интенсива»" />
                        <span>Во&nbsp;время интенсива</span>
                    </Link>
                    <Link theme="default" className="collaboration-page__academy-articles-item" href="/blog/posts/course-hacking-3/">
                        <Image sources={[courseHacking3, courseHacking32x]} alt="Иллюстрация «После интенсива»" />
                        <span>После интенсива</span>
                    </Link>
                </div>
            </div>
        </section>

        <section className="collaboration-page__invitation page-layout__container">
            <P size="normal" className="collaboration-page__invitation-text">Чтобы я&nbsp;поменторил вас на&nbsp;интенсиве, напишите мне, я&nbsp;попрошу кураторов добавить вас.</P>
            <Factoid
                title="shalfey"
                text="Промокод со&nbsp;скидкой 800&nbsp;₽ на&nbsp;любой интенсив HTML Academy"
            />
        </section>

        <section className="page-layout__container">
            <div className="collaboration-page__frontend-reviews">
                <Blockquote className="collaboration-page__frontend-reviews-item">
                    <P size="normal">Наставник продвинутого HTML (Дмитрий Безуглый), по&nbsp;сравнению с&nbsp;наставником базового HTML, был демократичен и&nbsp;практически всегда был на&nbsp;связи, также оказывал учебное содействие в&nbsp;трудный момент. Как ученик, рекомендую выбирать данного наставника, при этом надо самому понимать что вам ближе: демократия или тоталитаризм.</P>
                </Blockquote>

                <Blockquote className="collaboration-page__frontend-reviews-item">
                    <P size="normal">Отличный наставник! Регулярные созвоны, онлайн почти всегда 24/7, реально проверяет твой код, а&nbsp;не&nbsp;просто поверхностно смотрит и&nbsp;пишет &laquo;ок&raquo;. Ведет свой блог, присылает рекомендуемые статьи для чтения. Дима, спасибо тебе!</P>
                </Blockquote>

                <Blockquote className="collaboration-page__frontend-reviews-item">
                    <P size="normal">Дима не&nbsp;давал лениться, следил за&nbsp;тем, чтобы работа сдавалась в&nbsp;срок. Был всегда доступен для вопросов. Созванивались мы&nbsp;столько сколько было нужно для решения проблемы, иногда по&nbsp;несколько раз в&nbsp;неделю. Работой с&nbsp;ним я&nbsp;доволен.</P>
                </Blockquote>
            </div>
        </section>

        <section className="page-layout__container">
            <H2 className="collaboration-page__title">Записаться на&nbsp;платную консультацию по&nbsp;скайпу</H2>

            <BlogContent>
                <Factoid
                    title="5000 руб."
                    text="Стоит час разговора"
                />

                <P size="normal">Посмотрю ваш код: дам советы, скину статьи, посоветую книги и&nbsp;отвечу на&nbsp;вопросы. Не&nbsp;буду писать код за&nbsp;вас, но&nbsp;расскажу, как&nbsp;бы я&nbsp;решил вашу задачу. Подскажу, что подтянуть и&nbsp;куда двигаться дальше.</P>

                <P size="normal">Расскажу, как попасть на&nbsp;работу: как пройти собеседование, к&nbsp;чему готовиться и&nbsp;что читать.</P>

                <P size="normal">Консультация отличается от&nbsp;интенсива Академии тем, что мы&nbsp;поговорим конкретно о&nbsp;вашей работе, а&nbsp;не&nbsp;учебной.</P>

                <P size="normal">Провожу консультацию только после оплаты. Возможно, мои советы вам не&nbsp;помогут и&nbsp;вы&nbsp;не&nbsp;узнаете ничего нового. К&nbsp;сожалению, я&nbsp;не&nbsp;смогу вернуть деньги за&nbsp;консультацию. Если вас это не&nbsp;пугает&nbsp;&mdash; пишите, договоримся о&nbsp;времени.</P>
            </BlogContent>
        </section>
    </Fragment>
);

export default Frontend;
