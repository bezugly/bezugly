import React, { Fragment } from 'react';
import { Helmet } from 'react-helmet';

import TrackerIcon from 'ui/Icon/Tracker';
import P from 'ui/Typography/P';
import H2 from 'ui/Typography/H2';
import Link from 'ui/Typography/Link';
import Factoid from 'ui/Factoid';

import { getWorkingTime } from '../../../helpers';

import BlogContent from '../../BlogContent';
import SmallCTA from '../../SmallCTA';

const HR = () => (
    <Fragment>
        <Helmet>
            <title>Как рекрутеру поработать с Дмитрием Безуглым</title>

            <meta property="og:title" content="Как рекрутеру поработать с Дмитрием Безуглым" />
            <meta property="og:description" content="Помогу с разработкой фронтенда и мобильных приложений на React, Redux и React Native." />
        </Helmet>

        <section className="page-layout__container">
            <P size="large" className="collaboration-page__max-content">Помогу с&nbsp;разработкой фронтенда и&nbsp;мобильных приложений на&nbsp;React, Redux и&nbsp;React Native.</P>
        </section>

        <section className="page-layout__container">
            <H2 className="collaboration-page__title">Javascript-разработка</H2>

            <BlogContent>
                <P size="normal">Пишу на&nbsp;чистом&nbsp;JS и&nbsp;Node.js. Знаю React+Redux, React Native, Express, BEM, Backbone, Pug, jQuery.</P>

                <P size="normal">Могу посчитать сложность алгоритма. Знаю, как решать задачи по&nbsp;науке, но&nbsp;я&nbsp;не&nbsp;олимпиадник.</P>

                <P size="normal">Верстаю семантично, адаптивно и&nbsp;кроссбраузерно. Спецификация не&nbsp;висит у&nbsp;меня на&nbsp;стене, но&nbsp;я&nbsp;знаю, как быстро проверить верстку на&nbsp;валидность и&nbsp;куда смотреть, если начался холивар по&nbsp;поводу &laquo;правильных&raquo; тегов.</P>

                <P size="normal">Верстаю макеты из&nbsp;зеплина, скетча, фотошопа, иллюстратора и&nbsp;индизайна.</P>

                <P size="normal">Стили пишу по&nbsp;БЭМ. Из&nbsp;препроцессоров использовал Sass, Less и&nbsp;Stylus. В&nbsp;препроцессорах использую переменные, вложенность, иногда миксины и&nbsp;редко циклы. Из&nbsp;PostCSS пробовал разные плагины, но&nbsp;прижился только автопрефиксер. Я&nbsp;вообще не&nbsp;любитель навешивать кучи плагинов, только по&nbsp;необходимости.</P>

                <P size="normal">Автоматизирую процессы с&nbsp;помощью Webpack, но&nbsp;я&nbsp;не&nbsp;любитель писать к&nbsp;нему конфиги. Еще знаю Gulp.</P>

                <P size="normal">Пользуюсь git и&nbsp;консолью. Работал в&nbsp;GitHub, Bitbucket и&nbsp;GitLab.</P>
            </BlogContent>
        </section>

        <section className="page-layout__container">
            <P size="large">{`Опыт работы ${getWorkingTime()}. Сейчас работаю в рекламном отделе ВКонтакте.`}</P>
        </section>

        <section className="page-layout__container">
            <div className="collaboration-page__hired-list">
                <div className="collaboration-page__hired-item">
                    <P size="large" className="collaboration-page__hired-title">
                        Работал в&nbsp;
                        <Link
                            theme="default"
                            href="//yandex.ru/tracker"
                            icon={<TrackerIcon />}
                            target="_blank"
                            inlineBlock
                            route={false}
                        >
                            Яндекс.Трекере
                        </Link>
                        {' '}
                        год
                    </P>
                    <P size="normal">Делал фронтенд мобильного приложения и&nbsp;сайта в&nbsp;большой крутой команде. Приложение на&nbsp;React Native, сайт на&nbsp;БЭМ.</P>
                </div>

                <div className="collaboration-page__hired-item">
                    <P size="large" className="collaboration-page__hired-title">
                        Работал в&nbsp;
                        <Link theme="default" href="//starline.ru" target="_blank" route={false}>СтарЛайне</Link>
                        {' '}
                        14&nbsp;месяцев
                    </P>
                    <P size="normal">
                        Делал фронтенд&nbsp;
                        <Link theme="default" href="//robofinist.ru" target="_blank" route={false}>сайта Робофинист</Link>
                        {' '}
                        один, работал в&nbsp;команде с&nbsp;бекенд-разработчиком и&nbsp;менеджером.
                    </P>
                </div>
            </div>

            <P size="large">До&nbsp;работы в&nbsp;компаниях фрилансил полтора года. Учился верстать, программировать на&nbsp;jQuery и&nbsp;общаться с&nbsp;клиентами. Ставил сайты на&nbsp;Wordpress, совсем немного писал на&nbsp;PHP. В&nbsp;компании не&nbsp;шел, потому что учился в&nbsp;университете.</P>
        </section>

        <section className="page-layout__container">
            <H2 className="collaboration-page__title">Свои проекты</H2>

            <BlogContent>
                <P size="normal">Помимо программирования, я&nbsp;увлекаюсь дизайном и&nbsp;ведением проектов.</P>

                <Factoid
                    title={<Link theme="default" href="/">bezugly.ru</Link>}
                    text="Мой сайт, с&nbsp;описанием проектов и&nbsp;блогом"
                />

                <P size="normal">Раньше вписывался в&nbsp;чужие проекты: продажа беседок из&nbsp;лиственницы, строительство деревянных домов, школа иностранных языков. Товарищ настраивал рекламу, а&nbsp;я&nbsp;делал сайты.</P>

                <P size="normal">Сейчас сконцентрировался на&nbsp;собственном продвижении и&nbsp;развитии себя как IT-специалиста. Стараюсь, чтобы все проекты варились в&nbsp;IT, чтобы не&nbsp;терять фокус.</P>
            </BlogContent>
        </section>

        <section className="page-layout__container">
            <H2 className="collaboration-page__title">Наставничество в&nbsp;HTML Academy</H2>

            <BlogContent>
                <P size="normal">В&nbsp;Университете ИТМО я&nbsp;вел занятия по&nbsp;вёрстке для одногруппников. Потом решил стать наставником в&nbsp;HTML&nbsp;Academy, где учат на&nbsp;профессиональных фронтенд-разработчиков.</P>

                <P size="normal">Наставничаю на&nbsp;курсах по&nbsp;фронтенду, передаю опыт работы в&nbsp;компаниях начинающим разработчикам. Обучение других помогает быстрее развиваться и&nbsp;учиться быть тим-лидом.</P>

                <P size="normal">Помог 38&nbsp;студентам. Максимум вел 10&nbsp;человек за&nbsp;интенсив. Чтобы вести много людей, добавил в&nbsp;работу немного аджайла: с&nbsp;планированием, индивидуальными созвонами и&nbsp;стендапами.</P>
            </BlogContent>
        </section>

        <SmallCTA />
    </Fragment>
);

export default HR;
