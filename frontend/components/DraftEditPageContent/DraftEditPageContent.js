import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';

import P from 'ui/Typography/P';
import Input from 'ui/Input';
import Textarea from 'ui/Textarea';
import Button from 'ui/Button';
import Spinner from 'ui/Spinner';
import PopupConfirm from 'ui/PopupConfirm';

import BlogMenu from '../BlogMenu';
import BlogControls from '../BlogControls';
import FilesAttach from '../FilesAttach';
import ImageAttach from '../ImageAttach';

import './style.scss';

class DraftEditPageContent extends Component {
    titleInput = React.createRef();

    componentDidMount() {
        this.titleInput.current.focus();
    }

    renderMarkdownPopupText = () => (
        <Fragment>
            <span className="blog-article__markdown-popup-item">
                <b className="blog-article__markdown-popup-title">Текст</b>
                <span className="blog-article__markdown-popup-text"># Заголовок первого уровня</span>
                <span className="blog-article__markdown-popup-text">## Заголовок второго уровня</span>
                <span className="blog-article__markdown-popup-text">### Заголовок третьего уровня</span>
            </span>

            <span className="blog-article__markdown-popup-item">
                <span className="blog-article__markdown-popup-text">**Жирный**</span>
                <span className="blog-article__markdown-popup-text">*Курсив*</span>
                <span className="blog-article__markdown-popup-text">{'> Крупный текст'}</span>
                <span className="blog-article__markdown-popup-text">`Код`</span>
            </span>

            <span className="blog-article__markdown-popup-item">
                <b className="blog-article__markdown-popup-title">Многострочный код</b>
                <span className="blog-article__markdown-popup-text">```</span>
                <span className="blog-article__markdown-popup-text">{'<h1>Привет</h1>'}</span>
                <span className="blog-article__markdown-popup-text">```</span>
            </span>

            <span className="blog-article__markdown-popup-item">
                <b className="blog-article__markdown-popup-title">Список</b>
                <span className="blog-article__markdown-popup-text">- Пункт 1</span>
                <span className="blog-article__markdown-popup-text">- Пункт 2</span>
            </span>

            <span className="blog-article__markdown-popup-item">
                <b className="blog-article__markdown-popup-title">Ссылка</b>
                <span className="blog-article__markdown-popup-text">[Текст ссылки](https://bezugly.ru)</span>
            </span>

            <span className="blog-article__markdown-popup-item">
                <b className="blog-article__markdown-popup-title">Изображение</b>
                <span className="blog-article__markdown-popup-text">![Подпись изображения](https://bezugly.ru)</span>
            </span>

            <span className="blog-article__markdown-popup-item">
                <b className="blog-article__markdown-popup-title">Таблица</b>
                <span className="blog-article__markdown-popup-text">| Заголовок 1 | Заголовок 2 |</span>
                <span className="blog-article__markdown-popup-text">| --- | --- |</span>
                <span className="blog-article__markdown-popup-text">| Текст 1 | Текст 2 |</span>
            </span>

            <span className="blog-article__markdown-popup-item">
                <b className="blog-article__markdown-popup-title">Фактоид</b>
                <span className="blog-article__markdown-popup-text">{'<Factoid title="Заголовок маркдаун" text="Текст маркдаун" />'}</span>
            </span>

            <span className="blog-article__markdown-popup-item">
                <b className="blog-article__markdown-popup-title">Текст в рамке</b>
                <span className="blog-article__markdown-popup-text">{'<Idea>Текст</Idea>'}</span>
            </span>
        </Fragment>
    );

    render() {
        const {
            draft,
            pageTitle,
            editStatus,
            onChangeText,
            onChangeSlug,
            onChangeFile,
            onChangeOGImage,
            onImageClick,
            uploadFilesLoader,
            slugError,
            deleteDraft,
            deleteDraftLoader,
            showDraft,
            showDraftLoader,
            updateDraftLoader,
            isOpenMarkdownPopup,
            openMarkdownPopup,
            closeMarkdownPopup,
            isOpenRemovePopup,
            openRemovePopup,
            closeRemovePopup,
        } = this.props;

        const isEmptyDraft = !draft.title && !draft.content && !draft.lead;

        return (
            <div className="page-layout__main blog-article-edit">
                <div className="page-layout__container">
                    <BlogMenu>
                        <BlogControls.group>
                            <Button
                                theme="primary"
                                size="small"
                                icon="preview"
                                onClick={showDraft}
                                disabled={isEmptyDraft}
                                loading={showDraftLoader}
                                route
                            >
                                Посмотреть, что получилось
                            </Button>
                        </BlogControls.group>
                    </BlogMenu>

                    <div className="blog-article-edit__status-controls">
                        <P size="smallest">{pageTitle}</P>
                        {updateDraftLoader
                            ? <Spinner theme="default" size="normal" className="blog-controls__status-loader" />
                            : Boolean(editStatus) && <P size="smallest" className="blog-controls__status-text">{editStatus}</P>
                        }
                    </div>

                    <section className="blog-article-edit">
                        <Textarea
                            size="large"
                            className="blog-article-edit__name-input"
                            name="title"
                            placeholder="Дим, как назовем эту заметку?"
                            onChange={onChangeText}
                            ref={this.titleInput}
                            value={draft.title}
                            autoComplete="false"
                        />

                        <Textarea
                            size="large"
                            className="blog-article-edit__lead-input"
                            name="lead"
                            placeholder="Подводка"
                            onChange={onChangeText}
                            value={draft.lead}
                        />

                        <Textarea
                            size="large"
                            className="blog-article-edit__content-input"
                            name="content"
                            placeholder="Ну, давай начнем…"
                            onChange={onChangeText}
                            value={draft.content}
                            maxRows={15}
                        />

                        <div className="blog-article-edit__files-attach">
                            <FilesAttach
                                files={draft.files}
                                onChange={onChangeFile}
                                onFileClick={onImageClick}
                                uploadFileLoader={uploadFilesLoader}
                            />

                            <PopupConfirm
                                onClose={closeMarkdownPopup}
                                isOpen={isOpenMarkdownPopup}
                                title="Помощь по маркдауну"
                                text={this.renderMarkdownPopupText()}
                                footer={<Button key={1} size="small" theme="default" onClick={closeMarkdownPopup}>Закрыть</Button>}
                            >
                                <Button className="blog-article__markdown-button" theme="black" size="normal" onClick={openMarkdownPopup} route>Маркдаун</Button>
                            </PopupConfirm>
                        </div>
                    </section>

                    <div className="blog-article-edit__footer-controls">
                        <div className="blog-article-edit__footer-item">
                            <label className="blog-article-edit__footer-item-label" htmlFor="slug">
                                <p className="blog-article-edit__footer-item-label-title">Доступно по&nbsp;ссылке</p>
                                <Input
                                    size="small"
                                    className="blog-article-edit__footer-item-input"
                                    name="slug"
                                    id="slug"
                                    value={draft.slug}
                                    onChange={onChangeSlug}
                                    autoComplete="false"
                                />
                                <p className="blog-article-edit__footer-item-label-title">{slugError}</p>
                            </label>
                        </div>

                        <div className="blog-article-edit__footer-item">
                            {/* eslint-disable-next-line */}
                            <label className="blog-article-edit__footer-item-label" htmlFor="created">
                                <p className="blog-article-edit__footer-item-label-title">Дата публикации</p>
                                <Input
                                    size="small"
                                    className="blog-article-edit__footer-item-input"
                                    name="created"
                                    type="date"
                                    id="created"
                                    value={draft.created}
                                    onChange={onChangeText}
                                />
                            </label>
                        </div>
                    </div>

                    <div className="blog-article-edit__og-image-attach">
                        <p className="blog-article-edit__footer-item-label-title">Изображение в соц. сетях</p>
                        <ImageAttach
                            image={draft.ogImage}
                            onChange={onChangeOGImage}
                        />
                    </div>

                    <div className="blog-article-edit__footer-buttons">
                        <PopupConfirm
                            onClose={closeRemovePopup}
                            isOpen={isOpenRemovePopup}
                            title="Удаление черновика"
                            text="Это искусство удалится навсегда."
                            footer={[
                                <Button key={1} size="small" theme="default" onClick={closeRemovePopup}>Не удалять</Button>,
                                <Button key={2} size="small" theme="danger" icon="trash" onClick={deleteDraft} disabled={deleteDraftLoader} loading={deleteDraftLoader}>Удалить</Button>,
                            ]}
                        >
                            <Button
                                className="blog-menu__link blog-article-edit__delete-button"
                                theme="danger"
                                size="small"
                                icon="trash"
                                onClick={openRemovePopup}
                            >
                                Удалить черновик
                            </Button>
                        </PopupConfirm>
                    </div>
                </div>
            </div>
        );
    }
}

DraftEditPageContent.propTypes = {
    draft: PropTypes.shape({
        title: PropTypes.string,
        content: PropTypes.string,
        lead: PropTypes.string,
        files: PropTypes.arrayOf(PropTypes.shape()),
        ogImage: PropTypes.shape(),
        slug: PropTypes.string,
        created: PropTypes.string,
    }).isRequired,
    pageTitle: PropTypes.node.isRequired,
    editStatus: PropTypes.string.isRequired,
    onChangeText: PropTypes.func.isRequired,
    onChangeSlug: PropTypes.func.isRequired,
    onChangeFile: PropTypes.func.isRequired,
    onChangeOGImage: PropTypes.func.isRequired,
    onImageClick: PropTypes.func.isRequired,
    deleteDraft: PropTypes.func.isRequired,
    showDraft: PropTypes.func.isRequired,
    uploadFilesLoader: PropTypes.bool.isRequired,
    deleteDraftLoader: PropTypes.bool.isRequired,
    updateDraftLoader: PropTypes.bool.isRequired,
    slugError: PropTypes.string.isRequired,
    showDraftLoader: PropTypes.bool.isRequired,
    isOpenMarkdownPopup: PropTypes.bool.isRequired,
    openMarkdownPopup: PropTypes.func.isRequired,
    closeMarkdownPopup: PropTypes.func.isRequired,
    isOpenRemovePopup: PropTypes.bool.isRequired,
    openRemovePopup: PropTypes.func.isRequired,
    closeRemovePopup: PropTypes.func.isRequired,
};

export default DraftEditPageContent;
