import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import Link from 'ui/Typography/Link';

import {
    updateDraft,
    checkSlug,
    deleteDraft,
    sendMessage,
} from '../../actions';
import { throttle, getInputDateFromISO, getISODateFromInput } from '../../helpers';

import DraftEditPageContent from './DraftEditPageContent';

class DraftEditPageContentContainer extends Component {
    isMounted = false;

    state = {
        isOpenMarkdownPopup: false,
        isOpenRemovePopup: false,
        updateDraftLoader: false,
        editStatus: '',
        slugError: '',
        title: '',
        content: '',
        lead: '',
        slug: '',
        created: '',
        files: [],
        ogImage: {},
        showDraftLoader: false,
    };

    constructor(props) {
        super(props);

        this.changes = {
            pending: false,
            subscribers: [],
        };

        const { draft, files, ogImage } = props;

        this.state.title = draft.title || '';
        this.state.content = draft.content || '';
        this.state.lead = draft.lead || '';
        this.state.slug = draft.slug;
        this.state.files = files;
        this.state.ogImage = ogImage;
        this.state.created = getInputDateFromISO(draft.created);

        this.updateDraft = throttle(this.updateDraft, 3000);
    }

    componentDidMount() {
        this.isMounted = true;
    }

    componentWillUnmount() {
        this.isMounted = false;
    }

    openMarkdownPopup = () => {
        if (this.isMounted) {
            this.setState({ isOpenMarkdownPopup: true });
        }
    };

    closeMarkdownPopup = () => {
        if (this.isMounted) {
            this.setState({ isOpenMarkdownPopup: false });
        }
    };

    openRemovePopup = () => {
        if (this.isMounted) {
            this.setState({ isOpenRemovePopup: true });
        }
    };

    closeRemovePopup = () => {
        if (this.isMounted) {
            this.setState({ isOpenRemovePopup: false });
        }
    };

    getPageTitle = () => {
        const { editedPost } = this.props;

        if (!editedPost) {
            return 'Новая заметка';
        }

        return (
            <Fragment>
                {'Рекдактирование заметки '}
                <Link theme="default" href={`/blog/posts/${editedPost.slug}`} route>{editedPost.title}</Link>
            </Fragment>
        );
    };

    onChangeText = (event) => {
        this.setTextToState(event, () => {
            this.changes.pending = true;

            this.updateDraft();
        });
    };

    setTextToState = (event, cb) => {
        const { name, value } = event.target;

        if (this.isMounted) {
            this.setState({ updateDraftLoader: true });
            this.setState({ [name]: value }, cb);
        }
    };

    updateDraft = () => {
        // eslint-disable-next-line
        const { updateDraft, draft } = this.props;

        const {
            title,
            content,
            lead,
            files,
            created,
            slug,
            ogImage,
        } = this.state;
        // eslint-disable-next-line
        const filesIds = files.map(file => file._id);
        const body = {
            title,
            content,
            lead,
            files: filesIds,
            slug,
            // eslint-disable-next-line
            ogImage: ogImage && ogImage._id ? ogImage._id : undefined,
            created: getISODateFromInput(created, draft.created),
        };

        // eslint-disable-next-line
        updateDraft(draft._id, body)
            .then(({ payload, error }) => {
                const message = payload ? 'Все изменения записаны' : `Не записал изменения: ${error}`;

                this.changes.pending = false;
                this.changes.subscribers.forEach(func => func());

                if (this.isMounted) {
                    this.setState({ updateDraftLoader: false });
                }

                return this.updateEditStatus(message);
            });
    };

    updateEditStatus = (message) => {
        if (this.isMounted) {
            this.setState({ editStatus: message });
        }
    };

    onChangeFile = (type, file) => {
        // eslint-disable-next-line
        const { files } = this.state;
        let modifiedFiles = [];

        switch (type) {
            case 'upload':
            case 'revert': {
                modifiedFiles = [...files, file];
                break;
            }

            case 'delete': {
                // eslint-disable-next-line
                modifiedFiles = files.filter(item => item._id !== file._id);
                break;
            }

            default: {
                console.log('onChangeFile', 'Нет подходящего типа');
            }
        }

        if (this.isMounted) {
            this.setState({ files: modifiedFiles }, () => this.updateDraft());
        }
    };

    onChangeOGImage = (ogImage) => {
        if (this.isMounted) {
            this.setState({ ogImage }, () => this.updateDraft());
        }
    };

    checkSlug = (event) => {
        this.setTextToState(event, () => {
            // eslint-disable-next-line
            const { checkSlug, editedPost } = this.props;
            const { slug } = this.state;

            return checkSlug(slug)
                .then(({ payload: checkedSlug }) => {
                    let slugError = checkedSlug ? 'Такой адрес уже есть' : '';

                    if (editedPost && editedPost.slug === checkedSlug) {
                        slugError = '';
                    }

                    if (this.isMounted) {
                        this.setState({ slugError });
                    }

                    return Promise.resolve();
                })
                .then(() => {
                    this.changes.pending = true;

                    return this.updateDraft();
                });
        });
    };

    onImageClick = (src) => {
        const { content } = this.state;
        const newContent = `${content}\n\n![](${src})`;

        if (this.isMounted) {
            this.setState({ content: newContent }, () => {
                this.changes.pending = true;

                this.updateDraft();
            });
        }
    };

    deleteDraft = async () => {
        if (this.changes.pending) {
            this.changes.subscribers.push(this.deleteDraft);
        } else {
            // eslint-disable-next-line
            const { history, deleteDraft, sendMessage, draft } = this.props;

            // eslint-disable-next-line
            await deleteDraft(draft._id);

            this.changes.subscribers = this.changes.subscribers
                .filter(func => func !== this.deleteDraft);

            sendMessage('success', 200, 'Черновик удален');

            history.push('/blog/drafts');
        }
    };

    showDraft = () => {
        if (this.changes.pending) {
            this.changes.subscribers.push(this.showDraft);

            if (this.isMounted) {
                this.setState({ showDraftLoader: true });
            }
        } else {
            // eslint-disable-next-line
            const { history, draft } = this.props;

            this.changes.subscribers = this.changes.subscribers
                .filter(func => func !== this.showDraft);

            if (this.isMounted) {
                this.setState({ showDraftLoader: false });
            }

            history.push(`/blog/drafts/${draft.draftSlug}`);
        }
    };

    render() {
        const {
            uploadFilesLoader,
            deleteDraftLoader,
        } = this.props;
        const {
            title,
            content,
            lead,
            slug,
            created,
            files,
            ogImage,
            editStatus,
            slugError,
            showDraftLoader,
            updateDraftLoader,
            isOpenMarkdownPopup,
            isOpenRemovePopup,
        } = this.state;
        const draft = {
            title,
            content,
            lead,
            slug,
            files,
            ogImage,
            created,
        };
        const pageTitle = this.getPageTitle();

        return (
            <DraftEditPageContent
                draft={draft}
                pageTitle={pageTitle}
                editStatus={editStatus}
                slugError={slugError}
                onChangeText={this.onChangeText}
                onChangeFile={this.onChangeFile}
                onChangeOGImage={this.onChangeOGImage}
                onChangeSlug={this.checkSlug}
                onImageClick={this.onImageClick}
                deleteDraft={this.deleteDraft}
                showDraft={this.showDraft}
                uploadFilesLoader={uploadFilesLoader}
                deleteDraftLoader={deleteDraftLoader}
                updateDraftLoader={updateDraftLoader}
                showDraftLoader={showDraftLoader}
                isOpenMarkdownPopup={isOpenMarkdownPopup}
                openMarkdownPopup={this.openMarkdownPopup}
                closeMarkdownPopup={this.closeMarkdownPopup}
                isOpenRemovePopup={isOpenRemovePopup}
                openRemovePopup={this.openRemovePopup}
                closeRemovePopup={this.closeRemovePopup}
            />
        );
    }
}

DraftEditPageContentContainer.defaultProps = {
    editedPost: null,
    ogImage: null,
};

DraftEditPageContentContainer.propTypes = {
    draft: PropTypes.shape({
        title: PropTypes.string,
        content: PropTypes.string,
        lead: PropTypes.string,
        slug: PropTypes.string,
        created: PropTypes.string,
    }).isRequired,
    editedPost: PropTypes.shape({
        slug: PropTypes.string,
        title: PropTypes.string,
    }),
    files: PropTypes.arrayOf(PropTypes.shape()).isRequired,
    ogImage: PropTypes.shape(),
    uploadFilesLoader: PropTypes.bool.isRequired,
    deleteDraftLoader: PropTypes.bool.isRequired,
    history: PropTypes.shape({
        push: PropTypes.func,
    }).isRequired,
};

const mapStateToProps = ({ blog }, props) => {
    let editedPost = null;

    if (props.draft.reference) {
        editedPost = Array.from(blog.posts.list.values())
            // eslint-disable-next-line
            .find(post => post._id === props.draft.reference);
    }

    return {
        uploadFilesLoader: blog.loaders.uploadFile,
        deleteDraftLoader: blog.loaders.deleteDraft,
        editedPost,
        ...props,
    };
};

export default withRouter(connect(
    mapStateToProps,
    {
        updateDraft,
        checkSlug,
        deleteDraft,
        sendMessage,
    },
)(DraftEditPageContentContainer));
