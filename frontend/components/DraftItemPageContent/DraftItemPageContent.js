import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import Button from 'ui/Button';
import P from 'ui/Typography/P';
import Link from 'ui/Typography/Link';
import Signature from 'ui/Typography/Signature';
import Spinner from 'ui/Spinner';
import PopupConfirm from 'ui/PopupConfirm';

import {
    publishDraft,
    replaceDraftWithPost,
    publishDraftAsNewPost,
    sendMessage,
} from '../../actions';
import { postOgImageSelectorFactory } from '../../selectors/blogSelectors';

import BlogMenu from '../BlogMenu';
import BlogControls from '../BlogControls';
import PostItemContent from '../PostItemContent';
import { formatDate } from '../../helpers';

import './style.scss';

class DraftItemPageContent extends Component {
    isMounted = false;

    state = {
        isOpenConfirm: false,
    };

    componentDidMount() {
        this.isMounted = true;
    }

    componentWillUnmount() {
        this.isMounted = false;
    }

    openConfirm = () => {
        if (this.isMounted) {
            this.setState({ isOpenConfirm: true });
        }
    };

    closeConfirm = () => {
        if (this.isMounted) {
            this.setState({ isOpenConfirm: false });
        }
    };

    publishDraft = () => {
        // eslint-disable-next-line
        const { publishDraft, sendMessage, draft, history } = this.props;

        // eslint-disable-next-line
        return publishDraft(draft._id)
            .then(({ payload: post, error }) => {
                if (error) {
                    return Promise.reject(error);
                }

                sendMessage('success', 200, 'Заметка опубликована');

                return history.push(`/blog/posts/${post.slug}`);
            });
    };

    replaceDraftWithPost = () => {
        // eslint-disable-next-line
        const { replaceDraftWithPost, sendMessage, editedPost, draft, history } = this.props;

        // eslint-disable-next-line
        return replaceDraftWithPost(draft)
            .then(({ payload: post, error }) => {
                if (error) {
                    return Promise.reject(error);
                }

                sendMessage('success', 200, `Заметка «${editedPost.title}» изменена`);

                return history.push(`/blog/posts/${post.slug}`);
            });
    };

    publishDraftAsNewPostLoader = () => {
        // eslint-disable-next-line
        const { publishDraftAsNewPost, sendMessage, draft, history } = this.props;

        // eslint-disable-next-line
        return publishDraftAsNewPost(draft)
            .then(({ payload: post, error }) => {
                if (error) {
                    return Promise.reject(error);
                }

                sendMessage('success', 200, 'Заметка опубликована как новая');

                return history.push(`/blog/posts/${post.slug}`);
            });
    };

    renderMenu = () => {
        const {
            draft,
            editedPost,
            publishDraftLoader,
            replaceDraftLoader,
            publishDraftAsNewPostLoader,
        } = this.props;
        const { isOpenConfirm } = this.state;
        const isEmptyDraft = !draft.title && !draft.content && !draft.lead;

        const group = editedPost ? [
            <Button
                key={1}
                theme="default"
                size="small"
                icon="planePlus"
                disabled={isEmptyDraft}
                onClick={this.publishDraftAsNewPostLoader}
                loading={publishDraftAsNewPostLoader}
                noBorder
                route
            >
                Опубликовать новый пост
            </Button>,
            <PopupConfirm
                onClose={this.closeConfirm}
                isOpen={isOpenConfirm}
                title="Изменение заметки"
                text="Заметка, которую видят пользователи, изменится на этот пост."
                footer={[
                    <Button key={1} size="small" theme="default" onClick={this.closeConfirm}>Оставить</Button>,
                    <Button
                        key={2}
                        theme="primary"
                        size="small"
                        icon="plane"
                        disabled={isEmptyDraft}
                        onClick={this.replaceDraftWithPost}
                        loading={replaceDraftLoader}
                        route
                    >
                        Изменить
                    </Button>,
                ]}
            >
                <Button theme="primary" size="small" icon="plane" onClick={this.openConfirm} route>Изменить пост</Button>
            </PopupConfirm>,
        ] : (
            <Fragment>
                <Button
                    theme="primary"
                    size="small"
                    icon="plane"
                    onClick={this.publishDraft}
                    loading={publishDraftLoader}
                    disabled={isEmptyDraft}
                    route
                >
                    Опубликовать
                </Button>
            </Fragment>
        );

        return (
            <BlogMenu>
                <BlogControls.group>
                    <Button
                        theme="default"
                        size="small"
                        icon="edit"
                        href={`/blog/drafts/${draft.draftSlug}/edit`}
                        noBorder
                        route
                    >
                        Редактировать
                    </Button>
                    {group}
                </BlogControls.group>
            </BlogMenu>
        );
    };

    renderMessage = () => {
        const { editedPost, draft } = this.props;
        const message = !editedPost
            ? `Будет опубликовано по адресу /${draft.slug}`
            : (
                <Fragment>
                    {'Этот черновик заменит пост '}
                    <Link theme="default" href={`/blog/posts/${editedPost.slug}`} route>{editedPost.title}</Link>
                </Fragment>
            );

        return <div className="blog-draft-page-content__message">{message}</div>;
    };

    render() {
        const { draft, ogImage, draftLoader } = this.props;

        return (
            <PostItemContent
                post={draft}
                menu={this.renderMenu()}
                message={this.renderMessage()}
            >
                {draftLoader && <Spinner theme="default" size="large" />}

                {Boolean(draft) && (
                    <Fragment>
                        <div className="blog-item__footer">
                            <Signature className="blog-item__date">{formatDate(draft.created)}</Signature>
                        </div>

                        <div className="blog-draft-page-content__social-image">
                            {ogImage ? (
                                <Fragment>
                                    <P size="smallest" className="blog-draft-page-content__social-image-text">Изображение для шаринга</P>
                                    <img src={`https://bezugly.ru/api/uploads/blog/${ogImage.name}`} alt={ogImage.name} />
                                </Fragment>
                            ) : (
                                <Fragment>
                                    <P size="smallest" className="blog-draft-page-content__social-image-text">Нет изображение для шаринга. Лучше добавь, а то будет некрасивый шаринг в соц. сеточки.</P>
                                    <Button
                                        theme="default"
                                        size="small"
                                        icon="edit"
                                        href={`/blog/drafts/${draft.draftSlug}/edit`}
                                        route
                                    >
                                        Вернуться к редактированию
                                    </Button>
                                </Fragment>
                            )}
                        </div>
                    </Fragment>
                )}
            </PostItemContent>
        );
    }
}

DraftItemPageContent.defaultProps = {
    editedPost: null,
    ogImage: null,
};

DraftItemPageContent.propTypes = {
    draft: PropTypes.shape({
        title: PropTypes.string,
        content: PropTypes.string,
        lead: PropTypes.string,
        slug: PropTypes.string,
        created: PropTypes.string,
    }).isRequired,
    editedPost: PropTypes.shape({
        slug: PropTypes.string,
        title: PropTypes.string,
    }),
    history: PropTypes.shape({
        push: PropTypes.func,
    }).isRequired,
    ogImage: PropTypes.shape({
        name: PropTypes.string,
    }),
    draftLoader: PropTypes.bool.isRequired,
    publishDraft: PropTypes.func.isRequired,
    publishDraftLoader: PropTypes.bool.isRequired,
    replaceDraftLoader: PropTypes.bool.isRequired,
    publishDraftAsNewPostLoader: PropTypes.bool.isRequired,
};

const mapStateToProps = () => {
    const ogImageSelector = postOgImageSelectorFactory('drafts');

    return (state, props) => {
        let editedPost = null;
        const { blog: { loaders } } = state;

        if (props.draft.reference) {
            editedPost = Array.from(state.blog.posts.list.values())
                // eslint-disable-next-line
                .find(post => post._id === props.draft.reference);
        }

        return {
            ogImage: ogImageSelector(state, props),
            publishDraftLoader: loaders.publishDraft,
            replaceDraftLoader: loaders.replaceDraftWithPost,
            publishDraftAsNewPostLoader: loaders.publishDraftAsNewPost,
            draftLoader: loaders.draft,
            editedPost,
            ...props,
        };
    };
};

export default withRouter(connect(
    mapStateToProps,
    {
        publishDraft,
        replaceDraftWithPost,
        publishDraftAsNewPost,
        sendMessage,
    },
)(DraftItemPageContent));
