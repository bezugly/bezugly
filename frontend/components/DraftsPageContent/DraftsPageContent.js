import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import P from 'ui/Typography/P';
import Link from 'ui/Typography/Link';
import Spinner from 'ui/Spinner';

import { getRightWord } from '../../helpers';

import PromoText from '../PromoText';
import BlogMenu from '../BlogMenu';

import './style.scss';

const renderList = list => (
    Array.from(list).map(([key, draft]) => (
        <Link key={key} theme="default" href={`/blog/drafts/${draft.draftSlug}`} className="blog-drafts__item">
            <div className="blog-drafts__item-paper">
                <div className="blog-drafts__item-content">
                    <pre className="blog-drafts__item-content-pre">{draft.content}</pre>
                </div>
            </div>
            <P size="normal" className="blog-drafts__item-title">{draft.title}</P>
        </Link>
    ))
);

const DraftsPageContent = ({ className, loader, drafts }) => {
    const classes = classnames(
        'blog-drafts',
        className,
    );
    const { list } = drafts;

    return (
        <div className={classes}>
            <div className="page-layout__container">
                <BlogMenu />

                <PromoText
                    title={
                        list && list.size
                            ? `${list.size}\u00A0${getRightWord(list.size, ['черновик', 'черновика', 'черновиков'])}`
                            : 'Черновики'
                    }
                />

                {loader && <Spinner theme="default" size="large" />}

                {list && list.size
                    ? (<div className="blog-drafts__list">{renderList(list)}</div>)
                    : (
                        <P size="large">
                            Черновиков пока нет&nbsp;
                            <span role="img" aria-label="Удивленное лицо">😱</span>
                        </P>
                    )
                }
            </div>
        </div>
    );
};

DraftsPageContent.defaultProps = {
    className: '',
};

DraftsPageContent.propTypes = {
    drafts: PropTypes.shape({
        list: PropTypes.instanceOf(Map).isRequired,
    }).isRequired,
    loader: PropTypes.bool.isRequired,
    className: PropTypes.string,
};

export default DraftsPageContent;
