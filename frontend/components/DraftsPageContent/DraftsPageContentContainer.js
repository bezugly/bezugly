import { connect } from 'react-redux';

import DraftsPageContent from './DraftsPageContent';

const mapStateToProps = ({ blog: { drafts, loaders } }) => ({ drafts, loader: loaders.drafts });

export default connect(mapStateToProps, null)(DraftsPageContent);
