import React, { Component } from 'react';
import PropTypes from 'prop-types';

import H1 from 'ui/Typography/H1';
import P from 'ui/Typography/P';
import Button from 'ui/Button';

class ErrorBoundary extends Component {
    static propTypes = {
        children: PropTypes.oneOfType([
            PropTypes.arrayOf(PropTypes.node),
            PropTypes.node,
        ]).isRequired,
    };

    state = {
        error: null,
    };

    componentDidCatch(error, info) {
        // TODO: отправить ошибку в метрику
        console.log('info', info.componentStack);

        this.setState({ error });
    }

    render() {
        const { error } = this.state;
        const { children } = this.props;

        if (error) {
            return (
                <div className="page-layout__main">
                    <div className="page-layout__container">
                        <div>
                            <H1>Ошибочка вышла</H1>
                            <P size="normal">{error.message}</P>
                        </div>

                        <div style={{ display: 'flex' }}>
                            <Button size="large" theme="primary" href="/">Перейти на главную</Button>
                        </div>
                    </div>
                </div>
            );
        }

        return children;
    }
}

export default ErrorBoundary;
