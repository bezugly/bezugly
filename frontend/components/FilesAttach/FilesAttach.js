import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import Icon from 'ui/Icon';
import Button from 'ui/Button';
import Spinner from 'ui/Spinner';

import './style.scss';

class FilesAttach extends Component {
    filesInput = React.createRef();

    renderFiles = () => {
        const { files, onFileClick, updateFile } = this.props;

        return Array.from(files.entries()).map(([key, {
            file,
            file: {
                _id: id,
                name,
                deleted,
            },
            type,
        }]) => {
            const itemClasses = classnames(
                'files-attach__item',
                { 'files-attach__item_type_horizontal': type === 'horizontal' },
                { 'files-attach__item_type_vertical': type === 'vertical' },
                { 'files-attach__item_type_archive': type === 'archive' },
                { 'files-attach__item_type_document': type === 'document' },
                { 'files-attach__item_deleted': deleted },
            );
            const buttonClasses = classnames(
                'files-attach__item-button',
                { 'files-attach__item-button_type_restore': deleted },
                { 'files-attach__item-button_type_remove': !deleted },
            );

            return (
                // eslint-disable-next-line
                <div className={itemClasses} key={key}>
                    <button type="button" className="files-attach__item-image" onClick={onFileClick(file)}>
                        <img src={`/api/uploads/blog/${name}`} alt={name} />
                    </button>

                    <p className="files-attach__item-title" title={name}>{name}</p>

                    <button type="button" className={buttonClasses} onClick={updateFile(id)}>
                        {deleted
                            ? (<Icon type="revert" title="Восстановить" />)
                            : (<Icon type="cross" title="Удалить" />)
                        }
                    </button>
                </div>
            );
        });
    };

    render() {
        const {
            files,
            accept,
            multiple,
            className,
            uploadFileLoader,
            onChangeFile,
        } = this.props;
        const classes = classnames(
            'files-attach',
            className,
        );

        return (
            <section className={classes}>
                <Button theme="black" size="normal" icon="clip" className="files-attach__file-button">
                    {/* eslint-disable-next-line */}
                    <label htmlFor="files-input">Прикрепить файл</label>
                </Button>
                <input
                    id="files-input"
                    type="file"
                    className="visually-hidden"
                    ref={this.filesInput}
                    accept={accept}
                    multiple={multiple}
                    onChange={onChangeFile}
                />

                <div className="files-attach__list">
                    {Boolean(files.size) && this.renderFiles()}
                    {uploadFileLoader && (
                        <div className="files-attach__loader">
                            <Spinner theme="default" size="normal" />
                        </div>
                    )}
                </div>
            </section>
        );
    }
}

FilesAttach.defaultProps = {
    className: '',
    onFileClick: () => {},
    updateFile: () => {},
};

FilesAttach.propTypes = {
    files: PropTypes.instanceOf(Map).isRequired,
    onChangeFile: PropTypes.func.isRequired,
    uploadFileLoader: PropTypes.bool.isRequired,
    accept: PropTypes.string.isRequired,
    multiple: PropTypes.bool.isRequired,
    className: PropTypes.string,
    onFileClick: PropTypes.func,
    updateFile: PropTypes.func,
};

export default FilesAttach;
