import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { uploadFile, updateFile } from '../../actions';

import FilesAttach from './FilesAttach';

import './style.scss';

class FilesAttachContainer extends Component {
    constructor(props) {
        super(props);

        // eslint-disable-next-line
        const files = props.files.reduce((map, file) => map.set(file._id, { file }), new Map());

        this.state = {
            files,
        };
    }

    onChangeFile = ({ target: { files } }) => {
        const { files: stateFiles } = this.state;
        const oldFiles = new Map(stateFiles);
        // eslint-disable-next-line
        const { uploadFile, onChange } = this.props;

        const images = Array.from(files).filter((file) => {
            const hasFileInState = Array.from(oldFiles.values())
                .some(({ file: item }) => item.name === file.name && item.size === file.size);

            return file.type.startsWith('image/') && !hasFileInState;
        });

        images.forEach(async (image) => {
            await uploadFile(image)
                .then(({ payload: file, error }) => {
                    if (!file) {
                        return Promise.reject(error);
                    }

                    const { _id } = file;
                    const element = document.createElement('img');
                    let type = 'horizontal';

                    element.onload = (event) => {
                        if (event.target.width <= event.target.height) {
                            type = 'vertical';

                            this.setState({
                                files: oldFiles.set(_id, {
                                    element,
                                    file,
                                    type,
                                }),
                            });
                        }
                    };

                    oldFiles.set(_id, {
                        element,
                        file,
                        type,
                    });

                    onChange('upload', file);

                    return this.setState({ files: oldFiles });
                });
        });
    };

    updateFile = id => () => {
        // eslint-disable-next-line
        const { updateFile, onChange } = this.props;
        const { files: stateFiles } = this.state;
        const oldFiles = new Map(stateFiles);
        const oldFile = oldFiles.get(id);

        const body = { deleted: !oldFile.file.deleted };

        return updateFile(id, body)
            .then(({ payload: file, error }) => {
                if (!file) {
                    return Promise.reject(error);
                }

                const newState = {
                    ...oldFile,
                    file,
                };

                oldFiles.set(id, newState);

                this.setState({ files: oldFiles });

                return onChange(oldFile.file.deleted ? 'revert' : 'delete', file);
            });
    };

    onFileClick = ({ name }) => () => {
        const { onFileClick } = this.props;

        onFileClick(`${window.location.origin}/api/uploads/blog/${name}`);
    };

    render() {
        const {
            className,
            accept,
            multiple,
            uploadFileLoader,
        } = this.props;
        const { files } = this.state;

        return (
            <FilesAttach
                className={className}
                files={files}
                onChangeFile={this.onChangeFile}
                accept={accept}
                multiple={multiple}
                uploadFileLoader={uploadFileLoader}
                onFileClick={this.onFileClick}
                updateFile={this.updateFile}
            />
        );
    }
}

const mapStateToProps = (_, props) => ({ ...props });

FilesAttachContainer.defaultProps = {
    className: '',
    accept: 'image/*',
    multiple: true,
    uploadFileLoader: true,
    onFileClick: () => {},
};

FilesAttachContainer.propTypes = {
    files: PropTypes.arrayOf(PropTypes.shape()).isRequired,
    onChange: PropTypes.func.isRequired,
    className: PropTypes.string,
    accept: PropTypes.string,
    multiple: PropTypes.bool,
    onFileClick: PropTypes.func,
    uploadFileLoader: PropTypes.bool,
};

export default connect(mapStateToProps, { uploadFile, updateFile })(FilesAttachContainer);
