import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { connect } from 'react-redux';

import Link from 'ui/Typography/Link';
import P from 'ui/Typography/P';

import { logoutUser } from '../../actions';

import LoginModal from '../LoginModal';

import './style.scss';

class Footer extends Component {
    static propTypes = {
        auth: PropTypes.shape().isRequired,
    };

    state = {
        modalOpen: false,
    };

    onLoginClick = () => {
        // eslint-disable-next-line
        const { auth, logoutUser } = this.props;

        if (auth.user) {
            logoutUser();
        } else {
            this.setState({ modalOpen: true });
        }
    };

    onLoginClose = () => {
        this.setState({ modalOpen: false });
    };

    render() {
        const { modalOpen } = this.state;
        const { auth } = this.props;
        const loginClasses = classnames(
            'footer__copyright',
            { footer__copyright_logged: auth.user },
        );

        return (
            <Fragment>
                <footer className="footer">
                    <div className="page-layout__container">
                        <div className="footer__content">
                            <div className="footer__row footer__row_space-between">
                                <section className="footer__column">
                                    <P size="small" className="footer__text" small="true">
                                        Подписывайтесь на&nbsp;
                                        <Link theme="default" href="http://ttttt.me/sexy_frontend" route={false} target="_blank">канал в телеграме</Link>
                                        . Рассказываю о фронтенде, делюсь лайфхаками
                                        и опытом работы в больших проектах.
                                    </P>
                                </section>

                                <section className="footer__column footer__column_copyright">
                                    <P size="small" className="footer__text" small="true">
                                        <button
                                            type="button"
                                            className={loginClasses}
                                            onClick={this.onLoginClick}
                                        >
                                            ©
                                        </button>
                                        &nbsp;Дмитрий Безуглый, 2014–
                                        {new Date().getFullYear()}
                                    </P>
                                </section>
                            </div>
                        </div>
                    </div>
                </footer>

                {modalOpen && <LoginModal onClose={this.onLoginClose} />}
            </Fragment>
        );
    }
}

const mapStateToProps = ({ auth }) => ({ auth });

export default connect(mapStateToProps, { logoutUser })(Footer);
