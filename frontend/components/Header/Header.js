import React from 'react';
import { NavLink } from 'react-router-dom';

import './style.scss';

const Header = () => (
    <header className="header">
        <nav className="page-layout__container">
            <ul className="header__list">
                <li className="header__list-item">
                    <NavLink className="main__link header__link" exact to="/">Дима Безуглый</NavLink>
                </li>
                <li className="header__list-item">
                    <NavLink activeClassName="header__link--active" className="main__link header__link" to="/blog">пишет заметки</NavLink>
                </li>
            </ul>
        </nav>
    </header>
);

export default Header;
