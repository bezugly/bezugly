import React from 'react';
import classnames from 'classnames';
import { NavLink } from 'react-router-dom';

const HeaderLink = ({ className, children, ...rest }) => {
    const classes = classnames(
        'header__link',
        className,
    );

    return (
        <NavLink
            className={classes}
            activeClassName="header__link_active"
            {...rest}
        >
            {children}
        </NavLink>
    );
};

export default HeaderLink;
