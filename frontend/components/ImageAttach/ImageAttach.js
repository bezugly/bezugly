import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import Spinner from 'ui/Spinner';

import './style.scss';

const ImageAttach = (props) => {
    const {
        className,
        image,
        uploadOgImageLoader,
        onChangeImage,
    } = props;
    const classes = classnames(
        'image-attach',
        className,
    );

    return (
        <Fragment>
            <input
                id="image-attach-input"
                type="file"
                className="visually-hidden image-attach__input"
                accept="image/*"
                onChange={onChangeImage}
            />
            <label htmlFor="image-attach-input" className={classes}>
                {/* eslint-disable-next-line */}
                {image && image._id
                    ? (<img className="image-attach__image" src={`/api/uploads/blog/${image.name}`} alt={image.name} />)
                    : (
                        <div className="image-attach__content">
                            {uploadOgImageLoader
                                ? (<div className="image-attach__loader"><Spinner theme="default" size="large" /></div>)
                                : (<div className="image-attach__icon" />)
                            }
                            <p className="image-attach__text">Нажмите, чтобы загрузить</p>
                        </div>
                    )
                }
            </label>
        </Fragment>
    );
};

ImageAttach.defaultProps = {
    className: '',
    image: null,
};

ImageAttach.propTypes = {
    image: PropTypes.shape(),
    uploadOgImageLoader: PropTypes.bool.isRequired,
    onChangeImage: PropTypes.func.isRequired,
    className: PropTypes.string,
};

export default ImageAttach;
