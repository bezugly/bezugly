import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { uploadOgImage } from '../../actions';

import ImageAttach from './ImageAttach';

import './style.scss';

class ImageAttachContainer extends Component {
    onChangeFile = ({ target: { files } }) => {
        // eslint-disable-next-line
        const { uploadOgImage, onChange } = this.props;

        const images = Array.from(files).filter(file => file.type.startsWith('image/'));

        images.forEach(async (image) => {
            await uploadOgImage(image)
                .then(({ payload: file, error }) => {
                    if (!file) {
                        return Promise.reject(error);
                    }

                    return onChange(file);
                });
        });
    };

    render() {
        const {
            className,
            image,
            uploadOgImageLoader,
        } = this.props;

        return (
            <ImageAttach
                className={className}
                image={image}
                uploadOgImageLoader={uploadOgImageLoader}
                onChangeImage={this.onChangeFile}
            />
        );
    }
}

const mapStateToProps = ({ blog }, props) => ({
    uploadOgImageLoader: blog.loaders.uploadOgImage,
    ...props,
});

ImageAttachContainer.defaultProps = {
    className: '',
    image: null,
};

ImageAttachContainer.propTypes = {
    image: PropTypes.shape(),
    onChange: PropTypes.func.isRequired,
    uploadOgImageLoader: PropTypes.bool.isRequired,
    uploadOgImage: PropTypes.func.isRequired,
    className: PropTypes.string,
};

export default connect(mapStateToProps, { uploadOgImage })(ImageAttachContainer);
