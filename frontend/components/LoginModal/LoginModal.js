import React, { Component } from 'react';
import PropTypes from 'prop-types';

import H2 from 'ui/Typography/H2';
import Button from 'ui/Button';
import Modal from 'ui/Modal';
import Input from 'ui/Input';

import './style.scss';

class LoginModal extends Component {
    input = React.createRef();

    componentDidMount() {
        this.input.current.focus();
    }

    onSubmit = (event) => {
        event.preventDefault();

        const { onSubmit } = this.props;

        onSubmit(this.input.current.val());
    };

    render() {
        const { onClose, loader, error } = this.props;

        return (
            <Modal className="login-modal" onClose={onClose}>
                <H2 className="login-modal__title">Димася, это ты?</H2>

                <form onSubmit={this.onSubmit}>
                    <label className="login-modal__label" htmlFor="login-password">
                        <Input
                            size="normal"
                            className="login-modal__input"
                            ref={this.input}
                            id="login-password"
                            type="password"
                            placeholder="Пароль"
                            required
                        />

                        {Boolean(error.length) && <span className="login-modal__error">{error}</span>}
                    </label>

                    <Modal.Footer>
                        <Button
                            size="normal"
                            theme="primary"
                            className="login-modal__button"
                            type="submit"
                            disabled={loader}
                            loading={loader}
                        >
                            Зайти
                        </Button>

                        <Button
                            size="normal"
                            theme="default"
                            className="login-modal__button"
                            onClick={onClose}
                        >
                            Отменить
                        </Button>
                    </Modal.Footer>
                </form>
            </Modal>
        );
    }
}

LoginModal.propTypes = {
    onClose: PropTypes.func.isRequired,
    loader: PropTypes.bool.isRequired,
    error: PropTypes.string.isRequired,
    onSubmit: PropTypes.func.isRequired,
};

export default LoginModal;
