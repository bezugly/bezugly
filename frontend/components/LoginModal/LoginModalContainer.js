import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { loginUser } from '../../actions';

import LoginModal from './LoginModal';

class LoginModalContainer extends PureComponent {
    state = {
        error: '',
    };

    logIn = (password) => {
        // eslint-disable-next-line
        const { loginUser, onClose } = this.props;

        // eslint-disable-next-line
        loginUser(password)
            .then(({ payload, status }) => {
                if (payload) {
                    return onClose();
                }

                return status === 404 && this.setState({ error: 'Чел, ты даже пароль не смог ввести.' });
            });
    };

    render() {
        const { onClose, loader } = this.props;
        const { error } = this.state;

        return <LoginModal onClose={onClose} loader={loader} error={error} onSubmit={this.logIn} />;
    }
}

LoginModalContainer.propTypes = {
    onClose: PropTypes.func.isRequired,
    loginUser: PropTypes.func.isRequired,
    loader: PropTypes.bool.isRequired,
};

const mapStateToProps = ({ auth }, props) => ({
    loader: auth.loaders.login,
    ...props,
});

export default connect(mapStateToProps, { loginUser })(LoginModalContainer);
