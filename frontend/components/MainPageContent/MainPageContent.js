/* eslint-disable react/jsx-one-expression-per-line */
import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { Link } from 'react-router-dom';
import mainImageDesktopWebp from '../../images/main-image-desktop.webp';
import mainImageDesktopJpg from '../../images/main-image-desktop.jpg';
import mainImageTabletWebp from '../../images/main-image-tablet.webp';
import mainImageTabletJpg from '../../images/main-image-tablet.jpg';
import sexyReactWebp from '../../images/sexy-react-mobile.webp';
import sexyReactJpg from '../../images/sexy-react-mobile.jpg';
import webDeveloperWebp from '../../images/web-developer-mobile.webp';
import webDeveloperJpg from '../../images/web-developer-mobile.jpg';
import './style.scss';

const MainPageContent = ({ className }) => {
    const classes = classnames(
        'main-page main',
        'page-layout__container',
        className,
    );

    return (
        <Fragment>
            <div className={classes}>
                <div className="main-image">
                    <picture>
                        <source srcSet={mainImageDesktopWebp} type="image/webp" media="(min-width: 1280px)" />
                        <source srcSet={mainImageDesktopJpg} type="image/jpeg" media="(min-width: 1280px)" />
                        <source srcSet={mainImageTabletWebp} type="image/webp" />
                        <source srcSet={mainImageTabletJpg} type="image/jpeg" />
                        <img className="main__image" src={mainImageTabletJpg} alt="Я у озера" />
                    </picture>
                </div>

                <div className="promo">
                    <h1 className="main__title">Фронтенд-тимлид в команде рекламы и бизнеса <a className="main__link" href="https://vk.com/sexy">ВКонтакте</a></h1>
                    <p className="main__text main__text--big">Пишу заметки, записываю курсы, запускаю проекты, помогаю разработчикам и вообще я молодец.</p>
                </div>

                <section className="courses">
                    <h2 className="main__title-secondary">Курсы</h2>

                    <div className="courses__list">
                        <a href="https://www.udemy.com/course/react-redux-2020/?referralCode=0C74053563423A398A3C" rel="noopener noreferrer" target="_blank" className="courses__item">
                            <picture>
                                <source srcSet={sexyReactWebp} type="image/webp" />
                                <source srcSet={sexyReactJpg} type="image/jpeg" />
                                <img className="main__image" src={sexyReactJpg} alt="Превью курса. Рассказываю, как работает браузер" />
                            </picture>
                            <p className="main__text"><span className="courses__fake-link">Из верстки во фронтенд!</span> Для тех, кто хочет попасть в топовую компанию. Про реакт, архитектуру и оптимизации.</p>
                        </a>
                        <a href="https://unirub.ru" rel="noopener noreferrer" target="_blank" className="courses__item">
                            <picture>
                                <source srcSet={webDeveloperWebp} type="image/webp" />
                                <source srcSet={webDeveloperJpg} type="image/jpeg" />
                                <img className="main__image" src={webDeveloperJpg} alt="Сижу рассказываю о курсе" />
                            </picture>
                            <p className="main__text">Профессия Веб-разработчик. Записал для <span className="courses__fake-link">Университета удалённых профессий Алекса Рубанова</span></p>
                        </a>
                    </div>
                </section>

                <section className="notes">
                    <h2 className="main__title-secondary">Популярные заметки</h2>

                    <div className="notes__content">
                        <ul className="notes__list">
                            <li><Link className="main__link notes__link" to="/blog/posts/emmet">Как писать HTML и CSS быстро: emmet</Link></li>
                            <li><Link className="main__link notes__link" to="/blog/posts/change-css-variables">Как еще использовать CSS-переменные</Link></li>
                            <li><Link className="main__link notes__link" to="/blog/posts/nostalgy-2015">Ностальгирую о прошлом</Link></li>
                            <li><Link className="main__link notes__link" to="/blog/posts/job-after-course">Гарантия трудоустройства на курсах</Link></li>
                        </ul>

                        {/* TODO: сделать подсчет количества заметок */}
                        <Link className="main__link main__text main__text--big notes__more-link" to="/blog">смотрите<br />все заметки</Link>
                    </div>
                </section>
            </div>

            <footer className="main-footer page-layout__container">
                <section className="subscribe">
                    <h2 className="main__title-secondary">Подписывайтесь на каналы sexy_frontend!</h2>
                    <p className="main__text subscribe__text"><span>Рассказываю о фронтенде, делюсь лайфхаками и опытом работы в больших проектах.</span><span>Выберите удобный канал.</span></p>

                    <ul className="subscribe__list">
                        <li><a className="main__text main__text--big subscribe__link" href="https://ttttt.me/sexy_frontend">Телеграм</a></li>
                        <li><a className="main__text main__text--big subscribe__link" href="https://vk.com/sexy_frontend">ВКонтакте</a></li>
                        <li><a className="main__text main__text--big subscribe__link" href="https://twitter.com/sexy_frontend">Твиттер</a></li>
                    </ul>
                </section>

                <section className="garage">
                    <h2 className="main__title-secondary">Гаражик</h2>
                    <p className="main))text"><Link className="main__link" to="/collaboration/hr">Резюме</Link>, <Link className="main__link" to="/collaboration/frontend">менторство</Link>, <Link className="main__link" to="/collaboration/company">портфолио</Link>.</p>

                    <ul className="garage__list">
                        <li className="garage__list-item"><a className="main__link garage__link" href="tel:79313541057">+7 931 354 10 57</a></li>
                        <li className="garage__list-item"><a className="main__link garage__link" href="https://ttttt.me/sexy_dima">@sexy_dima</a></li>
                    </ul>
                </section>
            </footer>
        </Fragment>
    );
};

MainPageContent.defaultProps = {
    className: '',
};

MainPageContent.propTypes = {
    className: PropTypes.string,
};

export default MainPageContent;
