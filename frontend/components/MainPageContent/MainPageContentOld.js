import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import H1 from 'ui/Typography/H1';
import H2 from 'ui/Typography/H2';
import H3 from 'ui/Typography/H3';
import Link from 'ui/Typography/Link';
import Image from 'ui/Typography/Image';
import P from 'ui/Typography/P';
import Blockquote from 'ui/Typography/Blockquote';
import Factoid from 'ui/Factoid';

import ProjectPreview from '../ProjectPreview';
import BlogContent from '../BlogContent';

import trackerPromoMobile from '../../images/tracker-mobile.png';
import trackerPromoMobile2x from '../../images/tracker-mobile@2x.png';
import trackerPromoWeb from '../../images/tracker-promo-web.png';
import trackerPromoWeb2x from '../../images/tracker-promo-web@2x.png';
import trackerPromoIssue from '../../images/tracker-promo-issue.png';
import trackerPromoIssue2x from '../../images/tracker-promo-issue@2x.png';
import trackerPromoAgile from '../../images/tracker-promo-agile.png';
import trackerPromoAgile2x from '../../images/tracker-promo-agile@2x.png';
import robofinistPreview from '../../images/main-robofinist-preview.png';
import robofinistPreview2x from '../../images/main-robofinist-preview@2x.png';
import robenPreview from '../../images/main-preview-roben.png';
import robenPreview2x from '../../images/main-preview-roben@2x.png';
import summerHousePreview from '../../images/main-summerhouse-preview.png';
import summerHousePreview2x from '../../images/main-summerhouse-preview@2x.png';
import housePreview from '../../images/main-house-preview.png';
import housePreview2x from '../../images/main-house-preview@2x.png';
import blogRunaway from '../../images/main-blog-runaway.png';
import blogRunaway2x from '../../images/main-blog-runaway@2x.png';

import './style-old.scss';

const MainPageContent = ({ className }) => {
    const classes = classnames(
        'main-page',
        className,
    );

    return (
        <div className={classes}>
            <section className="page-layout__container main-page__header">
                <div className="main-page__image-container">
                    <picture>
                        <source
                            media="(min-width: 992px)"
                            srcSet="/images/bezugly-desktop.jpg 1x, /images/bezugly-desktop@2x.jpg 2x"
                        />
                        <source
                            media="(min-width: 768px)"
                            srcSet="/images/bezugly-tablet.jpg 1x, /images/bezugly-tablet@2x.jpg 2x"
                        />
                        <img
                            className="main-page__header-image"
                            src="/images/bezugly-mobile.jpg"
                            srcSet="/images/bezugly-mobile@2x.jpg 2x"
                            alt="Дмитрий Безуглый в доме компании «Зингер»"
                        />
                    </picture>
                </div>
                <div className="main-page__header-content">
                    <H1 className="main-page__header-title">Дмитрий Безуглый</H1>
                    {/* eslint-disable-next-line */}
                    <P size="normal">Разработчик интерфейсов {<Link theme="default" href="//vk.com/about" target="_blank">vk.com</Link>} в&nbsp;команде рекламы.</P>
                </div>
            </section>

            <section className="page-layout__container">
                <H2 className="main-page__title main-page__promo-project-title">
                    Работал в&nbsp;
                    <Link
                        theme="default"
                        href="//yandex.ru/tracker"
                        icon="tracker"
                        target="_blank"
                        inlineBlock
                    >
                        Яндекс.Трекере
                    </Link>
                    {' '}
                    год
                </H2>

                <div className="main-page__promo-project">
                    <div className="main-page__promo-image main-page__promo-image--mobile">
                        <Link theme="black" href="//itunes.apple.com/us/app/%D1%8F%D0%BD%D0%B4%D0%B5%D0%BA%D1%81-%D1%82%D1%80%D0%B5%D0%BA%D0%B5%D1%80/id1351761680?mt=8" target="_blank">
                            <Image
                                className="main-page__image-mobile"
                                border={false}
                                sources={[trackerPromoMobile, trackerPromoMobile2x]}
                                alt="Мобильное приложение Трекера"
                            />
                        </Link>
                    </div>
                    <div className="main-page__promo-image main-page__promo-image--web">
                        <Link theme="black" href="//yandex.ru/tracker" target="_blank">
                            <Image
                                sources={[trackerPromoWeb, trackerPromoWeb2x]}
                                alt="Сайт Трекера"
                            />
                        </Link>
                    </div>
                    <div className="main-page__promo-image main-page__promo-image--issue">
                        <Link theme="black" href="//yandex.ru/tracker" target="_blank">
                            <Image
                                sources={[trackerPromoIssue, trackerPromoIssue2x]}
                                alt="Страница задач в Трекере"
                            />
                        </Link>
                    </div>
                    <div className="main-page__promo-image main-page__promo-image--agile">
                        <Link theme="black" href="//yandex.ru/tracker" target="_blank">
                            <Image
                                sources={[trackerPromoAgile, trackerPromoAgile2x]}
                                alt="Доска задач в Трекере"
                            />
                        </Link>
                    </div>
                    <P size="normal" className="main-page__promo-text">Делал фронтенд мобильного приложения и&nbsp;сайта. Приложение на&nbsp;React Native, сайт на&nbsp;БЭМ.</P>
                </div>
            </section>

            <section className="page-layout__container">
                <ProjectPreview
                    horizontal
                    href="//robofinist.ru"
                    target="_blank"
                    image={{
                        sources: [robofinistPreview, robofinistPreview2x],
                        alt: 'Сайт РобоФинист',
                    }}
                >
                    <Fragment>
                        <H3>
                            Работал в СтарЛайне над&nbsp;
                            <Link
                                theme="default"
                                href="//robofinist.ru"
                                route={false}
                                target="_blank"
                            >
                                сайтом РобоФинист
                            </Link>
                            {' '}
                            чуть больше года
                        </H3>
                        <P size="normal">
                            Делал фронтенд на&nbsp;Backbone, потом на&nbsp;React. Работал один.
                        </P>
                    </Fragment>
                </ProjectPreview>
            </section>

            <section className="page-layout__container">
                <BlogContent>
                    <H2 className="main-page__title">
                        Наставничаю в&nbsp;
                        <Link theme="default" href="//htmlacademy.ru" target="_blank">HTML Academy</Link>
                    </H2>

                    <Factoid
                        title="shalfey"
                        text={<P size="normal">Промокод со&nbsp;скидкой 800&nbsp;₽ на&nbsp;любой интенсив HTML Academy</P>}
                    />

                    <P size="normal">Веду студентов на&nbsp;курсах по&nbsp;верстке 1&nbsp;и&nbsp;2&nbsp;уровня, и&nbsp;программированию 1&nbsp;уровня. Всего помог 38&nbsp;студентам.</P>

                    <P size="normal">Чтобы вести много людей, добавил в&nbsp;работу немного аджайла: с&nbsp;планированием, индивидуальными созвонами и&nbsp;стендапами.</P>

                    <H3 className="main-page__reviews-title">Отзывы студентов</H3>

                    <div className="main-page__reviews">
                        <Blockquote className="main-page__reviews-item"><P size="normal">Наставник продвинутого HTML (Дмитрий Безуглый), по&nbsp;сравнению с&nbsp;наставником базового HTML, был демократичен и&nbsp;практически всегда был на&nbsp;связи, также оказывал учебное содействие в&nbsp;трудный момент. Как ученик, рекомендую выбирать данного наставника, при этом надо самому понимать что вам ближе: демократия или тоталитаризм.</P></Blockquote>
                        <Blockquote className="main-page__reviews-item"><P size="normal">Отличный наставник! Регулярные созвоны, онлайн почти всегда 24/7, реально проверяет твой код, а&nbsp;не&nbsp;просто поверхностно смотрит и&nbsp;пишет &laquo;ок&raquo;. Ведет свой блог, присылает рекомендуемые статьи для чтения. Дима, спасибо тебе!</P></Blockquote>
                        <Blockquote className="main-page__reviews-item"><P size="normal">Дима не&nbsp;давал лениться, следил за&nbsp;тем, чтобы работа сдавалась в&nbsp;срок. Был всегда доступен для вопросов. Созванивались мы&nbsp;столько сколько было нужно для решения проблемы, иногда по&nbsp;несколько раз в&nbsp;неделю. Работой с&nbsp;ним я&nbsp;доволен.</P></Blockquote>
                    </div>
                </BlogContent>
            </section>

            <section className="page-layout__container">
                <H2 className="main-page__title">Делаю коммерческие проекты</H2>

                <div className="main-page__commercial-list">
                    <ProjectPreview
                        className="main-page__commercial-item"
                        horizontal
                        href="//roben.ru/dealers-list"
                        target="_blank"
                        image={{
                            sources: [robenPreview, robenPreview2x],
                            alt: 'Сайт roben.ru',
                        }}
                    >
                        <Fragment>
                            <H3><Link theme="default" href="//roben.ru/dealers-list">Виджет для поиска дилеров</Link></H3>
                            <P size="normal">Заверстал, запрограммировал и&nbsp;поставил на&nbsp;вордпресс, чтобы пользователи сами вбивали адреса.</P>
                            <P size="normal">#Yandex Map API, #React, #Webpack, #SCSS, #Wordpress.</P>
                        </Fragment>
                    </ProjectPreview>

                    <ProjectPreview
                        className="main-page__commercial-item main-page__commercial-item--fixed-height"
                        href="/summer-house"
                        target="_blank"
                        image={{
                            sources: [summerHousePreview, summerHousePreview2x],
                            alt: 'Сайт по продаже беседок',
                        }}
                    >
                        <Fragment>
                            <H3><Link theme="default" href="/summer-house" target="_blank" route={false}>Сайт по&nbsp;продаже беседок</Link></H3>
                            <P size="normal">Написал текст, задизайнил и&nbsp;запрограммировал.</P>
                        </Fragment>
                    </ProjectPreview>

                    <ProjectPreview
                        className="main-page__commercial-item main-page__commercial-item--fixed-height"
                        href="/house-quiz"
                        target="_blank"
                        image={{
                            sources: [housePreview, housePreview2x],
                            alt: 'Сайт-тест по деревянным домам',
                        }}
                    >
                        <Fragment>
                            <H3><Link theme="default" href="/house-quiz" target="_blank" route={false}>Сайт-тест по&nbsp;выбору деревянных домов</Link></H3>
                            <P size="normal">Задизайнил и&nbsp;запрограммировал. Заявки приходят в&nbsp;телеграм и&nbsp;амоцрм.</P>
                            <P size="normal">#React, #Webpack, #SCSS, #Telegram API, #AMOCRM API.</P>
                        </Fragment>
                    </ProjectPreview>
                </div>
            </section>

            <section className="page-layout__container">
                <H2 className="main-page__title">Веду блог</H2>

                <div className="main-page__blog-list">
                    <div className="main-page__blog-item-title">
                        <P size="normal" className="main-page__blog-list-title"><b>Студентам HTML Academy</b></P>
                        <P size="normal" className="main-page__blog-link">
                            <Link theme="default" href="/blog/posts/course-hacking-1/" inlineBlock>Как пройти интенсив HTML&nbsp;Academy. Часть&nbsp;1: до&nbsp;интенсива</Link>
                        </P>
                    </div>
                    <P size="normal" className="main-page__blog-link">
                        <Link theme="default" href="/blog/posts/course-hacking-2/" inlineBlock>Как пройти интенсив HTML&nbsp;Academy. Часть&nbsp;2: во&nbsp;время интенсива</Link>
                    </P>
                    <P size="normal" className="main-page__blog-link">
                        <Link theme="default" href="/blog/posts/course-hacking-3/" inlineBlock>Как пройти интенсив HTML&nbsp;Academy. Часть&nbsp;3: после интенсива</Link>
                    </P>
                    <div className="main-page__blog-link main-page__blog-link--image">
                        <Link theme="default" href="/blog/posts/runaway/" image="true" inlineBlock>
                            <Image sources={[blogRunaway, blogRunaway2x]} alt="Иллюстрация «Хочется отдохнуть»" />
                            <span>Что делать, если хочется слиться</span>
                        </Link>
                    </div>
                    <P size="normal" className="main-page__blog-link">
                        <Link theme="default" href="/blog/posts/less-style-no-pain/" inlineBlock>Как писать меньше стилей и&nbsp;не&nbsp;бояться правок</Link>
                    </P>

                    <div className="main-page__blog-item-title">
                        <P size="normal" className="main-page__blog-list-title"><b>Верстка</b></P>
                        <P size="normal" className="main-page__blog-link">
                            <Link theme="default" href="/blog/posts/emmet/" inlineBlock>Как писать HTML и&nbsp;CSS быстро: emmet</Link>
                        </P>
                    </div>
                    <P size="normal" className="main-page__blog-link">
                        <Link theme="default" href="/blog/posts/safe-service/" inlineBlock>Страховка надежных сервисов</Link>
                    </P>
                    <P size="normal" className="main-page__blog-link">
                        <Link theme="default" href="/blog/posts/sass-or-less/" inlineBlock>Sass или Less?</Link>
                    </P>
                    <P size="normal" className="main-page__blog-link">
                        <Link theme="default" href="/blog/posts/google-first/" inlineBlock>Сначала погуглить, потом к&nbsp;наставнику</Link>
                    </P>
                    <P size="normal" className="main-page__blog-link">
                        <Link theme="default" href="/blog/posts/create-components/" inlineBlock>Проектируйте компоненты</Link>
                    </P>
                    <P size="normal" className="main-page__blog-link">
                        <Link theme="default" href="/blog/posts/easy-label/" inlineBlock>Простой лейбл с&nbsp;инпутом</Link>
                    </P>
                    <P size="normal" className="main-page__blog-link">
                        <Link theme="default" href="/blog/posts/bootstrap-grids/" inlineBlock>Хорошая сетка</Link>
                    </P>
                </div>
            </section>

            <section className="main-page__cta">
                <div className="page-layout__container">
                    <H1 className="main-page__cta-title">Давайте поработаем</H1>
                    <P size="large" className="main-page__cta-text">Работаю со&nbsp;студиями, компаниями и&nbsp;начинающими фронтендерами</P>

                    <div className="main-page__cta-links">
                        <Link theme="white" href="/collaboration/hr" className="main-page__cta-link">Рекрутеру ›</Link>
                        <Link theme="white" href="/collaboration/company" className="main-page__cta-link">Студии и&nbsp;компании ›</Link>
                        <Link theme="white" href="/collaboration/frontend" className="main-page__cta-link">Фронтендеру ›</Link>
                    </div>
                </div>
            </section>
        </div>
    );
};

MainPageContent.defaultProps = {
    className: '',
};

MainPageContent.propTypes = {
    className: PropTypes.string,
};

export default MainPageContent;
