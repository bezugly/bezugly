import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import PromoText from '../PromoText';

import './style.scss';

const NotFoundPageContent = ({ className }) => {
    const classes = classnames(
        'not-found-page',
        className,
    );

    return (
        <div className={classes}>
            <div className="page-layout__container">
                <PromoText
                    title="Страница не&nbsp;найдена"
                    text="Когда-нибудь сделаю тут креативно"
                />
            </div>
        </div>
    );
};

NotFoundPageContent.defaultProps = {
    className: '',
};

NotFoundPageContent.propTypes = {
    className: PropTypes.string,
};

export default NotFoundPageContent;
