import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';

import Header from '../Header';
import Footer from '../Footer';

import './style.scss';

class PageLayout extends Component {
    static propTypes = {
        children: PropTypes.oneOfType([
            PropTypes.arrayOf(PropTypes.node),
            PropTypes.node,
        ]),
        pathname: PropTypes.string.isRequired,
    };

    static defaultProps = {
        children: null,
    };

    componentDidUpdate({ pathname }) {
        const { pathname: currentPathname } = this.props;

        if (currentPathname !== pathname) {
            window.scrollTo(0, 0);
        }
    }

    render() {
        const { children, pathname } = this.props;

        return (
            <Fragment>
                <Helmet>
                    <meta property="og:url" content={`https://bezugly.ru${pathname}`} />
                    <meta property="og:image" content="https://bezugly.ru/images/bezugly.jpg" />
                </Helmet>

                <div className="page-layout">
                    <Header />

                    {children && <main>{children}</main>}

                    {pathname !== '/' && <Footer />}
                </div>
            </Fragment>
        );
    }
}

export default PageLayout;
