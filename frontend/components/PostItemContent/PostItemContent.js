import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Typograf from 'typograf';

import H1 from 'ui/Typography/H1';

import markdownToReact from '../hocs/markdownToReact';
import BlogContent from '../BlogContent';

import './style.scss';

const tp = new Typograf({ locale: ['ru', 'en-US'] });

class PostItemContent extends Component {
    componentDidMount() {
        const article = document.getElementById('blog-article');

        article.innerHTML = tp.execute(article.innerHTML);
    }

    render() {
        const {
            menu,
            post,
            message,
            children,
        } = this.props;

        return (
            <div className="page-layout__main blog-item">
                {Boolean(menu) && <div className="page-layout__container">{menu}</div>}

                {Boolean(message) && <div className="page-layout__container">{message}</div>}

                <div className="page-layout__container">
                    {post && (
                        <article id="blog-article">
                            <H1 className="blog-article__title">{post.title}</H1>

                            {Boolean(post.lead) && <div className="blog-article__lead">{markdownToReact(post.lead)}</div>}

                            {post.content && (
                                <BlogContent>{markdownToReact(post.content)}</BlogContent>
                            )}
                        </article>
                    )}

                    {children}
                </div>
            </div>
        );
    }
}

PostItemContent.defaultProps = {
    menu: null,
    message: null,
    children: null,
};

PostItemContent.propTypes = {
    post: PropTypes.shape().isRequired,
    menu: PropTypes.oneOfType([
        PropTypes.arrayOf(PropTypes.node),
        PropTypes.node,
    ]),
    children: PropTypes.oneOfType([
        PropTypes.arrayOf(PropTypes.node),
        PropTypes.node,
    ]),
    message: PropTypes.node,
};

export default PostItemContent;
