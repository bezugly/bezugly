import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter, Link as RouteLink } from 'react-router-dom';

import Button from 'ui/Button';
import PopupConfirm from 'ui/PopupConfirm';
import Signature from 'ui/Typography/Signature';
import Spinner from 'ui/Spinner/Spinner';
import P from 'ui/Typography/P/P';
import Link from 'ui/Typography/Link/Link';

import { deletePost, copyPost, sendMessage } from '../../actions';

import BlogMenu from '../BlogMenu';
import BlogControls from '../BlogControls';
import PostItemContent from '../PostItemContent';
import { formatDate } from '../../helpers';

class PostItemPageContent extends Component {
    isMounted = false;

    state = {
        isOpenConfirm: false,
    };

    componentDidMount() {
        this.isMounted = true;
    }

    componentWillUnmount() {
        this.isMounted = false;
    }

    openConfirm = () => {
        if (this.isMounted) {
            this.setState({ isOpenConfirm: true });
        }
    };

    closeConfirm = () => {
        if (this.isMounted) {
            this.setState({ isOpenConfirm: false });
        }
    };

    deletePost = async () => {
        // eslint-disable-next-line
        const { deletePost, copyPost, sendMessage, post, history } = this.props;

        this.closeConfirm();

        const copyBody = await copyPost(post);

        if (copyBody.error) {
            return Promise.reject(copyBody.error);
        }

        // eslint-disable-next-line
        const deleteBody = await deletePost(post._id);

        if (deleteBody.error) {
            return Promise.reject(deleteBody.error);
        }

        sendMessage('success', 200, 'Заметка скрыта от пользователей и находится в черновиках');

        return history.push('/blog');
    };

    editPost = () => {
        // eslint-disable-next-line
        const { copyPost, post, history } = this.props;

        return copyPost(post)
            .then(({ payload: draft, error }) => {
                if (error) {
                    return Promise.reject(error);
                }

                return history.push(`/blog/drafts/${draft.draftSlug}/edit`);
            });
    };

    renderMenu = () => {
        const { user } = this.props;
        const { isOpenConfirm } = this.state;

        return (
            <BlogMenu>
                {Boolean(user) && (
                    <Fragment>
                        <BlogControls.group>
                            <PopupConfirm
                                onClose={this.closeConfirm}
                                isOpen={isOpenConfirm}
                                title="Удаление заметки"
                                text="Пользователи не смогут увидеть заметку, но ты найдешь ее в черновиках"
                                footer={[
                                    <Button key={1} size="small" theme="default" onClick={this.closeConfirm}>Не удалять</Button>,
                                    <Button key={2} size="small" icon="trash" theme="danger" onClick={this.deletePost}>Удалить заметку</Button>,
                                ]}
                            >
                                <Button theme="danger" size="small" icon="trash" onClick={this.openConfirm} route>Удалить</Button>
                            </PopupConfirm>
                        </BlogControls.group>

                        <BlogControls.group>
                            <Button theme="primary" size="small" icon="edit" onClick={this.editPost} route>Редактировать</Button>
                        </BlogControls.group>
                    </Fragment>
                )}
            </BlogMenu>
        );
    };

    render() {
        const { post, postLoader } = this.props;

        return (
            <Fragment>
                <PostItemContent post={post} menu={this.renderMenu()}>
                    {postLoader && <Spinner theme="default" size="large" />}

                    {Boolean(post) && (
                        <Fragment>
                            <div className="blog-item__footer">
                                <Signature className="blog-item__date">{formatDate(post.created)}</Signature>
                            </div>

                            <ul className="footer__list">
                                <li className="footer__item footer__item--telegram">
                                    <P size="small">
                                        Самый свежак на&nbsp;
                                        <Link theme="default" href="http://ttttt.me/sexy_frontend" route={false} target="_blank">канале в телеграме</Link>
                                        . Лайфхаки, мысли, фронтенд — заходите!
                                    </P>
                                </li>
                            </ul>
                        </Fragment>
                    )}
                </PostItemContent>

                {Boolean(post) && (Boolean(post.prevPost) || Boolean(post.nextPost)) && (
                    <div className="blog-item__next-articles">
                        <div className="page-layout__container blog-item__next-articles__container">
                            <div className="blog-item__next-articles-item">
                                {Boolean(post.prevPost) && (
                                    <RouteLink className="blog-item__next-articles-link" to={`/blog/posts/${post.prevPost.slug}`}>
                                        <p className="blog-item__next-articles-label">Предыдущая заметка</p>
                                        <p className="blog-item__next-articles-text">{post.prevPost.title}</p>
                                    </RouteLink>
                                )}
                            </div>

                            <div className="blog-item__next-articles-item">
                                {Boolean(post.nextPost) && (
                                    <RouteLink className="blog-item__next-articles-link" to={`/blog/posts/${post.nextPost.slug}`}>
                                        <p className="blog-item__next-articles-label">Следующая заметка</p>
                                        <p className="blog-item__next-articles-text">{post.nextPost.title}</p>
                                    </RouteLink>
                                )}
                            </div>
                        </div>
                    </div>
                )}
            </Fragment>
        );
    }
}
const mapStateToProps = ({ blog, auth }) => ({
    user: auth.user,
    postLoader: blog.loaders.post,
});

PostItemPageContent.defaultProps = {
    user: null,
};

PostItemPageContent.propTypes = {
    post: PropTypes.shape({
        title: PropTypes.string,
        content: PropTypes.string,
        slug: PropTypes.string,
        created: PropTypes.string,
    }).isRequired,
    postLoader: PropTypes.bool.isRequired,
    deletePost: PropTypes.func.isRequired,
    copyPost: PropTypes.func.isRequired,
    history: PropTypes.shape({
        push: PropTypes.func,
    }).isRequired,
    user: PropTypes.shape(),
};

export default withRouter(connect(
    mapStateToProps,
    {
        deletePost,
        copyPost,
        sendMessage,
    },
)(PostItemPageContent));
