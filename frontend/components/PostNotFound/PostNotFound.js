import React from 'react';
import PropTypes from 'prop-types';

import H1 from 'ui/Typography/H1';
import Button from 'ui/Button';
import BlogMenu from '../BlogMenu';

import './style.scss';

const PostNotFound = ({
    error,
    createDraft,
    reload,
    createDraftLoader,
    user,
}) => (
    <div className="page-layout__main">
        <div className="page-layout__container"><BlogMenu /></div>

        <div className="page-layout__container">
            <H1 className="post-not-found__title">{`${error} 😥`}</H1>

            <div className="post-not-found__actions">
                {Boolean(user) && <Button theme="primary" size="normal" onClick={createDraft} loading={createDraftLoader}>Написать новую заметку</Button>}
                <Button theme="default" size="normal" onClick={reload}>Загрузить пост еще раз</Button>
                <Button theme="black" size="normal" href="/blog">К постам</Button>
            </div>
        </div>
    </div>
);

PostNotFound.defaultProps = {
    user: null,
};

PostNotFound.propTypes = {
    createDraft: PropTypes.func.isRequired,
    reload: PropTypes.func.isRequired,
    error: PropTypes.string.isRequired,
    createDraftLoader: PropTypes.bool.isRequired,
    user: PropTypes.shape(),
};

export default PostNotFound;
