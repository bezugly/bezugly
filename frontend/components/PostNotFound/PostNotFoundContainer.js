import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import { createDraft } from '../../actions';

import PostNotFound from './PostNotFound';

class PostNotFoundContainer extends Component {
    createDraft = () => {
        // eslint-disable-next-line
        const { createDraft, history, removeError } = this.props;

        return createDraft()
            .then(({ payload: draft }) => {
                removeError();

                return draft && history.push(`/blog/drafts/${draft.draftSlug}/edit`);
            });
    };

    reload = () => {
        window.location.reload();
    };

    render() {
        const { error, createDraftLoader, user } = this.props;

        return (
            <PostNotFound
                error={error}
                createDraft={this.createDraft}
                reload={this.reload}
                createDraftLoader={createDraftLoader}
                user={user}
            />
        );
    }
}

const mapStateToProps = ({ blog, auth }, { removeError }) => ({
    user: auth.user,
    createDraftLoader: blog.loaders.createDraft,
    removeError,
});

PostNotFoundContainer.defaultProps = {
    user: null,
};

PostNotFoundContainer.propTypes = {
    error: PropTypes.string.isRequired,
    removeError: PropTypes.func.isRequired,
    createDraftLoader: PropTypes.bool.isRequired,
    history: PropTypes.shape({
        push: PropTypes.func,
    }).isRequired,
    user: PropTypes.shape(),
};

export default withRouter(connect(mapStateToProps, { createDraft })(PostNotFoundContainer));
