import React from 'react';
import PropTypes from 'prop-types';
import { Link as RouteLink } from 'react-router-dom';

import H1 from 'ui/Typography/H1';
import P from 'ui/Typography/P';
import Signature from 'ui/Typography/Signature';
import Spinner from 'ui/Spinner';

import { formatDate } from '../../helpers';
import markdownToReact from '../hocs/markdownToReact';

import BlogMenu from '../BlogMenu';

import './style.scss';

const renderList = list => (
    Array.from(list).map(([key, post]) => (
        <RouteLink key={key} className="blog-list__article" to={`/blog/posts/${post.slug}`}>
            <div>
                <H1 className="blog-list__article-title">{post.title}</H1>

                {post.lead && (<div className="blog-list__article-lead">{markdownToReact(post.lead)}</div>)}

                <Signature className="blog-list__article-date">{formatDate(post.created)}</Signature>
            </div>
        </RouteLink>
    ))
);

const renderScreen = (loader, posts) => {
    if (loader) {
        return <Spinner theme="default" size="large" />;
    }

    return posts.list.size
        ? renderList(posts.list)
        : (
            <P size="large">
                Почитать пока нечего&nbsp;
                <span role="img" aria-label="Удивленное лицо">😱</span>
            </P>
        );
};

const PostsPageContent = ({ loader, posts }) => (
    <div className="page-layout__main blog-list">
        <section className="page-layout__container">
            <BlogMenu />

            {renderScreen(loader, posts)}
        </section>
    </div>
);

PostsPageContent.propTypes = {
    posts: PropTypes.shape({
        list: PropTypes.instanceOf(Map).isRequired,
    }).isRequired,
    loader: PropTypes.bool.isRequired,
};

export default PostsPageContent;
