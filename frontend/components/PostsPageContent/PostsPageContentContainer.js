import { connect } from 'react-redux';

import PostsPageContent from './PostsPageContent';

const mapStateToProps = ({ blog: { posts, loaders } }) => ({ posts, loader: loaders.posts });

export default connect(mapStateToProps, null)(PostsPageContent);
