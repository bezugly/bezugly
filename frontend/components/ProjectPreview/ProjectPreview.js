import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import Image from 'ui/Typography/Image';
import Link from 'ui/Typography/Link';

import './style.scss';

const onMouseOver = (e) => {
    const link = e.currentTarget;
    const parent = link.parentElement;
    const contentLinks = parent.querySelectorAll('.project-preview__content .link');

    Array.prototype.forEach.call(contentLinks, (item) => {
        if (item.href === link.href) {
            item.classList.add('link_hovered');
        }
    });
};

const onMouseOut = (e) => {
    const link = e.currentTarget;
    const parent = link.parentElement;
    const contentLinks = parent.querySelectorAll('.project-preview__content .link');

    Array.prototype.forEach.call(contentLinks, (item) => {
        if (item.href === link.href) {
            item.classList.remove('link_hovered');
        }
    });
};

const ProjectPreview = ({
    className,
    horizontal,
    target,
    image,
    href,
    children,
    route,
    ...restProps
}) => {
    const classes = classnames(
        'project-preview',
        { 'project-preview_horizontal': horizontal },
        className,
    );

    return (
        <section className={classes} {...restProps}>
            {image && (
                // eslint-disable-next-line
                <Link
                    theme="default"
                    href={href}
                    target={target}
                    className="project-preview__image"
                    onMouseOver={onMouseOver}
                    onMouseOut={onMouseOut}
                    route={route}
                >
                    <Image sources={image.sources} alt={image.alt} />
                </Link>
            )}

            {children && (
                <div className="project-preview__content">{children}</div>
            )}
        </section>
    );
};

ProjectPreview.defaultProps = {
    className: '',
    horizontal: false,
    route: false,
    target: '_self',
    href: '#',
    image: null,
    children: null,
};

ProjectPreview.propTypes = {
    className: PropTypes.string,
    horizontal: PropTypes.bool,
    route: PropTypes.bool,
    target: PropTypes.string,
    href: PropTypes.string,
    image: PropTypes.shape(),
    children: PropTypes.oneOfType([
        PropTypes.arrayOf(PropTypes.node),
        PropTypes.node,
    ]),
};

export default ProjectPreview;
