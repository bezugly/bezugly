import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import H1 from 'ui/Typography/H1';
import P from 'ui/Typography/P';

import './style.scss';

const PromoText = ({ className, title, text }) => {
    const classes = classnames(
        'promo',
        className,
    );

    return (
        <section className={classes}>
            {title && <H1 className="promo__title">{title}</H1>}
            {text && <P className="promo__text" size="large" dangerouslySetInnerHTML={{ __html: text }} />}
        </section>
    );
};

PromoText.defaultProps = {
    className: '',
    title: '',
    text: '',
};

PromoText.propTypes = {
    className: PropTypes.string,
    title: PropTypes.string,
    text: PropTypes.string,
};

export default PromoText;
