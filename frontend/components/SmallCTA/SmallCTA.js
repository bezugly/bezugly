import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import P from 'ui/Typography/P';
import Link from 'ui/Typography/Link';

import './style.scss';

const SmallCTA = ({ className }) => {
    const classes = classnames(
        'small-cta',
        className,
    );
    return (
        <section className={classes}>
            <div className="page-layout__container">
                <P size="large" className="small-cta__text">Расскажите о&nbsp;своем проекте</P>
                <Link theme="white" className="small-cta__link" href="mailto:d.e.bezugly@gmail.com" route={false}>d.e.bezugly@gmail.com</Link>
            </div>
        </section>
    );
};

SmallCTA.defaultProps = {
    className: '',
};

SmallCTA.propTypes = {
    className: PropTypes.string,
};

export default SmallCTA;
