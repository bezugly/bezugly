import React from 'react';

import NotFoundPage from '../../pages/NotFound';

export default ChildComponent => (props) => {
    const { user } = props;

    if (user) {
        return (<ChildComponent {...props} />);
    }

    return <NotFoundPage.component />;
};
