/* eslint-disable react/prop-types */
import React, { createElement } from 'react';
import marksy from 'marksy/jsx';
import hljs from 'highlight.js/lib/highlight';
import hljsJavascript from 'highlight.js/lib/languages/javascript';
import hljsHTML from 'highlight.js/lib/languages/xml';
import hljsCSS from 'highlight.js/lib/languages/css';
import hljsSCSS from 'highlight.js/lib/languages/scss';
import hljsLESS from 'highlight.js/lib/languages/less';

import H1 from 'ui/Typography/H1';
import H2 from 'ui/Typography/H2';
import H3 from 'ui/Typography/H3';
import P from 'ui/Typography/P';
import Ul from 'ui/Typography/list/Ul';
import Ol from 'ui/Typography/list/Ol';
import Table from 'ui/Typography/Table';
import Link from 'ui/Typography/Link';
import Image from 'ui/Typography/Image';
import Factoid from 'ui/Factoid';
import Idea from 'ui/Typography/Idea';
import Signature from 'ui/Typography/Signature';

import './style.scss';

hljs.registerLanguage('javascript', hljsJavascript);
hljs.registerLanguage('html', hljsHTML);
hljs.registerLanguage('css', hljsCSS);
hljs.registerLanguage('less', hljsLESS);
hljs.registerLanguage('scss', hljsSCSS);

const compile = marksy({
    createElement,
    highlight(language, code) {
        return hljs.highlight(language, code).value;
    },
    components: {
        Factoid({ title, text }) {
            const markedTitle = title ? compile(title).tree : '';
            const markedText = text ? compile(text).tree : '';

            return <Factoid title={markedTitle} text={markedText} />;
        },
        Idea({ children }) {
            return <Idea>{children}</Idea>;
        },
    },
    elements: {
        h1({ children }) {
            return <H1>{children}</H1>;
        },
        h2({ children }) {
            return <H2>{children}</H2>;
        },
        h3({ children }) {
            return <H3>{children}</H3>;
        },
        h4({ children }) {
            return <H3>{children}</H3>;
        },
        p({ children }) {
            if (Array.isArray(children) && !children[0] && !children[2]) {
                return children;
            }

            return <P size="normal">{children}</P>;
        },
        blockquote({ children }) {
            return <div className="blog-content__blockquote">{children}</div>;
        },
        hr() {
            return <hr />;
        },
        ol({ children }) {
            return <Ol>{children}</Ol>;
        },
        ul({ children }) {
            return <Ul>{children}</Ul>;
        },
        li({ children }) {
            return <Ol.li>{children}</Ol.li>;
        },
        table({ children }) {
            return <Table>{children}</Table>;
        },
        thead({ children }) {
            return <Table.Thead>{children}</Table.Thead>;
        },
        tbody({ children }) {
            return <Table.Tbody>{children}</Table.Tbody>;
        },
        tr({ children }) {
            return <Table.Tr>{children}</Table.Tr>;
        },
        th({ children }) {
            return <Table.Th>{children}</Table.Th>;
        },
        td({ children }) {
            return <Table.Td>{children}</Table.Td>;
        },
        a({
            href, title, children,
        }) {
            return <Link theme="default" href={href} title={title} target="_blank" route={false}>{children}</Link>;
        },
        strong({ children }) {
            return <strong>{children}</strong>;
        },
        em({ children }) {
            return <strong>{children}</strong>;
        },
        del({ children }) {
            return <del>{children}</del>;
        },
        img({ src, alt }) {
            return (
                <div className="blog-content__image-container">
                    <Image sources={[src]} alt={alt} />
                    {alt && <Signature>{compile(alt).tree}</Signature>}
                </div>
            );
        },
    },
});

const markdownToReact = (content = '') => compile(content).tree;

export default markdownToReact;
