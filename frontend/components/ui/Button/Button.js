import React, { Fragment } from 'react';
import classnames from 'classnames';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import Spinner from '../Spinner';
import Icon from '../Icon';

import './style.scss';

const renderIcon = (icon) => {
    if (typeof icon === 'string') {
        return <Icon className="button__icon" type={icon} />;
    }

    return <Icon className="button__icon">{icon}</Icon>;
};

const Button = ({
    theme,
    size,
    className,
    loading,
    icon,
    href,
    route,
    type,
    children,
    noBorder,
    ...restProps
}) => {
    const classes = classnames(
        'button',
        `button_theme_${theme}`,
        `button_size_${size}`,
        {
            'button_no-border': noBorder,
        },
        className,
    );

    const content = (
        <Fragment>
            {loading && <Spinner theme={theme} size={size} />}
            {icon && !loading && renderIcon(icon)}
            <span className="button__text">{children}</span>
        </Fragment>
    );

    if (route && href) {
        return (
            <Link to={href} className={classes} {...restProps}>{content}</Link>
        );
    }

    return React.createElement(
        href ? 'a' : 'button',
        {
            className: classes,
            href,
            type: href ? null : type,
            ...restProps,
        },
        content,
    );
};

Button.defaultProps = {
    className: '',
    children: '',
    type: 'button',
    disabled: false,
    loading: false,
    route: false,
    icon: null,
    href: null,
    onClick: null,
    noBorder: false,
};

Button.propTypes = {
    theme: PropTypes.oneOf(['default', 'primary', 'danger', 'black']).isRequired,
    size: PropTypes.oneOf(['small', 'normal', 'large']).isRequired,
    noBorder: PropTypes.bool,
    className: PropTypes.string,
    onClick: PropTypes.func,
    type: PropTypes.string,
    disabled: PropTypes.bool,
    loading: PropTypes.bool,
    route: PropTypes.bool,
    href: PropTypes.string,
    icon: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.node,
    ]),
    children: PropTypes.oneOfType([
        PropTypes.arrayOf(PropTypes.node),
        PropTypes.node,
    ]),
};

export default Button;
