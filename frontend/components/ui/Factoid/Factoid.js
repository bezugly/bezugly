import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import './style.scss';

const Factoid = ({
    className,
    title,
    text,
    ...restProps
}) => {
    const classes = classnames(
        'factoid',
        className,
    );

    return (
        <div className={classes} {...restProps}>
            <div className="factoid__title">{title}</div>
            <div className="factoid__content">{text}</div>
        </div>
    );
};

Factoid.displayName = 'Factoid';

Factoid.defaultProps = {
    className: '',
};

Factoid.propTypes = {
    className: PropTypes.string,
    title: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.node,
    ]).isRequired,
    text: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.node,
    ]).isRequired,
};

export default Factoid;
