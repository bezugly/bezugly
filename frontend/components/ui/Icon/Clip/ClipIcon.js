import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import './style.scss';
import svg from './clip-icon';

const ClipIcon = ({ classNames }) => {
    const classes = classnames(
        classNames,
        'icon__clip',
    );

    return (
        <span className={classes} dangerouslySetInnerHTML={{ __html: svg }} />
    );
};

ClipIcon.propTypes = {
    classNames: PropTypes.string.isRequired,
};

export default ClipIcon;
