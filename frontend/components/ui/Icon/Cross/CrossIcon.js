import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import './style.scss';
import svg from './cross-icon';

const CrossIcon = ({ classNames }) => {
    const classes = classnames(
        classNames,
        'icon__cross',
    );

    return (
        <span className={classes} dangerouslySetInnerHTML={{ __html: svg }} />
    );
};

CrossIcon.propTypes = {
    classNames: PropTypes.string.isRequired,
};

export default CrossIcon;
