export default `
<?xml version="1.0" encoding="UTF-8"?>
<svg viewBox="0 0 11 11" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g class="icon__color" transform="translate(-282.000000, -764.000000)">
            <g transform="translate(273.000000, 755.000000)">
                <polygon points="10.6 19.12 9.58 18.08 13.36 14.3 9.58 10.52 10.6 9.48 14.38 13.3 18.16 9.48 19.18 10.52 15.4 14.3 19.18 18.08 18.16 19.12 14.38 15.32"></polygon>
            </g>
        </g>
    </g>
</svg>
`;
