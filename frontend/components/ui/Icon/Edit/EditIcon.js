import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import './style.scss';
import svg from './edit-icon';

const EditIcon = ({ classNames }) => {
    const classes = classnames(
        classNames,
        'icon__edit',
    );

    return (
        <span className={classes} dangerouslySetInnerHTML={{ __html: svg }} />
    );
};

EditIcon.propTypes = {
    classNames: PropTypes.string.isRequired,
};

export default EditIcon;
