import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import './style.scss';

import SearchIcon from './Search';
import PlusIcon from './Plus';
import RevertIcon from './Revert';
import CrossIcon from './Cross';
import TilesIcon from './Tiles';
import TrashIcon from './Trash';
import EditIcon from './Edit';
import TrackerIcon from './Tracker';
import PlaneIcon from './Plane';
import ClipIcon from './Clip';
import PreviewIcon from './Preview';
import PlanePlus from './PlanePlus';

const types = {
    search: classes => <SearchIcon classNames={classes} />,
    plus: classes => <PlusIcon classNames={classes} />,
    revert: classes => <RevertIcon classNames={classes} />,
    cross: classes => <CrossIcon classNames={classes} />,
    tiles: classes => <TilesIcon classNames={classes} />,
    trash: classes => <TrashIcon classNames={classes} />,
    edit: classes => <EditIcon classNames={classes} />,
    tracker: classes => <TrackerIcon classNames={classes} />,
    plane: classes => <PlaneIcon classNames={classes} />,
    clip: classes => <ClipIcon classNames={classes} />,
    preview: classes => <PreviewIcon classNames={classes} />,
    planePlus: classes => <PlanePlus classNames={classes} />,
};

const Icon = ({
    className,
    type,
    title,
    children,
}) => {
    const classes = classnames(
        'icon',
        className,
    );

    if (typeof children === 'string') {
        return (
            <span
                className={classes}
                title={title}
                dangerouslySetInnerHTML={{ __html: children }}
            />
        );
    }

    return (
        <span className={classes} title={title}>
            {type && types[type]
                ? types[type](classes, title)
                : children
            }
        </span>
    );
};

Icon.defaultProps = {
    className: '',
    title: null,
    children: null,
    type: null,
};

Icon.propTypes = {
    className: PropTypes.string,
    title: PropTypes.string,
    children: PropTypes.node,
    type: PropTypes.oneOf([
        'search',
        'plus',
        'tracker',
        'revert',
        'cross',
        'tiles',
        'trash',
        'edit',
        'tracker',
        'plane',
        'clip',
        'preview',
        'planePlus',
    ]),
};

export default Icon;
