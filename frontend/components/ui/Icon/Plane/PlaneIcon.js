import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import './style.scss';
import svg from './plane-icon';

const PlaneIcon = ({ classNames }) => {
    const classes = classnames(
        classNames,
        'icon__plane',
    );

    return (
        <span className={classes} dangerouslySetInnerHTML={{ __html: svg }} />
    );
};

PlaneIcon.propTypes = {
    classNames: PropTypes.string.isRequired,
};

export default PlaneIcon;
