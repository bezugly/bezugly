import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import './style.scss';
import svg from './plane-plus-icon';

const PlanePlusIcon = ({ classNames }) => {
    const classes = classnames(
        classNames,
        'icon__plane-plus',
    );

    return (
        <span className={classes} dangerouslySetInnerHTML={{ __html: svg }} />
    );
};

PlanePlusIcon.propTypes = {
    classNames: PropTypes.string.isRequired,
};

export default PlanePlusIcon;
