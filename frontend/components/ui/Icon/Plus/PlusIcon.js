import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import './style.scss';
import svg from './plus-icon';

const PlusIcon = ({ classNames }) => {
    const classes = classnames(
        classNames,
        'icon__plus',
    );

    return (
        <span className={classes} dangerouslySetInnerHTML={{ __html: svg }} />
    );
};

PlusIcon.propTypes = {
    classNames: PropTypes.string.isRequired,
};

export default PlusIcon;
