import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import './style.scss';
import svg from './preview-icon';

const PreviewIcon = ({ classNames }) => {
    const classes = classnames(
        classNames,
        'icon__preview',
    );

    return (
        <span className={classes} dangerouslySetInnerHTML={{ __html: svg }} />
    );
};

PreviewIcon.propTypes = {
    classNames: PropTypes.string.isRequired,
};

export default PreviewIcon;
