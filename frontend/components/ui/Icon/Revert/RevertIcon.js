import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import './style.scss';
import svg from './revert-icon';

const RevertIcon = ({ classNames }) => {
    const classes = classnames(
        classNames,
        'icon__revert',
    );

    return (
        <span className={classes} dangerouslySetInnerHTML={{ __html: svg }} />
    );
};

RevertIcon.propTypes = {
    classNames: PropTypes.string.isRequired,
};

export default RevertIcon;
