import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import './style.scss';
import svg from './search-icon';

const SearchIcon = ({ classNames }) => {
    const classes = classnames(
        classNames,
        'icon__search',
    );

    return (
        <span className={classes} dangerouslySetInnerHTML={{ __html: svg }} />
    );
};

SearchIcon.propTypes = {
    classNames: PropTypes.string.isRequired,
};

export default SearchIcon;
