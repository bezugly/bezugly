export default `
<?xml version="1.0" encoding="UTF-8"?>
<svg viewBox="0 0 19 18" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g class="icon__color" transform="translate(-1308.000000, -129.000000)" fill-rule="nonzero" stroke-width="0.5">
            <g transform="translate(1285.000000, 120.000000)">
                <g transform="translate(24.000000, 10.000000)">
                    <g>
                        <path d="M17,16.39905 L10.4431,9.84215 C11.3492,8.79835 11.9,7.44005 11.9,5.95 C11.9,2.6639 9.23525,0 5.95,0 C2.66475,0 0,2.6639 0,5.95 C0,9.2361 2.6639,11.9 5.95,11.9 C7.44005,11.9 8.7992,11.3492 9.843,10.4431 L16.3999,17 L17,16.39905 Z M0.85,5.95 C0.85,3.2164 3.2351,0.85 5.95,0.85 C8.67425,0.85 11.05,3.0634 11.05,5.95 C11.05,8.6887 8.7227,11.05 5.95,11.05 C3.2674,11.05 0.85,8.8366 0.85,5.95 Z"></path>
                    </g>
                </g>
            </g>
        </g>
    </g>
</svg>
`;
