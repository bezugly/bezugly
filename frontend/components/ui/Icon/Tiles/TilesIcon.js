import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import './style.scss';
import svg from './tiles-icon';

const TilesIcon = ({ classNames }) => {
    const classes = classnames(
        classNames,
        'icon__tiles',
    );

    return (
        <span className={classes} dangerouslySetInnerHTML={{ __html: svg }} />
    );
};

TilesIcon.propTypes = {
    classNames: PropTypes.string.isRequired,
};

export default TilesIcon;
