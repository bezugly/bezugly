import React from 'react';
import PropTypes from 'prop-types';

import './style.scss';

const TrackerIcon = ({ className }) => (<span className={`tracker-icon ${className}`} />);

TrackerIcon.defaultProps = {
    className: '',
};

TrackerIcon.propTypes = {
    className: PropTypes.string,
};

export default TrackerIcon;
