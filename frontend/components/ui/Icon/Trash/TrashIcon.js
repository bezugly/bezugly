import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import './style.scss';
import svg from './trash-icon';

const TrashIcon = ({ classNames }) => {
    const classes = classnames(
        classNames,
        'icon__trash',
    );

    return (
        <span className={classes} dangerouslySetInnerHTML={{ __html: svg }} />
    );
};

TrashIcon.propTypes = {
    classNames: PropTypes.string.isRequired,
};

export default TrashIcon;
