import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import './style.scss';

class Input extends Component {
    static propTypes = {
        size: PropTypes.oneOf(['small', 'normal', 'large']).isRequired,
        className: PropTypes.string,
    };

    static defaultProps = {
        className: '',
    };

    constructor(props) {
        super(props);

        this.input = React.createRef();
    }

    val(newValue) {
        if (newValue) {
            this.input.current.value = newValue;
        }

        return this.input.current.value;
    }

    focus() {
        this.input.current.focus();
    }

    render() {
        const { size, className, ...restProps } = this.props;
        const classes = classnames(
            'input',
            `input_size_${size}`,
            className,
        );

        return (<input className={classes} ref={this.input} {...restProps} />);
    }
}

export default Input;
