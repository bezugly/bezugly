import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import classnames from 'classnames';
import PropTypes from 'prop-types';
import { CSSTransition, TransitionGroup } from 'react-transition-group';

import danger from './icon/danger';
import success from './icon/success';
import warning from './icon/warning';
import info from './icon/info';

import { removeMessage } from '../../../actions';

import './style.scss';

const icon = {
    default: info,
    success,
    warning,
    danger,
};

class Message extends PureComponent {
    timeouts = new Map();

    componentDidUpdate() {
        // eslint-disable-next-line
        const { messages, removeMessage } = this.props;
        const lastMessage = messages[messages.length - 1];

        if (lastMessage) {
            const id = setTimeout(() => {
                removeMessage(lastMessage.id);

                this.timeouts.delete(lastMessage.id);
            }, 5000);

            this.timeouts.set(lastMessage.id, id);
        }
    }

    onClick = messageId => () => {
        // eslint-disable-next-line
        const { removeMessage } = this.props;

        clearTimeout(this.timeouts.get(messageId));

        this.timeouts.delete(messageId);

        removeMessage(messageId);
    };

    render() {
        const {
            className,
            messages,
        } = this.props;
        const classes = classnames(
            'message',
            className,
        );

        return (
            <div className={classes}>
                <div className="message__container">
                    <TransitionGroup>
                        {messages.map(message => (
                            <CSSTransition
                                in
                                timeout={200}
                                classNames="message-animation"
                                key={message.id}
                            >
                                <button type="button" className="message__item" onClick={this.onClick(message.id)}>
                                    <span
                                        className={`message__icon message__icon_${message.theme}`}
                                        dangerouslySetInnerHTML={{ __html: icon[message.theme] }}
                                    />
                                    {message.text}
                                </button>
                            </CSSTransition>
                        ))}
                    </TransitionGroup>
                </div>
            </div>
        );
    }
}

Message.defaultProps = {
    className: '',
};

Message.propTypes = {
    className: PropTypes.string,
    messages: PropTypes.arrayOf(PropTypes.shape({
        id: PropTypes.string,
        theme: PropTypes.oneOf(['default', 'success', 'danger', 'warning']),
        text: PropTypes.string,
    })).isRequired,
};

const mapStateToProps = ({ messages }) => ({ messages });

const mapDispatchToProps = dispatch => ({
    removeMessage: id => dispatch(removeMessage(id)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Message);
