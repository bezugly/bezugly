export default `
<?xml version="1.0" encoding="UTF-8"?>
<svg viewBox="0 0 86 86" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g class="message__icon-color" transform="translate(-894.000000, -438.000000)" fill-rule="nonzero">
            <g transform="translate(894.000000, 438.000000)">
                <polygon points="36.6 46.9 27.5 37.8 18.9 46.4 36.6 64.1 67.1 33.6 58.5 25"></polygon>
                <path d="M43,0 C19.3,0 0,19.3 0,43 C0,66.7 19.3,86 43,86 C66.7,86 86,66.7 86,43 C86,19.3 66.7,0 43,0 Z M43,78 C23.7,78 8,62.3 8,43 C8,23.7 23.7,8 43,8 C62.3,8 78,23.7 78,43 C78,62.3 62.3,78 43,78 Z"></path>
            </g>
        </g>
    </g>
</svg>

`;
