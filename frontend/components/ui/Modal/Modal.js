import React, { Component } from 'react';
import { createPortal } from 'react-dom';
import classnames from 'classnames';
import PropTypes from 'prop-types';

import './style.scss';

const Footer = ({
    children,
    ...props
}) => (
    <div className="modal__footer" {...props}>{children}</div>
);

Footer.propTypes = {
    children: PropTypes.oneOfType([
        PropTypes.arrayOf(PropTypes.node),
        PropTypes.node,
    ]).isRequired,
};

class Modal extends Component {
    static propTypes = {
        className: PropTypes.string,
        onClose: PropTypes.func.isRequired,
        children: PropTypes.oneOfType([
            PropTypes.arrayOf(PropTypes.node),
            PropTypes.node,
        ]),
    };

    static defaultProps = {
        className: '',
        children: null,
    };

    static Footer = Footer;

    root = document.createElement('div');

    componentDidMount() {
        document.body.appendChild(this.root);

        document.addEventListener('keydown', this.onKeyDown);
    }

    componentWillUnmount() {
        document.body.removeChild(this.root);

        document.removeEventListener('keydown', this.onKeyDown);
    }

    onKeyDown = ({ keyCode }) => {
        // ESC
        if (keyCode === 27) {
            const { onClose } = this.props;

            if (onClose) {
                onClose();
            }
        }
    };

    render() {
        const {
            className,
            children,
            ...restProps
        } = this.props;
        const classes = classnames(
            'modal',
            className,
        );

        return createPortal((
            <div className={classes} {...restProps}>
                <div className="modal__backdrop">
                    <div className="modal__content">{children}</div>
                </div>
            </div>
        ), this.root);
    }
}

export default Modal;
