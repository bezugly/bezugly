import React, { Component } from 'react';
import PropTypes from 'prop-types';

import './style.scss';

class PopupConfirm extends Component {
    componentDidUpdate(prevProps) {
        const { isOpen } = this.props;

        if (isOpen && !prevProps.isOpen) {
            document.body.addEventListener('click', this.onClose);
        }

        if (!isOpen && prevProps.isOpen) {
            document.body.removeEventListener('click', this.onClose);
        }
    }

    onClose = ({ target }) => {
        const { onClose } = this.props;

        if (!target.closest('.popup-confirm')) {
            onClose();
        }
    };

    render() {
        const {
            isOpen,
            children,
            title,
            text,
            footer,
        } = this.props;

        return (
            <div className="popup-confirm">
                {children}

                {isOpen && (
                    <div className="popup-confirm__popup">
                        {Boolean(title) && (
                            <h1 className="popup-confirm__title">{title}</h1>
                        )}
                        {Boolean(text) && <p className="popup-confirm__text">{text}</p>}
                        {Boolean(footer) && (
                            <div className="popup-confirm__actions">{footer}</div>
                        )}
                    </div>
                )}
            </div>
        );
    }
}

PopupConfirm.defaultProps = {
    title: '',
    text: '',
    footer: '',
    children: '',
};

PopupConfirm.propTypes = {
    isOpen: PropTypes.bool.isRequired,
    onClose: PropTypes.func.isRequired,
    title: PropTypes.node,
    text: PropTypes.node,
    footer: PropTypes.oneOfType([
        PropTypes.arrayOf(PropTypes.node),
        PropTypes.node,
    ]),
    children: PropTypes.oneOfType([
        PropTypes.arrayOf(PropTypes.node),
        PropTypes.node,
    ]),
};

export default PopupConfirm;
