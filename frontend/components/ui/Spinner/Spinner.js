import React from 'react';

import './style.scss';

import classnames from 'classnames';
import PropTypes from 'prop-types';

const Spinner = ({
    theme,
    size,
    className,
    ...restProps
}) => {
    const classes = classnames(
        'spinner',
        `spinner_theme_${theme}`,
        `spinner_size_${size}`,
        className,
    );

    return (
        <span className={classes} {...restProps} />
    );
};

Spinner.defaultProps = {
    className: '',
};

Spinner.propTypes = {
    theme: PropTypes.oneOf(['default', 'primary', 'danger']).isRequired,
    size: PropTypes.oneOf(['small', 'normal', 'large']).isRequired,
    className: PropTypes.string,
};

export default Spinner;
