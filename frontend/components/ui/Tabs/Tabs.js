import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import './style.scss';

import Button from '../Button';

const Tab = ({
    active,
    title,
    className,
    index,
    size,
    ...props
}) => (
    <Button
        className="tabs__button"
        key={index}
        theme={active ? 'primary' : 'default'}
        size={size}
        {...props}
    >
        {title}
    </Button>
);

Tab.defaultProps = {
    className: '',
};

Tab.propTypes = {
    title: PropTypes.string.isRequired,
    index: PropTypes.number.isRequired,
    className: PropTypes.string,
};

class Tabs extends Component {
    static propTypes = {
        defaultActiveIndex: PropTypes.number,
        className: PropTypes.string,
        onChange: PropTypes.func,
        container: PropTypes.bool,
        size: PropTypes.oneOf(['small', 'normal', 'large']).isRequired,
        children: PropTypes.oneOfType([
            PropTypes.arrayOf(PropTypes.node),
            PropTypes.node,
        ]).isRequired,
    };

    static defaultProps = {
        defaultActiveIndex: 0,
        className: '',
        onChange: null,
        container: false,
    };

    static Tab = Tab;

    state = {
        activeTabIndex: 0,
    };

    constructor(props) {
        super(props);

        this.state.activeTabIndex = props.defaultActiveIndex;

        this.tabsContainerRef = React.createRef();
    }

    componentDidMount() {
        this.scrollToActiveItem();
    }

    componentDidUpdate() {
        this.scrollToActiveItem();
    }

    onClick = activeTabIndex => () => {
        const { onChange } = this.props;

        this.setState({ activeTabIndex });

        if (onChange) {
            onChange(activeTabIndex);
        }
    };

    isActive = (index) => {
        const { activeTabIndex } = this.state;

        return index === activeTabIndex;
    };

    scrollToActiveItem = () => {
        const tabsContainer = this.tabsContainerRef.current;
        const activeTab = tabsContainer.querySelector('.button_theme_primary');

        // Ширина родителя — «окна», в котором прокручивается меню
        const tabsContainerWidth = tabsContainer.clientWidth;

        // Ширина активного пункта меню
        const activeTabWidth = activeTab.clientWidth;

        // Смещение активного пункта меню по горизонтали от левого края родителя
        const activeTabLeftOffset = activeTab.offsetLeft;

        // Проверяем, не влезает ли активный элемент в «окно»
        const isActiveTabInvisible = activeTabLeftOffset + activeTabWidth > tabsContainerWidth;

        if (isActiveTabInvisible) {
            // Прокручиваем родителя так, чтобы активный элемент стало точно по центру:
            // scrollLeft = activeElLeftOffset прокрутит активный элемент к левому краю «окна»;
            // scrollLeft = activeElLeftOffset - (scrollParentWidth / 2) прокрутит активный элемент
            // так, что он станет левым краем по центру «окна».
            tabsContainer.scrollLeft = activeTabLeftOffset
                - (tabsContainerWidth / 2) + (activeTabWidth / 2);
        } else {
            tabsContainer.scrollLeft = 0;
        }
    };

    render() {
        const {
            className,
            children,
            size,
            container,
        } = this.props;
        const tabsClasses = classnames(
            'tabs',
            className,
        );

        return (
            <Fragment>
                <nav className={tabsClasses}>
                    <div className={container && 'page-layout__container'}>
                        <div className="tabs__container" ref={this.tabsContainerRef}>
                            {React.Children.map(children, (child, index) => (
                                React.cloneElement(child, {
                                    active: this.isActive(index),
                                    onClick: this.onClick(index),
                                    size,
                                })
                            ))}
                        </div>
                    </div>
                </nav>

                {React.Children.map(children, (child, index) => {
                    const panesClasses = classnames(
                        'tabs__pane',
                        child.props.className,
                    );

                    return (
                        this.isActive(index) && (
                            <section className={panesClasses}>{child.props.children}</section>
                        )
                    );
                })}
            </Fragment>
        );
    }
}

export default Tabs;
