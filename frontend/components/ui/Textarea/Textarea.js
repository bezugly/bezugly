import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import './style.scss';

class Textarea extends Component {
    constructor(props) {
        super(props);

        this.textarea = React.createRef();
    }

    componentDidMount() {
        this.lineHeight = parseInt(window.getComputedStyle(this.textarea.current).lineHeight, 10);

        this.setHeight();
    }

    onChange = (props) => {
        const { onChange } = this.props;

        this.setHeight();

        if (onChange) {
            onChange(props);
        }
    };

    setHeight() {
        const { maxRows } = this.props;
        const textarea = this.textarea.current;
        textarea.style.height = '1px';

        let height = textarea.scrollHeight;
        const maxHeight = this.lineHeight * maxRows;

        height = height > maxHeight ? maxHeight : height;

        textarea.style.height = `${height}px`;
    }

    val(newValue) {
        if (newValue) {
            this.textarea.current.value = newValue;
        }

        return this.textarea.current.value;
    }

    focus() {
        this.textarea.current.focus();
    }

    render() {
        const {
            size,
            className,
            name,
            placeholder,
            value,
            autoComplete,
            children,
        } = this.props;
        const classes = classnames(
            'textarea',
            `textarea_size_${size}`,
            className,
        );

        return (
            <textarea
                className={classes}
                ref={this.textarea}
                onChange={this.onChange}
                name={name}
                placeholder={placeholder}
                value={value}
                autoComplete={autoComplete}
                rows={1}
            >
                {children}
            </textarea>
        );
    }
}

Textarea.defaultProps = {
    className: '',
    children: '',
    maxRows: 4,
    name: undefined,
    placeholder: undefined,
    value: undefined,
    autoComplete: undefined,
    onChange: undefined,
};

Textarea.propTypes = {
    size: PropTypes.oneOf(['small', 'normal', 'large']).isRequired,
    className: PropTypes.string,
    children: PropTypes.string,
    maxRows: PropTypes.number,
    name: PropTypes.string,
    placeholder: PropTypes.string,
    value: PropTypes.string,
    autoComplete: PropTypes.string,
    onChange: PropTypes.func,
};

export default Textarea;
