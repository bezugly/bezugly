import React from 'react';
import classnames from 'classnames';
import PropTypes from 'prop-types';

import './style.scss';

const Blockquote = ({
    className,
    title,
    children,
    ...restProps
}) => {
    const classes = classnames(
        'blockquote',
        className,
    );

    return (
        <blockquote className={classes} {...restProps}>
            <div className="blockquote__title">{title && (<span className="blockquote__title-text">{title}</span>)}</div>
            {children}
        </blockquote>
    );
};

Blockquote.defaultProps = {
    className: '',
    title: '',
    children: null,
};

Blockquote.propTypes = {
    className: PropTypes.string,
    title: PropTypes.string,
    children: PropTypes.oneOfType([
        PropTypes.arrayOf(PropTypes.node),
        PropTypes.node,
    ]),
};

export default Blockquote;
