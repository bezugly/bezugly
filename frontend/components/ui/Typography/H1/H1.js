import React from 'react';
import classnames from 'classnames';
import PropTypes from 'prop-types';

import './style.scss';

const H1 = ({ className, children, ...restProps }) => {
    const classes = classnames(
        'h1',
        className,
    );

    return (<h1 className={classes} {...restProps}>{children}</h1>);
};

H1.defaultProps = {
    className: '',
};

H1.propTypes = {
    className: PropTypes.string,
};

export default H1;
