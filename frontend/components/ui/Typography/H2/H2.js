import React from 'react';
import classnames from 'classnames';
import PropTypes from 'prop-types';

import './style.scss';

const H2 = ({ className, children, ...restProps }) => {
    const classes = classnames(
        'h2',
        className,
    );

    return (<h2 className={classes} {...restProps}>{children}</h2>);
};

H2.defaultProps = {
    className: '',
};

H2.propTypes = {
    className: PropTypes.string,
};

export default H2;
