import React from 'react';
import classnames from 'classnames';
import PropTypes from 'prop-types';

import './style.scss';

const H3 = ({ className, children, ...restProps }) => {
    const classes = classnames(
        'h3',
        className,
    );

    return (<h3 className={classes} {...restProps}>{children}</h3>);
};

H3.defaultProps = {
    className: '',
};

H3.propTypes = {
    className: PropTypes.string,
};

export default H3;
