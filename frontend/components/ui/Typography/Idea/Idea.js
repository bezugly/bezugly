import React from 'react';
import classnames from 'classnames';
import PropTypes from 'prop-types';

import './style.scss';

const Idea = ({ className, children, ...restProps }) => {
    const classes = classnames(
        'idea',
        className,
    );

    return (
        <p className={classes} {...restProps}>{children}</p>
    );
};

Idea.defaultProps = {
    className: '',
    children: null,
};

Idea.propTypes = {
    className: PropTypes.string,
    children: PropTypes.oneOfType([
        PropTypes.arrayOf(PropTypes.node),
        PropTypes.node,
    ]),
};

export default Idea;
