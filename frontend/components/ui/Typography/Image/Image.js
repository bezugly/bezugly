import React from 'react';
import classnames from 'classnames';
import PropTypes from 'prop-types';

import './style.scss';

const Image = ({
    className,
    sources,
    alt,
    border,
    inline,
}) => {
    const classes = classnames(
        'image',
        { 'image--border': border },
        className,
    );
    const images = sources
        .map((src, index) => {
            let newSrc = '';

            if (src[0] === '/' || src.startsWith('http')) {
                newSrc = `${src} ${index + 1}x`;
            } else {
                newSrc = `/${src} ${index + 1}x`;
            }

            return newSrc;
        })
        .join(', ');

    return React.createElement(
        inline ? 'span' : 'div',
        { className: classes },
        (
            <img
                srcSet={images}
                alt={alt}
            />
        ),
    );
};

Image.defaultProps = {
    className: '',
    alt: '',
    border: true,
    inline: false,
};

Image.propTypes = {
    sources: PropTypes.arrayOf(PropTypes.string).isRequired,
    className: PropTypes.string,
    alt: PropTypes.string,
    border: PropTypes.bool,
    inline: PropTypes.bool,
};

export default Image;
