import React, { Fragment } from 'react';
import classnames from 'classnames';
import PropTypes from 'prop-types';

import { Link as RouteLink } from 'react-router-dom';

import Icon from '../../Icon';

import './style.scss';

const renderIcon = (icon) => {
    if (typeof icon === 'string') {
        return <Icon className="link__icon" type={icon} />;
    }

    return <Icon className="link__icon">{icon}</Icon>;
};

const Link = ({
    theme,
    className,
    href,
    inlineBlock,
    icon,
    underline,
    children,
    route,
    ...restProps
}) => {
    const classes = classnames(
        'link',
        `link_theme_${theme}`,
        { 'link_inline-block': inlineBlock },
        { link_icon: icon },
        className,
    );
    const underlineClasses = classnames(
        'link__underline',
        { link__underline_line: underline },
    );

    const content = (
        <Fragment>
            {icon && renderIcon(icon)}
            <span className={underlineClasses}>{children}</span>
        </Fragment>
    );

    if (route) {
        return (
            <RouteLink to={href} className={classes} {...restProps}>
                {content}
            </RouteLink>
        );
    }
    return (
        <a href={href} className={classes} {...restProps}>
            {content}
        </a>
    );
};

Link.defaultProps = {
    className: '',
    href: null,
    inlineBlock: false,
    route: true,
    underline: true,
    icon: null,
    children: '',
};

Link.propTypes = {
    theme: PropTypes.oneOf(['default', 'black', 'white']).isRequired,
    className: PropTypes.string,
    href: PropTypes.string,
    inlineBlock: PropTypes.bool,
    route: PropTypes.bool,
    underline: PropTypes.bool,
    icon: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.node,
    ]),
    children: PropTypes.oneOfType([
        PropTypes.arrayOf(PropTypes.node),
        PropTypes.node,
    ]),
};

export default Link;
