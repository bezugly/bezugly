import React from 'react';
import classnames from 'classnames';
import PropTypes from 'prop-types';

import './style.scss';

const P = ({
    size,
    className,
    children,
    ...restProps
}) => {
    const classes = classnames(
        'text',
        `text_size_${size}`,
        className,
    );

    return (<p className={classes} {...restProps}>{children}</p>);
};

P.defaultProps = {
    className: '',
    children: null,
};

P.propTypes = {
    className: PropTypes.string,
    size: PropTypes.oneOf(['smallest', 'small', 'normal', 'large']).isRequired,
    children: PropTypes.oneOfType([
        PropTypes.arrayOf(PropTypes.node),
        PropTypes.node,
    ]),
};

export default P;
