import React from 'react';
import classnames from 'classnames';
import PropTypes from 'prop-types';

import './style.scss';

const Signature = ({ className, children, ...restProps }) => {
    const classes = classnames(
        'signature',
        className,
    );

    return (
        <div className={classes} {...restProps}>{children}</div>
    );
};

Signature.defaultProps = {
    className: '',
    children: null,
};

Signature.propTypes = {
    className: PropTypes.string,
    children: PropTypes.oneOfType([
        PropTypes.arrayOf(PropTypes.node),
        PropTypes.node,
    ]),
};

export default Signature;
