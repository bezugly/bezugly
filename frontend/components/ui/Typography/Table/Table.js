import React, { Component } from 'react';
import classnames from 'classnames';
import PropTypes from 'prop-types';

import './style.scss';

const Thead = ({ children, className }) => {
    const classes = classnames(
        'table__thead',
        className,
    );

    return (<thead className={classes}>{children}</thead>);
};

Thead.defaultProps = {
    className: '',
};

Thead.propTypes = {
    className: PropTypes.string,
    children: PropTypes.oneOfType([
        PropTypes.arrayOf(PropTypes.node),
        PropTypes.node,
    ]).isRequired,
};

const Tbody = ({ children, className }) => {
    const classes = classnames(
        'table__tbody',
        className,
    );

    return (<tbody className={classes}>{children}</tbody>);
};

Tbody.defaultProps = {
    className: '',
};

Tbody.propTypes = {
    className: PropTypes.string,
    children: PropTypes.oneOfType([
        PropTypes.arrayOf(PropTypes.node),
        PropTypes.node,
    ]).isRequired,
};

const Tr = ({ children, className }) => {
    const classes = classnames(
        'table__tr',
        className,
    );

    return (<tr className={classes}>{children}</tr>);
};

Tr.defaultProps = {
    className: '',
};

Tr.propTypes = {
    className: PropTypes.string,
    children: PropTypes.oneOfType([
        PropTypes.arrayOf(PropTypes.node),
        PropTypes.node,
    ]).isRequired,
};

const Th = ({ children, className }) => {
    const classes = classnames(
        'table__th',
        className,
    );

    return (<th className={classes}>{children}</th>);
};

Th.defaultProps = {
    className: '',
};

Th.propTypes = {
    className: PropTypes.string,
    children: PropTypes.oneOfType([
        PropTypes.arrayOf(PropTypes.node),
        PropTypes.node,
    ]).isRequired,
};

const Td = ({ children, className }) => {
    const classes = classnames(
        'table__td',
        className,
    );

    return (<td className={classes}>{children}</td>);
};

Td.defaultProps = {
    className: '',
};

Td.propTypes = {
    className: PropTypes.string,
    children: PropTypes.oneOfType([
        PropTypes.arrayOf(PropTypes.node),
        PropTypes.node,
    ]).isRequired,
};

class Table extends Component {
    static Thead = Thead;

    static Tbody = Tbody;

    static Tr = Tr;

    static Th = Th;

    static Td = Td;

    render() {
        const { className, children, ...restProps } = this.props;
        const classes = classnames(
            'table',
            className,
        );

        return (<table className={classes} {...restProps}>{children}</table>);
    }
}

Table.defaultProps = {
    className: '',
};

Table.propTypes = {
    className: PropTypes.string,
    children: PropTypes.oneOfType([
        PropTypes.arrayOf(PropTypes.node),
        PropTypes.node,
    ]).isRequired,
};

export default Table;
