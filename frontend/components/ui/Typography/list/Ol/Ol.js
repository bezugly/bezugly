import React, { Component } from 'react';
import classnames from 'classnames';
import PropTypes from 'prop-types';

import './style.scss';

const Li = ({ children, className }) => {
    const classes = classnames(
        'li',
        className,
    );

    return (<li className={classes}>{children}</li>);
};

Li.defaultProps = {
    className: '',
};

Li.propTypes = {
    className: PropTypes.string,
    children: PropTypes.oneOfType([
        PropTypes.arrayOf(PropTypes.node),
        PropTypes.node,
    ]).isRequired,
};

class Ol extends Component {
    static li = Li;

    render() {
        const { className, children, ...restProps } = this.props;
        const classes = classnames(
            'list',
            'ol',
            className,
        );

        return (<ol className={classes} {...restProps}>{children}</ol>);
    }
}

Ol.defaultProps = {
    className: '',
};

Ol.propTypes = {
    className: PropTypes.string,
    children: PropTypes.oneOfType([
        PropTypes.arrayOf(PropTypes.node),
        PropTypes.node,
    ]).isRequired,
};

export default Ol;
