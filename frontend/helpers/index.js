export const throttle = function (func, ms) {
    let isThrottled = false;
    let savedArgs;
    let savedThis;

    function wrapper() {
        if (isThrottled) {
            // eslint-disable-next-line
            savedArgs = arguments;
            savedThis = this;
            return;
        }

        // eslint-disable-next-line
        func.apply(this, arguments);

        isThrottled = true;

        setTimeout(() => {
            isThrottled = false;

            if (savedArgs) {
                wrapper.apply(savedThis, savedArgs);
                // eslint-disable-next-line
                savedArgs = savedThis = null;
            }
        }, ms);
    }

    return wrapper;
};

const months = [
    'января',
    'февраля',
    'марта',
    'апреля',
    'мая',
    'июня',
    'июля',
    'августа',
    'сентября',
    'октября',
    'ноября',
    'декабря',
];
const halfAnHour = 1000 * 60 * 30;
const oneHour = halfAnHour * 2;
const oneHourAndHalf = oneHour + halfAnHour;

export const formatDate = (dateAndTime = '') => {
    if (!dateAndTime.length) {
        return '';
    }

    const date = new Date(Date.parse(dateAndTime));
    const currentDate = new Date();
    const day = date.getDate();
    const month = date.getMonth();
    const year = date.getFullYear();
    const valueDifference = currentDate.valueOf() - date.valueOf();

    if (currentDate.getFullYear() !== year) {
        return `${day} ${months[month]} ${year}`;
    }

    if (currentDate.getDate() !== day) {
        return `${day} ${months[month]}`;
    }

    if (valueDifference < halfAnHour) {
        return 'Сейчас';
    }

    if (valueDifference < oneHour) {
        return 'Полчаса назад';
    }

    if (valueDifference < oneHourAndHalf) {
        return 'Час назад';
    }

    return 'Сегодня';
};

const cases = [2, 0, 1, 1, 1, 2];
// words: '1 заметка', '2 заметки', '5 заметок'
export const getRightWord = (count, words) => {
    if (Array.isArray(words) && words.length !== 3) {
        console.error('Должен быть массив из 3 слов');
        return '';
    }
    return words[
        (count % 100 > 4 && count % 100 < 20)
            ? 2
            : cases[(count % 10 < 5)
                ? count % 10
                : 5
            ]];
};

export const getWorkingTime = () => {
    const start = new Date('2015-01-01');
    const end = new Date();

    const days = Math.ceil((end.valueOf() - start.valueOf()) / (1000 * 3600 * 24));
    const workingMonths = parseInt(days / 30.5, 10);
    const workingYears = parseInt(workingMonths / 12, 10);
    const restMonths = workingMonths % 12;

    let result = `${workingYears} ${getRightWord(workingYears, ['год', 'года', 'лет'])}`;

    if (restMonths) {
        result = `${result} ${restMonths} ${getRightWord(restMonths, ['месяц', 'месяца', 'месяцев'])}`;
    }

    return result;
};

export const getInputDateFromISO = (ISODate) => {
    const date = new Date(Date.parse(ISODate));
    const year = date.getFullYear();
    let month = date.getMonth() + 1;
    let day = date.getDate();

    month = month < 10 ? `0${month}` : month;
    day = day < 10 ? `0${day}` : day;

    return `${year}-${month}-${day}`;
};

export const getISODateFromInput = (inputDate, prevISODate) => {
    if (!inputDate.length) {
        return prevISODate;
    }

    const [year, month, day] = inputDate.split('-');
    const prevDate = new Date(Date.parse(prevISODate));

    prevDate.setFullYear(year);
    prevDate.setMonth(month - 1);
    prevDate.setDate(day);

    return prevDate.toISOString();
};
