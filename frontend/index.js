import 'babel-polyfill';
import 'element-closest';
import axios from 'axios';
import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';
import { Provider } from 'react-redux';
import { renderRoutes } from 'react-router-config';

import reducers from './reducers';
import Routes from './Routes';

const axiosInstance = axios.create({
    baseURL: '/api/v1',
    headers: { 'Content-Type': 'application/json' },
    withCredentials: true,
});

const { blog } = window.INITIAL_STATE;

// TODO: Убрать эту херь в другое место
if (blog && blog.posts && blog.posts.list) {
    blog.posts.list = new Map(blog.posts.list);
}

if (blog && blog.drafts && blog.drafts.list) {
    blog.drafts.list = new Map(blog.drafts.list);
}

const store = createStore(reducers, window.INITIAL_STATE, composeWithDevTools(
    applyMiddleware(thunk.withExtraArgument(axiosInstance)),
));

ReactDOM.hydrate(
    <Provider store={store}>
        <BrowserRouter>
            <div>{renderRoutes(Routes)}</div>
        </BrowserRouter>
    </Provider>,
    document.getElementById('root'),
);
