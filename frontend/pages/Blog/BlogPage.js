import React from 'react';
import PropTypes from 'prop-types';
import { renderRoutes } from 'react-router-config';

import './style.scss';

const BlogPage = ({ route }) => (
    <section className="blog-page">
        {renderRoutes(route.routes)}
    </section>
);

BlogPage.propTypes = {
    route: PropTypes.shape({
        routes: PropTypes.array.isRequired,
    }).isRequired,
};

export default {
    component: BlogPage,
};
