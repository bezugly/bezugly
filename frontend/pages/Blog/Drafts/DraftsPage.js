import React, { PureComponent, Fragment } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';

import { getRightWord } from '../../../helpers';

import { fetchDrafts } from '../../../actions';

import DraftsPageContent from '../../../components/DraftsPageContent';
import checkAuth from '../../../components/hocs/checkAuth';

class DraftsPage extends PureComponent {
    componentDidMount() {
        // eslint-disable-next-line
        const { fetchDrafts } = this.props;

        fetchDrafts();
    }

    render() {
        const { drafts, user } = this.props;
        const { list } = drafts;
        const title = list && list.size > 0
            ? `${list.size} ${getRightWord(list.size, ['черновик', 'черновика', 'черновиков'])} Дмитрия Безуглого`
            : 'Черновики Дмитрия Безуглого';

        return (
            <Fragment>
                <Helmet>
                    <title>{title}</title>
                </Helmet>

                <DraftsPageContent className="page-layout__main" user={user} />
            </Fragment>
        );
    }
}

DraftsPage.propTypes = {
    drafts: PropTypes.shape({
        list: PropTypes.instanceOf(Map).isRequired,
    }).isRequired,
    user: PropTypes.shape().isRequired,
    fetchDrafts: PropTypes.func.isRequired,
};

const loadData = store => store.dispatch(fetchDrafts());
const mapStateToProps = state => ({
    drafts: state.blog.drafts,
    user: state.auth.user,
});

export default {
    component: connect(mapStateToProps, { fetchDrafts })(checkAuth(DraftsPage)),
    loadData,
};
