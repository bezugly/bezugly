import React, { PureComponent, Fragment } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';

import { fetchDraftBySlug, fetchPostById, deleteDraft } from '../../../../actions';
import { postEditSelectorFactory, postFilesSelectorFactory, postOgImageSelectorFactory } from '../../../../selectors/blogSelectors';

import checkAuth from '../../../../components/hocs/checkAuth';
import DraftEditPageContent from '../../../../components/DraftEditPageContent';
import PostNotFound from '../../../../components/PostNotFound';


class DraftEditPage extends PureComponent {
    state = {
        error: '',
    };

    componentDidMount() {
        this.fetchData();
    }

    componentWillUnmount() {
        // eslint-disable-next-line
        const { draft, deleteDraft } = this.props;

        if (!draft) {
            return;
        }

        if (!draft.title && !draft.content && !draft.lead && !draft.files.length) {
            // eslint-disable-next-line
            deleteDraft(draft._id);
        }
    }

    removeError = () => {
        this.setState({ error: '' });
    };

    fetchData = async () => {
        // eslint-disable-next-line
        const { fetchDraftBySlug, fetchPostById, match: { params: { slug } } } = this.props;

        const draftBody = await fetchDraftBySlug(slug);

        if (draftBody.error) {
            this.setState({ error: draftBody.error });
        }

        // eslint-disable-next-line
        if (draftBody.payload && draftBody.payload.draft.reference) {
            await fetchPostById(draftBody.payload.draft.reference);
        }
    };

    renderScreen = () => {
        const {
            draft,
            files,
            ogImage,
            draftLoader,
        } = this.props;
        const { error } = this.state;

        if (draftLoader) {
            return <p>Загрузка...</p>;
        }

        if (error) {
            return <PostNotFound error={error} removeError={this.removeError} />;
        }

        if (draft) {
            // eslint-disable-next-line
            return <DraftEditPageContent key={draft._id} draft={draft} files={files} ogImage={ogImage} />;
        }

        return null;
    };

    render() {
        const { draft } = this.props;
        const title = draft && draft.reference ? 'Редактирование заметки' : 'Новая заметка';

        return (
            <Fragment>
                <Helmet>
                    <title>{title}</title>
                </Helmet>

                {this.renderScreen()}
            </Fragment>
        );
    }
}

DraftEditPage.defaultProps = {
    draft: null,
    ogImage: null,
};

DraftEditPage.propTypes = {
    match: PropTypes.shape({
        params: PropTypes.shape({
            slug: PropTypes.string,
        }),
    }).isRequired,
    draft: PropTypes.shape(),
    ogImage: PropTypes.shape(),
    files: PropTypes.arrayOf(PropTypes.shape()).isRequired,
    draftLoader: PropTypes.bool.isRequired,
    fetchDraftBySlug: PropTypes.func.isRequired,
    // eslint-disable-next-line
    user: PropTypes.shape().isRequired,
};

const loadData = async ({ dispatch }, { slug }) => {
    const draftBody = await dispatch(fetchDraftBySlug(slug));

    // eslint-disable-next-line
    if (draftBody.payload && draftBody.payload.draft.reference) {
        await fetchPostById(draftBody.payload.draft.reference);
    }
};
const mapStateToProps = () => {
    const draftSelector = postEditSelectorFactory('drafts');
    const filesSelector = postFilesSelectorFactory('drafts');
    const ogImageSelector = postOgImageSelectorFactory('drafts');

    return (state, props) => ({
        draft: draftSelector(state, props),
        files: filesSelector(state, props),
        ogImage: ogImageSelector(state, props),
        draftLoader: state.blog.loaders.draft,
        user: state.auth.user,
    });
};
const mapDispatchToProps = dispatch => ({
    fetchDraftBySlug: slug => dispatch(fetchDraftBySlug(slug)),
    fetchPostById: id => dispatch(fetchPostById(id)),
    deleteDraft: id => dispatch(deleteDraft(id)),
});

export default {
    component: connect(mapStateToProps, mapDispatchToProps)(checkAuth(DraftEditPage)),
    loadData,
};
