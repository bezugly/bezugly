import React, { PureComponent, Fragment } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';

import { fetchDraftBySlug, fetchPostById } from '../../../../actions';
import { postEditSelectorFactory } from '../../../../selectors/blogSelectors';

import checkAuth from '../../../../components/hocs/checkAuth';
import DraftItemPageContent from '../../../../components/DraftItemPageContent';
import PostNotFound from '../../../../components/PostNotFound';

class DraftItemPage extends PureComponent {
    isMounted = false;

    state = {
        error: '',
    };

    componentDidMount() {
        this.isMounted = true;

        this.fetchData();
    }

    componentWillUnmount() {
        this.isMounted = false;
    }

    removeError = () => {
        if (this.isMounted) {
            this.setState({ error: '' });
        }
    };

    fetchData = async () => {
        // eslint-disable-next-line
        const { fetchDraftBySlug, fetchPostById, match: { params: { slug } } } = this.props;

        const draftBody = await fetchDraftBySlug(slug);

        if (draftBody.error && this.isMounted) {
            this.setState({ error: draftBody.error });
        }

        // eslint-disable-next-line
        if (draftBody.payload && draftBody.payload.draft.reference) {
            await fetchPostById(draftBody.payload.draft.reference);
        }
    };

    renderScreen = () => {
        const { draft } = this.props;
        const { error } = this.state;

        if (error) {
            return <PostNotFound error={error} removeError={this.removeError} />;
        }

        if (draft) {
            // eslint-disable-next-line
            return <DraftItemPageContent key={draft._id} draft={draft} />;
        }

        return null;
    };

    render() {
        const { draft } = this.props;
        const title = `Черновик${draft ? ` «${draft.title}»` : ''}`;

        return (
            <Fragment>
                <Helmet>
                    <title>{title}</title>
                </Helmet>

                {this.renderScreen()}
            </Fragment>
        );
    }
}

const loadData = async ({ dispatch }, { slug }) => {
    const draftBody = await dispatch(fetchDraftBySlug(slug));

    // eslint-disable-next-line
    if (draftBody.payload && draftBody.payload.draft.reference) {
        await dispatch(fetchPostById(draftBody.payload.draft.reference));
    }
};
const mapStateToProps = () => {
    const draftSelector = postEditSelectorFactory('drafts');

    return (state, props) => ({
        draft: draftSelector(state, props),
        user: state.auth.user,
    });
};
const mapDispatchToProps = dispatch => ({
    fetchDraftBySlug: slug => dispatch(fetchDraftBySlug(slug)),
    fetchPostById: id => dispatch(fetchPostById(id)),
});

DraftItemPage.defaultProps = {
    draft: null,
};

DraftItemPage.propTypes = {
    match: PropTypes.shape({
        params: PropTypes.shape({
            slug: PropTypes.string,
        }),
    }).isRequired,
    draft: PropTypes.shape(),
    fetchDraftBySlug: PropTypes.func.isRequired,
    // eslint-disable-next-line
    user: PropTypes.shape().isRequired,
};

export default {
    component: connect(mapStateToProps, mapDispatchToProps)(checkAuth(DraftItemPage)),
    loadData,
};
