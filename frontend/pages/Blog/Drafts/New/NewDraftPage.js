import { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { createDraft } from '../../../../actions';

import checkAuth from '../../../../components/hocs/checkAuth';

class NewDraftPage extends Component {
    componentDidMount() {
        const { draft } = this.props;

        if (!draft) {
            window.location.reload();
        } else {
            window.location.pathname = `/blog/drafts/${draft.draftSlug}/edit`;
        }
    }

    render() {
        return null;
    }
}

NewDraftPage.propTypes = {
    draft: PropTypes.shape().isRequired,
    // eslint-disable-next-line
    user: PropTypes.shape().isRequired,
};

const loadData = ({ dispatch }) => dispatch(createDraft());
const mapStateToProps = ({ blog, auth }) => ({
    draft: Array.from(blog.drafts.list.values())[0],
    user: auth.user,
});

export default {
    component: connect(mapStateToProps, null)(checkAuth(NewDraftPage)),
    loadData,
};
