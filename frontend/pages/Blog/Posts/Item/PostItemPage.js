import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';

import { fetchPostBySlug } from '../../../../actions';
import { postEditSelectorFactory, postOgImageSelectorFactory } from '../../../../selectors/blogSelectors';

import PostItemPageContent from '../../../../components/PostItemPageContent';
import PostNotFound from '../../../../components/PostNotFound';

class PostItemPage extends Component {
    isMounted = false;

    state = {
        error: '',
    };

    componentDidMount() {
        this.isMounted = true;

        this.fetchData();
    }

    componentDidUpdate(prevProps) {
        const { post } = this.props;

        // eslint-disable-next-line
        const oldId = prevProps.post && prevProps.post._id;
        // eslint-disable-next-line
        const newId = post && post._id;

        if (newId !== oldId) {
            this.fetchData();
        }
    }

    componentWillUnmount() {
        this.isMounted = false;
    }

    removeError = () => {
        if (this.isMounted) {
            this.setState({ error: '' });
        }
    };

    fetchData = async () => {
        // eslint-disable-next-line
        const { fetchPostBySlug, match: { params: { slug } } } = this.props;

        const post = await fetchPostBySlug(slug);

        if (post.error && this.isMounted) {
            this.setState({ error: post.error });
        }
    };

    renderScreen = () => {
        const { post } = this.props;
        const { error } = this.state;

        if (error) {
            return <PostNotFound error={error} removeError={this.removeError} />;
        }

        if (post) {
            // eslint-disable-next-line
            return <PostItemPageContent key={post._id} post={post} />;
        }

        return null;
    };

    renderHelmet = () => {
        const { post, ogImage } = this.props;
        const title = post ? post.title : 'Заметка Дмитрия Безуглого';
        const extra = [];

        if (post) {
            extra.push(<meta key="og:description" property="og:description" content={post.lead || ''} />);
            extra.push(<meta key="twitter:description" name="twitter:description" content={post.lead || ''} />);

            if (ogImage) {
                extra.push(<meta key="og:image" property="og:image" content={`https://bezugly.ru/api/uploads/blog/${ogImage.name}`} />);
                extra.push(<meta key="twitter:image:src" property="twitter:image:src" content={`https://bezugly.ru/api/uploads/blog/${ogImage.name}`} />);
            }
        }

        return (
            <Helmet>
                <title>{title}</title>
                <meta property="og:title" content={title} />
                <meta property="og:type" content="website" />
                <meta property="og:site_name" content="Блог Дмитрия Безуглого" />
                <meta property="og:locale" content="ru_RU" />
                <meta name="twitter:card" content="summary_large_image" />
                <meta name="twitter:title" content={title} />
                {extra}
            </Helmet>
        );
    };

    render() {
        return (
            <Fragment>
                {this.renderHelmet()}

                {this.renderScreen()}
            </Fragment>
        );
    }
}

const loadData = ({ dispatch }, { slug }) => dispatch(fetchPostBySlug(slug));
const mapStateToProps = () => {
    const postSelector = postEditSelectorFactory('posts');
    const ogImageSelector = postOgImageSelectorFactory('posts');

    return (state, props) => ({
        post: postSelector(state, props),
        ogImage: ogImageSelector(state, props),
        user: state.auth.user,
    });
};

PostItemPage.defaultProps = {
    post: null,
    ogImage: null,
};

PostItemPage.propTypes = {
    match: PropTypes.shape({
        params: PropTypes.shape({
            slug: PropTypes.string,
        }),
    }).isRequired,
    post: PropTypes.shape(),
    ogImage: PropTypes.shape(),
    fetchPostBySlug: PropTypes.func.isRequired,
};

export default {
    component: connect(mapStateToProps, { fetchPostBySlug })(PostItemPage),
    loadData,
};
