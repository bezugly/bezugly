import React, { PureComponent, Fragment } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';

import { getRightWord } from '../../../helpers';

import { fetchPosts } from '../../../actions';

import PostsPageContent from '../../../components/PostsPageContent';

class PostsPage extends PureComponent {
    componentDidMount() {
        // eslint-disable-next-line
        const { fetchPosts } = this.props;

        fetchPosts();
    }

    render() {
        const { posts: { list } } = this.props;
        const title = list && list.size > 0
            ? `${list.size} ${getRightWord(list.size, ['заметка', 'заметки', 'заметок'])} Дмитрия Безуглого`
            : 'Блог Дмитрия Безуглого';

        return (
            <Fragment>
                <Helmet>
                    <title>{title}</title>
                    <meta property="og:title" content={title} />
                </Helmet>

                <PostsPageContent />
            </Fragment>
        );
    }
}

PostsPage.propTypes = {
    posts: PropTypes.shape({
        list: PropTypes.instanceOf(Map).isRequired,
    }).isRequired,
    fetchPosts: PropTypes.func.isRequired,
};

const loadData = store => store.dispatch(fetchPosts());
const mapStateToProps = ({ blog }) => ({ posts: blog.posts });

export default {
    component: connect(mapStateToProps, { fetchPosts })(PostsPage),
    loadData,
};
