import React, { Fragment } from 'react';
import { Helmet } from 'react-helmet';
import PropTypes from 'prop-types';

import CollaborationPageContent from '../../components/CollaborationPageContent';

const CollaborationPage = ({
    match: { params: { tab } },
    history: { push },
}) => (
    <Fragment>
        <Helmet>
            <title>Как поработать с Дмитрием Безуглым</title>
        </Helmet>

        <CollaborationPageContent className="page-layout__main" tab={tab && tab.toLowerCase()} historyPush={push} />
    </Fragment>
);

CollaborationPage.propTypes = {
    match: PropTypes.shape({
        params: PropTypes.shape({
            tab: PropTypes.string,
        }),
    }).isRequired,
    history: PropTypes.shape({
        push: PropTypes.func,
    }).isRequired,
};

export default {
    component: CollaborationPage,
};
