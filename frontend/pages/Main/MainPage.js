import React, { Fragment } from 'react';
import { Helmet } from 'react-helmet';

import MainPageContent from '../../components/MainPageContent';

const MainPage = () => (
    <Fragment>
        <Helmet>
            <title>Дима Безуглый</title>
            <meta property="og:title" content="Дима Безуглый" />
        </Helmet>

        <MainPageContent className="page-layout__main" />
    </Fragment>
);

export default {
    component: MainPage,
};
