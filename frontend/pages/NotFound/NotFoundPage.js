import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';

import NotFoundPageContent from '../../components/NotFoundPageContent';

const NotFoundPage = ({ staticContext }) => {
    // eslint-disable-next-line
    staticContext.notFound = true;

    return (
        <Fragment>
            <Helmet>
                <title>Страница не найдена</title>
                <meta property="og:title" content="Страница не найдена" />
            </Helmet>

            <NotFoundPageContent className="page-layout__main" />
        </Fragment>
    );
};

NotFoundPage.defaultProps = {
    staticContext: {},
};

NotFoundPage.propTypes = {
    staticContext: PropTypes.instanceOf(Object),
};

export default {
    component: NotFoundPage,
};
