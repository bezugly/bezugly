import {
    LOGIN_USER,
    GET_CURRENT_USER,
    LOGOUT_USER,
    START,
    SUCCESS,
    FAIL,
} from '../actions/actionTypes';

const initialState = {
    user: null,
    loaders: {
        login: false,
    },
};

export default (state = initialState, action) => {
    switch (action.type) {
        case GET_CURRENT_USER + SUCCESS: {
            const { payload: user } = action;

            return {
                ...state,
                user,
            };
        }

        case LOGIN_USER + START: {
            return {
                ...state,
                loaders: {
                    ...state.loaders,
                    login: true,
                },
            };
        }

        case LOGIN_USER + FAIL: {
            return {
                ...state,
                loaders: {
                    ...state.loaders,
                    login: false,
                },
            };
        }

        case LOGIN_USER + SUCCESS: {
            const { payload: user } = action;

            return {
                ...state,
                loaders: {
                    ...state.loaders,
                    login: false,
                },
                user,
            };
        }

        case LOGOUT_USER + SUCCESS: {
            const { payload: user } = action;

            return {
                ...state,
                user,
            };
        }

        default:
            return state;
    }
};
