import {
    SHOW_LOADER,
    FETCH_POSTS,
    FETCH_POST,
    DELETE_POST,
    COPY_POST,
    FETCH_DRAFTS,
    FETCH_DRAFT,
    CREATE_DRAFT,
    UPDATE_DRAFT,
    DELETE_DRAFT,
    PUBLISH_DRAFT,
    REPLACE_DRAFT_WITH_POST,
    PUBLISH_DRAFT_AS_NEW_POST,
    CHECK_SLUG,
    UPLOAD_FILE,
    UPDATE_FILE,
    UPLOAD_OG_IMAGE,
    START,
    SUCCESS,
    FAIL,
} from '../actions/actionTypes';

const initialState = {
    files: {},
    posts: {
        list: new Map(),
    },
    drafts: {
        list: new Map(),
    },
    loaders: {
        posts: false,
        post: false,
        deletePost: false,
        copyPost: false,
        drafts: false,
        draft: false,
        createDraft: false,
        updateDraft: false,
        deleteDraft: false,
        publishDraft: false,
        replaceDraftWithPost: false,
        publishDraftAsNewPost: false,
        checkSlug: false,
        uploadFile: false,
        updateFile: false,
        uploadOgImage: false,
    },
    errors: {
        posts: '',
        post: '',
        deletePost: '',
        copyPost: '',
        drafts: '',
        draft: '',
        createDraft: '',
        updateDraft: '',
        deleteDraft: '',
        publishDraft: '',
        replaceDraftWithPost: '',
        publishDraftAsNewPost: '',
        checkSlug: '',
        uploadFile: '',
        updateFile: '',
        uploadOgImage: '',
    },
};

export default (state = initialState, action) => {
    switch (action.type) {
        case SHOW_LOADER: {
            const { payload: loader } = action;

            return {
                ...state,
                loaders: {
                    ...state.loaders,
                    [loader]: true,
                },
            };
        }

        case FETCH_POSTS + START: {
            return {
                ...state,
                errors: {
                    ...state.loaders,
                    posts: '',
                },
            };
        }

        case FETCH_POSTS + FAIL: {
            const { error } = action;

            return {
                ...state,
                loaders: {
                    ...state.loaders,
                    posts: false,
                },
                errors: {
                    ...state.loaders,
                    posts: error,
                },
            };
        }

        case FETCH_POSTS + SUCCESS: {
            const { payload: { list, files } } = action;

            return {
                ...state,
                loaders: {
                    ...state.loaders,
                    posts: false,
                },
                posts: {
                    ...state.posts,
                    list,
                },
                files: {
                    ...state.files,
                    ...files,
                },
            };
        }

        case FETCH_POST + START: {
            return {
                ...state,
                errors: {
                    ...state.loaders,
                    post: '',
                },
            };
        }

        case FETCH_POST + FAIL: {
            const { error } = action;

            return {
                ...state,
                loaders: {
                    ...state.loaders,
                    post: false,
                },
                errors: {
                    ...state.loaders,
                    post: error,
                },
            };
        }

        case FETCH_POST + SUCCESS: {
            const { payload: { post, files } } = action;
            const { list } = state.posts;

            list.set(post.slug, post);

            return {
                ...state,
                loaders: {
                    ...state.loaders,
                    post: false,
                },
                posts: {
                    ...state.posts,
                    list,
                },
                files: {
                    ...state.files,
                    ...files,
                },
            };
        }

        case DELETE_POST + START: {
            return {
                ...state,
                loaders: {
                    ...state.loaders,
                    deletePost: true,
                },
                errors: {
                    ...state.loaders,
                    deletePost: '',
                },
            };
        }

        case DELETE_POST + FAIL: {
            const { error } = action;

            return {
                ...state,
                loaders: {
                    ...state.loaders,
                    deletePost: false,
                },
                errors: {
                    ...state.loaders,
                    deletePost: error,
                },
            };
        }

        case DELETE_POST + SUCCESS: {
            const { payload: post } = action;
            const { posts } = state;

            posts.list.delete(post.slug);

            return {
                ...state,
                loaders: {
                    ...state.loaders,
                    deletePost: false,
                },
                posts: {
                    ...posts,
                    list: posts.list,
                },
            };
        }

        case COPY_POST + START: {
            return {
                ...state,
                loaders: {
                    ...state.loaders,
                    copyPost: true,
                },
                errors: {
                    ...state.loaders,
                    copyPost: '',
                },
            };
        }

        case COPY_POST + FAIL: {
            const { error } = action;

            return {
                ...state,
                loaders: {
                    ...state.loaders,
                    copyPost: false,
                },
                errors: {
                    ...state.loaders,
                    copyPost: error,
                },
            };
        }

        case COPY_POST + SUCCESS: {
            const { payload: draft } = action;
            const { drafts } = state;

            drafts.list.set(draft.draftSlug, draft);

            return {
                ...state,
                loaders: {
                    ...state.loaders,
                    copyPost: false,
                },
                drafts: {
                    ...state.drafts,
                    list: drafts.list,
                },
            };
        }

        case FETCH_DRAFTS + START: {
            return {
                ...state,
                errors: {
                    ...state.loaders,
                    drafts: '',
                },
            };
        }

        case FETCH_DRAFTS + FAIL: {
            const { error } = action;

            return {
                ...state,
                loaders: {
                    ...state.loaders,
                    drafts: false,
                },
                errors: {
                    ...state.loaders,
                    drafts: error,
                },
            };
        }

        case FETCH_DRAFTS + SUCCESS: {
            const { payload: { list, files } } = action;

            return {
                ...state,
                loaders: {
                    ...state.loaders,
                    drafts: false,
                },
                drafts: {
                    ...state.drafts,
                    list,
                },
                files: {
                    ...state.files,
                    ...files,
                },
            };
        }

        case FETCH_DRAFT + START: {
            return {
                ...state,
                errors: {
                    ...state.loaders,
                    draft: '',
                },
            };
        }

        case FETCH_DRAFT + FAIL: {
            const { error } = action;

            return {
                ...state,
                loaders: {
                    ...state.loaders,
                    draft: false,
                },
                errors: {
                    ...state.loaders,
                    draft: error,
                },
            };
        }

        case FETCH_DRAFT + SUCCESS: {
            const { payload: { draft, files } } = action;
            const { list } = state.drafts;

            list.set(draft.draftSlug, draft);

            return {
                ...state,
                loaders: {
                    ...state.loaders,
                    draft: false,
                },
                drafts: {
                    ...state.drafts,
                    list,
                },
                files: {
                    ...state.files,
                    ...files,
                },
            };
        }

        case CREATE_DRAFT + START: {
            return {
                ...state,
                loaders: {
                    ...state.loaders,
                    createDraft: true,
                },
                errors: {
                    ...state.loaders,
                    createDraft: '',
                },
            };
        }

        case CREATE_DRAFT + FAIL: {
            const { error } = action;

            return {
                ...state,
                loaders: {
                    ...state.loaders,
                    createDraft: false,
                },
                errors: {
                    ...state.loaders,
                    createDraft: error,
                },
            };
        }

        case CREATE_DRAFT + SUCCESS: {
            const { payload: draft } = action;
            const { drafts } = state;

            drafts.list.set(draft.draftSlug, draft);

            return {
                ...state,
                drafts,
                loaders: {
                    ...state.loaders,
                    createDraft: false,
                },
            };
        }

        case UPDATE_DRAFT + START: {
            return {
                ...state,
                loaders: {
                    ...state.loaders,
                    updateDraft: true,
                },
                errors: {
                    ...state.loaders,
                    updateDraft: '',
                },
            };
        }

        case UPDATE_DRAFT + FAIL: {
            const { error } = action;

            return {
                ...state,
                loaders: {
                    ...state.loaders,
                    updateDraft: false,
                },
                errors: {
                    ...state.loaders,
                    updateDraft: error,
                },
            };
        }

        case UPDATE_DRAFT + SUCCESS: {
            const { payload: draft } = action;
            const { drafts } = state;

            drafts.list.set(draft.draftSlug, draft);

            return {
                ...state,
                loaders: {
                    ...state.loaders,
                    updateDraft: false,
                },
                drafts: {
                    ...drafts,
                    list: drafts.list,
                },
            };
        }

        case DELETE_DRAFT + START: {
            return {
                ...state,
                loaders: {
                    ...state.loaders,
                    deleteDraft: true,
                },
                errors: {
                    ...state.loaders,
                    deleteDraft: '',
                },
            };
        }

        case DELETE_DRAFT + FAIL: {
            const { error } = action;

            return {
                ...state,
                loaders: {
                    ...state.loaders,
                    deleteDraft: false,
                },
                errors: {
                    ...state.loaders,
                    deleteDraft: error,
                },
            };
        }

        case DELETE_DRAFT + SUCCESS: {
            const { payload: draft } = action;
            const { drafts } = state;

            drafts.list.delete(draft.draftSlug);

            return {
                ...state,
                loaders: {
                    ...state.loaders,
                    deleteDraft: false,
                },
                drafts: {
                    ...drafts,
                    list: drafts.list,
                },
            };
        }

        case PUBLISH_DRAFT + START: {
            return {
                ...state,
                loaders: {
                    ...state.loaders,
                    publishDraft: true,
                },
                errors: {
                    ...state.loaders,
                    publishDraft: '',
                },
            };
        }

        case PUBLISH_DRAFT + FAIL: {
            const { error } = action;

            return {
                ...state,
                loaders: {
                    ...state.loaders,
                    publishDraft: false,
                },
                errors: {
                    ...state.loaders,
                    publishDraft: error,
                },
            };
        }

        case PUBLISH_DRAFT + SUCCESS: {
            const { payload: post } = action;
            const { drafts, posts } = state;

            drafts.list.delete(post.draftSlug);
            posts.list.set(post.slug, post);

            return {
                ...state,
                loaders: {
                    ...state.loaders,
                    publishDraft: false,
                },
                posts: {
                    ...posts,
                    list: posts.list,
                },
                drafts: {
                    ...drafts,
                    list: drafts.list,
                },
            };
        }

        case REPLACE_DRAFT_WITH_POST + START: {
            return {
                ...state,
                loaders: {
                    ...state.loaders,
                    replaceDraftWithPost: true,
                },
                errors: {
                    ...state.loaders,
                    replaceDraftWithPost: '',
                },
            };
        }

        case REPLACE_DRAFT_WITH_POST + FAIL: {
            const { error } = action;

            return {
                ...state,
                loaders: {
                    ...state.loaders,
                    replaceDraftWithPost: false,
                },
                errors: {
                    ...state.loaders,
                    replaceDraftWithPost: error,
                },
            };
        }

        case REPLACE_DRAFT_WITH_POST + SUCCESS: {
            return {
                ...state,
                loaders: {
                    ...state.loaders,
                    replaceDraftWithPost: false,
                },
            };
        }

        case PUBLISH_DRAFT_AS_NEW_POST + START: {
            return {
                ...state,
                loaders: {
                    ...state.loaders,
                    publishDraftAsNewPost: true,
                },
                errors: {
                    ...state.loaders,
                    publishDraftAsNewPost: '',
                },
            };
        }

        case PUBLISH_DRAFT_AS_NEW_POST + FAIL: {
            const { error } = action;

            return {
                ...state,
                loaders: {
                    ...state.loaders,
                    publishDraftAsNewPost: false,
                },
                errors: {
                    ...state.loaders,
                    publishDraftAsNewPost: error,
                },
            };
        }

        case PUBLISH_DRAFT_AS_NEW_POST + SUCCESS: {
            return {
                ...state,
                loaders: {
                    ...state.loaders,
                    publishDraftAsNewPost: false,
                },
            };
        }

        case CHECK_SLUG + START: {
            return {
                ...state,
                loaders: {
                    ...state.loaders,
                    checkSlug: true,
                },
                errors: {
                    ...state.loaders,
                    checkSlug: '',
                },
            };
        }

        case CHECK_SLUG + FAIL: {
            const { error } = action;

            return {
                ...state,
                loaders: {
                    ...state.loaders,
                    checkSlug: false,
                },
                errors: {
                    ...state.loaders,
                    checkSlug: error,
                },
            };
        }

        case CHECK_SLUG + SUCCESS: {
            return {
                ...state,
                loaders: {
                    ...state.loaders,
                    checkSlug: false,
                },
            };
        }

        case UPLOAD_FILE + START: {
            return {
                ...state,
                loaders: {
                    ...state.loaders,
                    uploadFile: true,
                },
                errors: {
                    ...state.loaders,
                    uploadFile: '',
                },
            };
        }

        case UPLOAD_FILE + FAIL: {
            const { error } = action;

            return {
                ...state,
                loaders: {
                    ...state.loaders,
                    uploadFile: false,
                },
                errors: {
                    ...state.loaders,
                    uploadFile: error,
                },
            };
        }

        case UPLOAD_FILE + SUCCESS: {
            const { payload: file } = action;

            return {
                ...state,
                loaders: {
                    ...state.loaders,
                    uploadFile: false,
                },
                files: {
                    ...state.files,
                    // eslint-disable-next-line
                    [file._id]: file,
                },
            };
        }

        case UPDATE_FILE + START: {
            return {
                ...state,
                loaders: {
                    ...state.loaders,
                    updateFile: true,
                },
                errors: {
                    ...state.loaders,
                    updateFile: '',
                },
            };
        }

        case UPDATE_FILE + FAIL: {
            const { error } = action;

            return {
                ...state,
                loaders: {
                    ...state.loaders,
                    updateFile: false,
                },
                errors: {
                    ...state.loaders,
                    updateFile: error,
                },
            };
        }

        case UPDATE_FILE + SUCCESS: {
            const { payload: file } = action;

            return {
                ...state,
                loaders: {
                    ...state.loaders,
                    updateFile: false,
                },
                files: {
                    ...state.files,
                    // eslint-disable-next-line
                    [file._id]: file,
                },
            };
        }

        case UPLOAD_OG_IMAGE + START: {
            return {
                ...state,
                loaders: {
                    ...state.loaders,
                    uploadOgImage: true,
                },
                errors: {
                    ...state.loaders,
                    uploadOgImage: '',
                },
            };
        }

        case UPLOAD_OG_IMAGE + FAIL: {
            const { error } = action;

            return {
                ...state,
                loaders: {
                    ...state.loaders,
                    uploadOgImage: false,
                },
                errors: {
                    ...state.loaders,
                    uploadOgImage: error,
                },
            };
        }

        case UPLOAD_OG_IMAGE + SUCCESS: {
            const { payload: file } = action;

            return {
                ...state,
                loaders: {
                    ...state.loaders,
                    uploadOgImage: false,
                },
                files: {
                    ...state.files,
                    // eslint-disable-next-line
                    [file._id]: file,
                },
            };
        }

        default:
            return state;
    }
};
