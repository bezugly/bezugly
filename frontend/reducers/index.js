import { combineReducers } from 'redux';
import blogReducer from './blogReducer';
import authReducer from './authReducer';
import messageReducer from './messageReducer';

export default combineReducers({
    blog: blogReducer,
    auth: authReducer,
    messages: messageReducer,
});
