import {
    SEND_MESSAGE,
    REMOVE_MESSAGE,
} from '../actions/actionTypes';

const initialState = [];

export default (state = initialState, action) => {
    switch (action.type) {
        case SEND_MESSAGE: {
            const {
                theme,
                status,
                text,
            } = action;

            return [
                ...state,
                {
                    id: Math.random()
                        .toString(36)
                        .substring(7),
                    theme,
                    status,
                    text,
                },
            ];
        }

        case REMOVE_MESSAGE: {
            const { id } = action;

            return state.filter(item => item.id !== id);
        }

        default:
            return state;
    }
};
