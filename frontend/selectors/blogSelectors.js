import { createSelector } from 'reselect';

const blogGetter = state => state.blog;
const matchParamsGetter = (state, props) => props.match.params;

export const postEditSelectorFactory = postType => createSelector(
    blogGetter,
    matchParamsGetter,
    (blog, match) => {
        const { slug } = match;
        let post = null;

        if (postType && slug && blog[postType] && blog[postType].list) {
            post = blog[postType].list.get(slug);
        }

        return post;
    },
);

export const postFilesSelectorFactory = postType => createSelector(
    blogGetter,
    matchParamsGetter,
    (blog, match) => {
        const { slug } = match;
        let post = null;

        if (postType && slug && blog[postType] && blog[postType].list) {
            post = blog[postType].list.get(slug);
        }

        if (!post) {
            return [];
        }

        return post.files.map(fileId => blog.files[fileId]).filter(Boolean);
    },
);

export const postOgImageSelectorFactory = postType => createSelector(
    blogGetter,
    matchParamsGetter,
    (blog, match) => {
        const { slug } = match;
        let post = null;

        if (postType && slug && blog[postType] && blog[postType].list) {
            post = blog[postType].list.get(slug);
        }

        if (!post || !post.ogImage) {
            return null;
        }

        return blog.files[post.ogImage] || null;
    },
);
