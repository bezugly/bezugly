const path = require('path');
const merge = require('webpack-merge');
const webpackNodeExternals = require('webpack-node-externals');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const baseConfig = require('./webpack.base');

const getConfig = isProduction => ({
    target: 'node',

    entry: './backend/index.js',

    output: {
        filename: 'server.js',
        path: path.resolve(__dirname, isProduction ? 'copy-build' : 'build'),
    },

    module: {
        rules: [
            {
                test: /\.html$/,
                use: 'raw-loader',
            },
            {
                test: /\.(scss|css)$/,
                loader: 'css-loader',
                options: {
                    exportOnlyLocals: true,
                },
            },
        ],
    },

    externals: [webpackNodeExternals()],

    plugins: [
        new CleanWebpackPlugin(),
    ],
});

module.exports = (_, options) => merge(baseConfig, getConfig(options.mode === 'production'));
